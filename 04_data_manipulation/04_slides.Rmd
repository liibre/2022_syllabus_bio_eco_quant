---
title: "Data manipulation"
author: "Sara Mortara & Andrea Sánchez-Tapia"
institute: "re.green | ¡liibre!"
date: "2022-07-12"
output:
  xaringan::moon_reader:
    lib_dir: libs
    css: xaringan-themer.css
    nature:
      highlightStyle: github
      highlightLines: true
      countIncrementalSlides: false
      ratio: '16:9'
self-contained: true
---

```{css css-config, echo = FALSE}
.tiny .remark-code { /*Change made here*/
  font-size: 50% !important;
}
```

```{r setup, include=FALSE}
options(htmltools.dir.version = FALSE)
```

```{r xaringanthemer, include = FALSE}
library(xaringanthemer)
xaringanthemer::style_mono_accent(
  base_color = "#A70000",
  #  primary_color = "#A70000",
  #secondary_color = "#ff0000",
  white_color = "white",
  colors = c(
    red = "#A70000",
    purple = "#88398a",
    orange = "#ff8811",
    green = "#136f63",
    blue = "#4B4FFF",
    white = "#FFFFFF",
    black = "#181818"
  ),
  header_font_google = google_font("Roboto Condensed"),
  text_font_google   = google_font("Roboto Condensed", "300", "300i"),
  code_font_google   = google_font("Ubuntu Mono"),
  text_font_size = "30px",
  code_font_size = "30px",
  )
xaringanExtra::use_share_again()

# clipboard
htmltools::tagList(
  xaringanExtra::use_clipboard(
    button_text = "Copy code <i class=\"fa fa-clipboard\"></i>",
    success_text = "Copied! <i class=\"fa fa-check\" style=\"color: #90BE6D\"></i>",
    error_text = "Not copied 😕 <i class=\"fa fa-times-circle\" style=\"color: #F94144\"></i>"
  ),
  rmarkdown::html_dependency_font_awesome()
)

```


## today

- data

- relational data bases

- manipulating data in R

---

## data

```{r, echo=FALSE}
knitr::include_graphics("figs/Data-Wisdom.jpg")
```



---
#  data and data structure 


- knowledge comes from synthesizing multiples perspectives
--


- every data have a __context__
--


- data cleanning
    - insistence on tidiness, cleanliness, and order
    - idea of data scientists as wizards or janitors
--


- ideas that data should always be clean and controlled have tainted historical roots
--



---
## organizing data


```{r, fig.align='center', echo=FALSE, out.width=600}
knitr::include_graphics("figs/tidy_data.png")
```

---
## organizing data

- manipulating data in rows and columns


- data tidying: structuring datasets to facilitate analysis



```{r, fig.align='center', echo=FALSE}
knitr::include_graphics("figs/tidy01.png")
```

---

## organizing data: tidy

- each column a variable

- each row an observation

```{r, fig.align='center', echo=FALSE}
knitr::include_graphics("figs/tidy02.png")
```

---
class: inverse, center, middle 

## relational data bases


---

## relational data bases

+ different data are organized in different tables
--


+ tables are integrated
--


+ common identifier for each table
--


+ in general organized in __SQL__ (__Structured Query Language__)

---

## dealing with relational data

+ tipically data analysis in different tables
--


+ relations between tables are pairwised
--


+ relations of three or more tables are always properties of pairwise relationships
--


+ __verbs__ to work with pairs of tables

---
## types of verbs

+ mutating
--


+ filtering
--


+ set operations
--


+ data manipulation can be done using packages __base__ and __dplyr__

---
background-image: url("figs/dplyr.png")
background-position: 98% 2%
background-size: 100px
## __dplyr__ package

based on relational data bases logic


+ easier than SQL because it is focused on data analysis
--


+ __grammar__ for data manipulation
--


+ `mutate()`
--


+ `filter()`
--

---
## data structure

```{r, fig.align='center', echo=FALSE}
knitr::include_graphics("figs/dplyr2.png")
```

---
## keys

```{r, fig.align='center', echo=FALSE, out.width=800}
knitr::include_graphics("figs/what-is-a-relational-database.jpg")
```

---
## keys


+ a key indicates an unique observation

--


+ __primary key__ --> identifies an observation in its own table
--


+ __foreign key__ --> identifies an observation in another table
--


+ a variable can be a primary key in one table and a foreign key in another
--


+ every primary key must contain unique information!
--


+ every table must have a primary key

---
## relation

is given by __primary key__ and its respective __foreign key__

.pull-left[


+ 1-to-many


+ 1-to-1



+ we use the keys to combine tables


+ we use `merge()` (__base__) or `join()` (__dplyr__) 

]


.pull-right[


```{r, fig.align='center', echo=FALSE, out.width=400}
knitr::include_graphics("figs/what-is-a-relational-database.jpg")
```


]


---
## using relationships to join data

```{r, fig.align='center', echo=FALSE, out.width=400}
knitr::include_graphics("figs/join.png")
```

---

## equivalences between __dplyr__ and __base__

```{r, fig.align='center', echo=FALSE}
knitr::include_graphics("figs/join_merge.png")
```

---
class: inverse, center, middle 

## manipulating data in R


---

## CESTES data base

```{r, fig.align='center', echo=FALSE, out.width=700}
knitr::include_graphics("figs/cestesdatabase.png")
```


[Jeliazkov et al 2020 Sci Data](https://doi.org/10.1038/s41597-019-0344-7)

---

## open data and code

```{r, fig.align='center', echo=FALSE}
knitr::include_graphics("figs/cabecalho_cestes.png")
```

---

## open data and code

```{r, fig.align='center', echo=FALSE}
knitr::include_graphics("figs/setwdeca.png")
```

---


## open data and code

.pull-left[
```{r, fig.align='center', echo=FALSE}
knitr::include_graphics("figs/JennyBryan.png")
```
]

.pull-right[
```{r, fig.align='center', echo=FALSE, out.width=300}
knitr::include_graphics("figs/jenny.jpg")
```
]


---
## todo

`r icons::fontawesome("laptop-code")`

- create and run `03_data_manipulation.R`  

- generate outputs

- `git add`, `commit`, and `push`


---

## a workflow in R

loading needed packages

```{r message = FALSE}
library("reshape2")
library("tidyverse")
```



---



## exploring the data
## species vs. sites

.tiny[
```{r}
files_path <- list.files("data/raw/cestes/", 
                         pattern = ".csv", 
                         full.names = TRUE)

files_path

files_path[3]
```
]

---
## reading the data in R
### creating `data.frame` objects

```{r}
comm <- read.csv(files_path[1])
coord <- read.csv(files_path[2])
envir <- read.csv(files_path[3])
splist <- read.csv(files_path[4])
traits <- read.csv(files_path[5])
```

---

## inspecting the community data

```{r}
comm[1:6, 1:10]
```

---
## inspecting traits data

```{r}
traits[1:9, 1:7]
```

---
## inspecting environmental data

```{r}
head(envir)
```

---
## inspecting coordenate data

```{r}
head(coord)
```

---
## inspecting the species list data

```{r}
head(splist)

# how many species?
nrow(splist)
```

---
## adding coordinates in the site `data.frame`

.tiny[
```{r}
# info on coord
names(coord)
dim(coord)

# info on envir
names(envir)
dim(envir)
```
]

---

## using `merge` and a common column

the common column is __primary__ and __foreign__ key


```{r}
envir_coord <- merge(x = envir, 
                     y = coord, 
                     by = "Sites")

dim(envir_coord)
```


---

## checking the `merge`


```{r}
names(envir_coord)
```

---

## transforming the species matrix vs. area in data table (tidy data)


```{r}
comm[1:6, 1:10]
```

---
## using the package **reshape2**


```{r}
# converting the matrix into tidy data
comm_df <- reshape2::melt(comm[, -1])

# checking if it worked
head(comm_df)
```

---
## using the package **reshape2**

.tiny[

```{r}
# how many the times the species repeats
table(comm_df$variable)
```

]

---
## creating the variable "Sites"


```{r}
# how many species?
n_sp <- nrow(splist)
n_sp 

# vector containing all sites
Sites <- envir$Sites
length(Sites)

comm_df$Sites <- rep(Sites, each = n_sp)
```

---
## checking if the first column was created

```{r}
head(comm_df)
```

---
## changing column names

```{r}
names(comm_df)

names(comm_df)[1:2] <- c("TaxCode", "Abundance")

head(comm_df)
```


---
## using R **base**

let's add a column with the species name to our `comm_df` object

```{r eval = FALSE}
comm_sp <- merge(x = comm_df, 
                 y = splist[, c(1, 3)], 
                 by = "TaxCode")
```

---
## using packages **dplyr** e **tidyr**

let's add a column with the species name to our `comm_df` object

```{r}
comm_sp <- inner_join(x = comm_df, 
                      y = splist[, c(1, 3)])
```

---
## checking our new column

```{r}
head(comm_sp)
```

---
## using the package **tidyr**

```{r}
comm_tidy <- tidyr::pivot_longer(comm, cols = 2:ncol(comm), names_to = "TaxCode", values_to = "Abundance")

head(comm_tidy)

dim(comm_tidy)
```

---
## joining all variables into a single table
### binding `comm_sp` with `envir.coord`

.tiny[
```{r}
comm_envir <- inner_join(x = comm_sp, 
                         y = envir_coord, 
                         by = "Sites")

dim(comm_sp)
dim(envir_coord)
dim(comm_envir)
```
]


---
## writing final table

```{r}
write.csv("comm_envir", "data/processed/03_Pavoine_full_table.csv", row.names = FALSE)
```



---
class: center, middle
# ¡Thanks!

<center>
`r icons::icon_style(icons::fontawesome("paper-plane", style = "solid"), scale = 1, fill = "#A70000")` [saramortara@gmail.com](mailto:saramortara@gmail.com) | [andreasancheztapia@gmail.com](mailto:andreasancheztapia@gmail.com) 


`r icons::icon_style(icons::fontawesome("twitter", style = "brands"), scale = 1, fill = "#A70000")` [@MortaraSara](https://twitter.com/MortaraSara) | [@SanchezTapiaA](https://twitter.com/SanchezTapiaA) 

`r icons::icon_style(icons::fontawesome("github", style = "brands"), scale = 1, fill = "#A70000")``r icons::icon_style(icons::fontawesome("gitlab", style = "brands"), scale = 1, fill = "#A70000")`  [saramortara](http://github.com/saramortara) |  [andreasancheztapia](http://github.com/andreasancheztapia) 


