---
title: "Statistical modeling"
author: "Sara Mortara & Andrea Sánchez-Tapia"
institute: "re.green | ¡liibre!"
date: "2022-07-19"
output:
  xaringan::moon_reader:
    lib_dir: libs
    css: xaringan-themer.css
    nature:
      highlightStyle: github
      highlightLines: true
      countIncrementalSlides: false
      ratio: '16:9'
self-contained: true
---


```{css css-config, echo = FALSE}
.tiny .remark-code { /*Change made here*/
  font-size: 50% !important;
}
```

```{r setup, include=FALSE}
options(htmltools.dir.version = FALSE)
```

```{r xaringanthemer, include = FALSE}
library(xaringanthemer)
xaringanthemer::style_mono_accent(
  base_color = "#A70000",
  #  primary_color = "#A70000",
  #secondary_color = "#ff0000",
  white_color = "white",
  colors = c(
    red = "#A70000",
    purple = "#88398a",
    orange = "#ff8811",
    green = "#136f63",
    blue = "#4B4FFF",
    white = "#FFFFFF",
    black = "#181818"
  ),
  header_font_google = google_font("Roboto Condensed"),
  text_font_google   = google_font("Roboto Condensed", "300", "300i"),
  code_font_google   = google_font("Ubuntu Mono"),
  text_font_size = "30px",
  code_font_size = "30px",
  )
xaringanExtra::use_share_again()

# clipboard
htmltools::tagList(
  xaringanExtra::use_clipboard(
    button_text = "Copy code <i class=\"fa fa-clipboard\"></i>",
    success_text = "Copied! <i class=\"fa fa-check\" style=\"color: #90BE6D\"></i>",
    error_text = "Not copied 😕 <i class=\"fa fa-times-circle\" style=\"color: #F94144\"></i>"
  ),
  rmarkdown::html_dependency_font_awesome()
)

```



```{r generating-data, echo=FALSE, message=FALSE}
library(wesanderson)
library(dplyr)
library(ggplot2)

cor <- wes_palette("Rushmore1") #("FantasticFox1")

set.seed(4)
x1 = seq(1, 5, by = 0.5)
y0 = 1.2 + 3.5 * x1
res = rnorm(n = 9, mean = 0, sd = 5)
y1 = y0 + res
xy = data.frame(x1, y0, res, y1)

## plot(x1, y1)
## abline(lm(y1~x1))
## summary(lm(y1~x1))


xy$lmpred <- predict(lm(y1~x1))
xy$lmres <- residuals(lm(y1~x1))

prev <- xy$lmpred

title_size <- 20
text_size <- 18
my_theme <- theme_classic() +
  theme(axis.title.x = element_text(size = title_size),
        axis.text.x = element_text(size = text_size),
        axis.title.y = element_text(size = title_size),
        axis.text.y = element_text(size = text_size), 
        plot.title = element_text(size = 40))
```


## about 

1. concepts in statistical modeling

2. probability distributions

3. the linear model

---
class: center, middle, inverse
# 1. concepts in statistical modellng


---
class: center, middle, inverse

# connect theory with data using statistical models

---
## best references

.pull-left[
```{r, echo=FALSE, fig.align='center', out.width=300}
knitr::include_graphics("figs/ecological_detective.jpg")
```

]


.pull-left[
```{r, echo=FALSE, fig.align='center', out.width=300}
knitr::include_graphics("figs/burnham_anderson.jpeg")
```

]


---
## best references

.pull-left[
```{r, echo=FALSE, fig.align='center', out.width=300}
knitr::include_graphics("figs/ben_bolker.jpg")
```

]


.pull-left[
```{r, echo=FALSE, fig.align='center', out.width=300}
knitr::include_graphics("figs/ecological_statistics.jpg")
```

]



---
## model & data

- data are not sacrossanct
- search for a minimal and suitable model

```{r, echo=FALSE, fig.align='center', out.width='30%'}
knitr::include_graphics("figs/noun-regression-analysis-2009607.png")
```


---

## the data

- continuous or dicrete variable?
- how many replicates?
- what are the predictor variables?
- what is the pattern?

--

## concepts

- maximum likelihood
- principle of parsimony 
  - Ocram's razor 
  
---
## maximum likelihood

given the data and the model:

what are the parameter values that make the data more plausible?

```{r, echo=FALSE, out.width='50%', fig.align='center'}
knitr::include_graphics("figs/mlenormal.png")
```


---
## principle of parsimony


.pull-left[

> all things
being equal, the
simpler solution
is the
best


William of Occam
]

.pull-right[
```{r, echo=FALSE}
knitr::include_graphics("figs/occams_razor.jpg")
```

]

---
## principle of parsimony

- models with fewer possible parameters
- linear models preferable to non-linear
- less assumptions
- minimally adequate models
- simpler explanations


```{r, echo=FALSE, out.width='110%', fig.align='center'}
knitr::include_graphics("figs/model_fit.png")
```


---
## best model is just a model 

- all models are wrong `r knitr::include_graphics("figs/melting_face.png")`
- some models are better than others
- we are never sure of the correct model
- the simpler the model, the better -- but not simplistic

```{r, echo=FALSE, fig.align='center', out.width='50%'}
knitr::include_graphics("figs/nailed_it.jpeg")
```


---
class: center, middle, invert
# 2. statistical distributions


---
## statistical distributions

.tiny[

distribution  | type | $E(X)$ | $\sigma^2(X)$ | usage | example
------------- | -----| -------|-----------|----------|-------------
normal | continuous | $\mu$ | $\sigma^2$ | Symmetric curve for continuous data | size distribution
binomial | discrete | $np$ | $np(1-p)$ | Number of successes in $n$ attempts | Presence or absence of species
Poisson | discrete | $\lambda$ | $\lambda$ | Independent rare events where $\lambda$ is the rate at which the event occurs in space or time | Distribution of rare species in space
Log-normal | continuous | $log(\mu)$ | $log(\sigma^2)$ | Asymmetric curve | Species abundance distribution

]

---
## continuous distributions


```{r}
df_n <- data.frame(val = rnorm(1000, mean = 0, sd = 1))
df_ln <- data.frame(val = exp(rnorm(1000)))
```

---
## continuous distributions

.pull-left[

```{r, echo=FALSE, message=FALSE, warning=FALSE, fig.align='center'}
# df <- data.frame(val = c(rnorm(1000), exp(rnorm(1000))),
#                 dist = rep(c("normal", "lognormal"), each = 1000))

df_n <- data.frame(val = rnorm(1000))
df_ln <- data.frame(val = exp(rnorm(1000)))

ggplot(df_n, aes(x = val)) + 
  geom_histogram(aes(y = ..density..), alpha = .6) +
  geom_density(color = "blue") +
  theme_void() +
  ggtitle("dnorm(x, 0, 1))")

```
]

.pull-right[

```{r, echo=FALSE, message=FALSE, warning=FALSE, fig.align='center'}
ggplot(df_ln, aes(x = val)) + 
  geom_histogram(aes(y = ..density..), alpha = .6) +
  geom_density(color = "blue") +
  theme_void() +
  ggtitle("exp(dnorm(x, 0, 1))")

```

]



---
class: center, middle, inverse
# 3. the linear model


mathematical model + uncertainty: $Y  = a + bx + \epsilon$


---
## statistical model

```{r themodel, echo=FALSE, fig.align='center', warning=FALSE}
plm <- ggplot(xy, aes(y = y1, x = x1)) + 
  scale_x_continuous(limits = c(0, 8)) +
  scale_y_continuous(limits = c(1, 30)) +
  geom_point(size = 5 ,color = cor[3]) +
  labs(x = "predictor", y = "response") +
  my_theme
plm 

```



---
## relation between variables: prediction

```{r, echo=FALSE, fig.align='center', warning=FALSE}

plm + geom_smooth(se = FALSE, method = lm, formula = 'y ~ x', color = cor[2], 
                  size = 2)

```


---
## relation between variables: extrapolation 

```{r, echo=FALSE, fig.align='center', warning=FALSE}

plm + geom_smooth(se = FALSE, method = lm, formula = 'y ~ x', color = cor[2], 
                  size = 2, fullrange = TRUE)

```


---

## the linear model

.pull-left[
$$y = a + bx$$

$$y = \alpha + \beta X + \epsilon$$

$$\epsilon \sim N (0, \sigma)$$ 

]

.pull-right[

```{r, echo=FALSE, fig.align='center', warning=FALSE}
ggplot(xy, aes(y = y1, x = x1)) + 
  geom_point(size = 5 ,color = cor[3]) +
  labs(x = "predictor", y = "response") +
  my_theme +
  geom_smooth(se = FALSE, method = lm, formula = 'y ~ x', color = cor[2], 
                  size = 2)

```
]

---
## is the response variable normal?


```{r norm, echo=FALSE, fig.align='center', out.width = '40%'}
hist(y1, probability = TRUE, ylab = "density", col = cor[3], main = "", 
     border = "grey20", las = 1)
#curve(dnorm(x, mean(y1), sd(y1)), add = TRUE, lwd = 3, col = cor[2])
```


---
## is the response variable normal?


```{r norm2, echo=FALSE, fig.align='center', out.width = '40%'}
hist(y1, probability = TRUE, ylab = "density", col = cor[3], main = "", 
     border = "grey20", las = 1)
curve(dnorm(x, mean(y1), sd(y1)), add = TRUE, lwd = 3, col = cor[2])
```


---

## what is relationship between the predictor and the response variable?


```{r xy, echo = FALSE, fig.align='center', out.width = '40%'}
plm
```

---
## assumptions


+ relationship between x and y is linear

+ normality of residuals

+ __homoscedasticity__ -- homogeneity of residuals variance

+ independence of residuals error terms


---

## parameter estimation

.pull-left[
- least squares method
- maximum likelihood

]


.pull-right[
```{r, echo=FALSE}
knitr::include_graphics("figs/noun-parameters-4125642.svg")
```
]
---
## least squares method

```{r, echo = FALSE, fig.align='center'}
plot(y1 ~ x1, data = xy, las = 1, bty = "l", cex = 2, cex.lab = 2,
     xlab = "predictor", ylab = "response", col = cor[3], pch = 19)
points(x = mean(x1), y = mean(y1), pch = 19, cex = 1)
points(x = mean(x1), y = mean(y1), pch = 1, cex = 2.2)
text("fulcrum", x = mean(x1) + .5, y = mean(y1), cex = 1.6)
```


---
## least squares method

```{r, echo = FALSE, fig.align='center'}
plot(y1 ~ x1, data = xy, las = 1, bty = "l", cex = 2, cex.lab = 2,
     xlab = "predictor", ylab = "response", col = cor[3], pch = 19)
points(x = mean(x1), y = mean(y1), pch = 19, cex = 1)
points(x = mean(x1), y = mean(y1), pch = 1, cex = 2.2)
text("fulcrum", x = mean(x1) + .5, y = mean(y1), cex = 1.6)
abline(a = 16.8 , b = -1)

```


---
## least squares method

```{r, echo = FALSE, fig.align='center'}
plot(y1 ~ x1, data = xy, las = 1, bty = "l", cex = 2, cex.lab = 2,
     xlab = "predictor", ylab = "response", col = cor[3], pch = 19)
points(x = mean(x1), y = mean(y1), pch = 19, cex = 1)
points(x = mean(x1), y = mean(y1), pch = 1, cex = 2.2)
abline(a = -10 , b = 8.07)

```


---
## least squares method

```{r, echo = FALSE, fig.align='center'}
plot(y1 ~ x1, data = xy, las = 1, bty = "l", cex = 2, cex.lab = 2,
     xlab = "predictor", ylab = "response", col = cor[3], pch = 19)
points(x = mean(x1), y = mean(y1), pch = 19, cex = 1)
points(x = mean(x1), y = mean(y1), pch = 1, cex = 2.2)
abline(lm(y1 ~ x1))
```


---

## linear model in R

.tiny[

```{r}
mod <- lm(y1 ~ x1)
summary(mod)
```

]


---
## uncertainty in the estimate

.tiny[

$$y = 1.63 + 4.08x + \epsilon$$ 

]

estimation of coefficients

.tiny[
```{r}
coef(mod)
```
]

confidence interval

.tiny[
```{r}
confint(mod)
```
]

---
## linear model residue

```{r, echo = FALSE, fig.align='center'}
plot(y1 ~ x1, data=xy, las=1, bty="l", cex=2, col=cor[3],
     #xaxt="n", yaxt="n",
     xlab="predictor", ylab="response", pch=19)
segments(x0=x1, x1=x1, y0=y1, y1=prev, lty=2)
points(y1 ~ x1, data=xy, las=1, bty="l", cex=.7, pch=19, col=cor[3])
abline(lm(y1 ~ x1), lwd=2, col=cor[2], cex=2)
```


---
## linear model variance partitioning

sum of squares from the linear model


$$SS_{total} = SS_{between} + SS_{error}$$ 



---

## total sum of squares


.pull-left[
$$SS_{total} = \sum_{i=1}^n (y_{i} - \bar{y})^2$$
]


.pull-right[
```{r, echo = FALSE, fig.align='center'}
plot(y1 ~ x1, data=xy, las=1, bty="l", cex=2, col=cor[3],
     #xaxt="n", yaxt="n",
     xlab="predictor", ylab="response", pch=19)
abline(h=mean(y1), col=cor[2], lwd=2)
segments(x0=x1, x1=x1, y0=y1, y1=mean(y1), lty=2)
points(y1 ~ x1, data=xy, las=1, bty="l", cex=.7, pch=19, col=cor[3])
#abline(lm(y1 ~ x1), lwd=2, col='grey30')
```
]

---
## total sum of squares


$$SS_{total} = \sum_{i=1}^n (y_{i} - \bar{y})^2$$


```{r, echo=FALSE, fig.width=4}
dev <- (y1 - mean(y1)) ^ 2
devtot <- sum(dev)
# tab.dev <-  data.frame(x1, y1, y_medio=mean(y1), dif_yi_y=y1 - mean(y1), 
#                        desvio=dev)
sq.tot <- round(sum(dev),2)
# knitr::kable(round(tab.dev, 2))
```   

$$SS_{total} = `r sq.tot`$$ 


---
## residual sum of squares

.pull-left[

$$SS_{error} = \sum_{i=1}^n (y_{i} - \hat{y}_i)^2$$

]


.pull-right[
```{r, echo = FALSE, fig.align='center'}
plot(y1 ~ x1, data=xy, las=1, bty="l", cex=2, col=cor[3],
     #xaxt="n", yaxt="n",
     xlab="predictor", ylab="response", pch=19)
abline(mod, col=cor[2], lwd=2)
segments(x0=x1, x1=x1, y0=y1, y1=prev, lty=2)
points(y1 ~ x1, data=xy, las=1, bty="l", cex=.7, pch=19, col=cor[3])
#abline(lm(y1 ~ x1), lwd=2, col='grey30')
```
]

---
## residual sum of squares


$$SS_{error} = \sum_{i=1}^n (y_{i} - \hat{y}_i)^2$$


```{r, echo=FALSE, fig.width=4}
devres <- residuals(mod)^2
tab.devres <- data.frame(x1, y1, residuo=residuals(mod), desvio=devres)
#kable(round(tab.devres, 2))
sq.er <- round(sum(devres),2)
sq.mod <- sq.tot-sq.er
```   

$$SS_{error} = `r sq.er`$$


---
## model sum of squares


$$SS_{total} = SS_{between} + SS_{error}$$ 



$$SS_{between} = SS_{total} - SS_{error}$$


$$SS_{between} = `r sq.tot` - `r sq.er`$$


$$SS_{between} = `r sq.tot-sq.er`$$



---
## variance partitioning


$$SS_{total} = `r sq.tot`$$


$$SS_{between} = `r sq.mod`$$


$$SS_{error} = `r sq.er`$$


---
## variance partitioning

__anova table__


.tiny[

```{r}
anova(mod)
```

]

---
## coefficient of determination $R^{2}$


$$R^2 = \frac{SS_{between}}{SS_total}$$


$$R^2 = \frac{`r sq.mod`}{`r sq.tot`}$$


$$R^2 = `r round(sq.mod/sq.tot, 4)`$$


---
## coefficient of determination $R^{2}$

.tiny[

```{r}
summary(mod)
```

]

---
## todo


`r icons::fontawesome("laptop-code")`

- `lm` tutorial


- `git add`, `commit`, and `push` of the day



---
class: center, middle

# ¡Thanks!

<center>
`r icons::icon_style(icons::fontawesome("paper-plane", style = "solid"), scale = 1, fill = "#A70000")` [saramortara@gmail.com](mailto:saramortara@gmail.com) | [andreasancheztapia@gmail.com](mailto:andreasancheztapia@gmail.com) 


`r icons::icon_style(icons::fontawesome("twitter", style = "brands"), scale = 1, fill = "#A70000")` [@MortaraSara](https://twitter.com/MortaraSara) | [@SanchezTapiaA](https://twitter.com/SanchezTapiaA) 

`r icons::icon_style(icons::fontawesome("github", style = "brands"), scale = 1, fill = "#A70000")``r icons::icon_style(icons::fontawesome("gitlab", style = "brands"), scale = 1, fill = "#A70000")`  [saramortara](http://github.com/saramortara) |  [andreasancheztapia](http://github.com/andreasancheztapia) 



