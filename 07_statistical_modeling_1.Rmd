---
title: "Statistical modeling in R (part 1)"
description: |
author:
  - name: Sara Mortara
    affiliation: re.green
    url: {}
    orcid_id: 0000-0001-6221-7537
  - name: Andrea Sánchez-Tapia
    orcid_id: 0000-0002-3521-4338
    affiliation: ¡liibre!
    affiliation_url: {}
    url: https://andreasancheztapia.netlify.app
citation_url: https://scientific-computing.netlify.app/07_statistical_modeling_1.html
date: 2022-07-14
---


```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = FALSE)
```


# Slides

```{r slides}
xaringanExtra::embed_xaringan("07_statistical_modeling_1/07_slides.html")
```


# Tutorial

+ Find [here](07_statistical_modeling_1/07_tutorial.html) the complete tutorial
+ Find [here](https://gitlab.com/liibre/2022_scientific_computing_intro/-/raw/main/R/05_lm.R) the R code from the tutorial
+ Download [here](https://gitlab.com/liibre/2022_scientific_computing_intro/-/raw/main/data/raw/crawley_regression.csv?inline=false) the data used in the R script and tutorial

# Libraries

To run the code from the slides you'll need to have installed the package __fitdistrplus__.

```
install.packages("fitdistrplus")
```


# Basic references

Anderson, D. and Burnham, K., 2004. Model selection and multi-model inference. Second. NY: Springer-Verlag, 63(2020), p.10.

Bolker, B.M., 2008. Ecological models and data in R. In Ecological Models and Data in R. Princeton University Press.

Crawley, M.J., 2012. The R book. John Wiley & Sons.

Delignette-Muller, M.L. and Dutang, C., 2015. fitdistrplus: An R package for fitting distributions. Journal of statistical software, 64, pp.1-34.

Dushoff, J., Kain, M.P. and Bolker, B.M., 2019. I can see clearly now: reinterpreting statistical significance. Methods in Ecology and Evolution, 10(6), pp.756-759.

Fox, G.A., Negrete-Yankelevich, S. and Sosa, V.J. eds., 2015. Ecological statistics: contemporary theory and application. Oxford University Press, USA.

Gotelli, N.J. and Ellison, A.M., 2004. A primer of ecological statistics (Vol. 1). Sunderland: Sinauer Associates.


Hilborn, R. and Mangel, M., 2013. The ecological detective. In The Ecological Detective. Princeton University Press.

Kuhn, M. and Silge, J., 2022. [Tidy Modeling with R](https://www.tidymodels.org/books/tmwr/). "O'Reilly Media, Inc.".
