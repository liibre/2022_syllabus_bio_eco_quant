---
title: "Data visualization with R"
description: |
author:
  - name: Sara Mortara
    affiliation: re.green
    url: {}
    orcid_id: 0000-0001-6221-7537
  - name: Andrea Sánchez-Tapia
    orcid_id: 0000-0002-3521-4338
    affiliation: ¡liibre!
    affiliation_url: {}
    url: https://andreasancheztapia.netlify.app
citation_url: https://scientific-computing.netlify.app/06_data_visualization.html
date: 2022-07-20
---


```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = FALSE)
```



# Slides

```{r slides}
xaringanExtra::embed_xaringan("06_data_visualization/06_slides.html")
```

# Libraries

To run the code from the slides you'll need, in addition to __ggplot2__, to have installed the packages: __patchwork__ and __ggridges__. 

```
packages <- c("patchwork", "ggridges)
for (package in packages) {
  if (!package %in% installed.packages()) install.packages(package)
}
```

# Rmarkdown

Find [here](https://gitlab.com/liibre/2022_scientific_computing_intro/-/raw/main/docs/graphics.Rmd) the Rmarkdown file to follow and generate a graphic.

