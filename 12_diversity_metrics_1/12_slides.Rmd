---
title: "Introduction to biological diversity analyses"
subtitle: "Serrapilheira/ICTP-SAIFR Training Program in Quantitative Biology and Ecology"
author: 
  - "Andrea Sánchez-Tapia & Sara Mortara"
date: '28 July 2022'
output: 
  xaringan::moon_reader:
    lib_dir: libs
    css: xaringan-themer.css
    nature:
      highlightStyle: github
      highlightLines: true
      countIncrementalSlides: false
      ratio: '16:9'
---

```{r setup, include=FALSE}
options(htmltools.dir.version = FALSE)
```

```{r xaringanExtra-clipboard, echo=FALSE}
htmltools::tagList(
  xaringanExtra::use_clipboard(
    button_text = "Copy code <i class=\"fa fa-clipboard\"></i>",
    success_text = "Copied! <i class=\"fa fa-check\" style=\"color: #90BE6D\"></i>",
    error_text = "Not copied :/ <i class=\"fa fa-times-circle\" style=\"color: #F94144\"></i>"
  ),
  rmarkdown::html_dependency_font_awesome()
)
xaringanExtra::use_share_again()
```

```{r xaringan-themer, include = FALSE}
library(xaringanthemer)
co <- "#A70000"
xaringanthemer::style_mono_accent(
  base_color = co,
  #  primary_color = "#A70000",
  #secondary_color = "#ff0000",
  white_color = "white",
  colors = c(
    red = "#A70000",
    purple = "#88398a",
    orange = "#ff8811",
    green = "#136f63",
    blue = "#4B4FFF",
    white = "#FFFFFF",
    black = "#181818"
  ),
  header_font_google = google_font("Roboto Condensed"),
  text_font_google   = google_font("Roboto Condensed", "300", "300i"),
  code_font_google   = google_font("Ubuntu Mono"),
  text_font_size = "30px",
  code_font_size = "30px",
)
xaringanExtra::use_share_again()
```

## Ecological community data as multivariate data 

```{r, out.width=480, echo = F, fig.align="center"}
knitr::include_graphics("figs/4thcorner.jpg")
```

---

## CESTES database

```{r, fig.align='center', echo=FALSE, out.width=800}
knitr::include_graphics("figs/cestesdatabase.png")
```


[Jeliazkov et al 2020 Sci Data](https://doi.org/10.1038/s41597-019-0344-7)

---
## Today

+ Short exercises 

+ __Pseudocode:__ a _verbal model_ of the steps you need to take to solve a problem

+ Code

+ Collaborative notes and answers [__here__](https://hackmd.io/@andreasancheztapia/comp_methods/edit)

---
## Species abundance, frequency, richness

1. Which are the 5 most abundant species overall in the dataset?

2. How many species are there in each site? (Richness)

3. Which the species that is most abundant in each site?


---
## Diversity indices


+ Shannon diversity index


$$
H = - \sum_{n=1}^{S}p_i \ln p_i
$$

+ Simpson's diversity index would be
$$
1 - \sum p_i^2
$$
+ Inverse Simpson 
$$
1/\sum p_i^2 
$$
---
## Creating functions

+ Create code to calculate Shannon and Simpson diversity in comm dataset (leave in notes!)

+ Let's create _functions_ from this code


---


```{r}
#| eval: true
comm <- read.csv("data/raw/cestes/comm.csv")
```

```{r, echo = FALSE}
if (interactive()) 
  comm <- read.csv("12_diversity_metrics_1/data/raw/cestes/comm.csv")

```

```{r}
dim(comm)
head(comm[,1:6])
```

