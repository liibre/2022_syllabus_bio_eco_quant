---
title: "Statistical modeling (part 2)"
author: "Sara Mortara & Andrea Sánchez-Tapia"
institute: "re.green | ¡liibre!"
date: "2022-07-20"
output:
  xaringan::moon_reader:
    lib_dir: libs
    css: xaringan-themer.css
    nature:
      highlightStyle: github
      highlightLines: true
      countIncrementalSlides: false
      ratio: '16:9'
self-contained: true
---


```{css css-config, echo = FALSE}
.tiny .remark-code { /*Change made here*/
  font-size: 50% !important;
}
```

```{r setup, include=FALSE}
options(htmltools.dir.version = FALSE)
```

```{r xaringanthemer, include = FALSE}
library(xaringanthemer)
xaringanthemer::style_mono_accent(
  base_color = "#A70000",
  #  primary_color = "#A70000",
  #secondary_color = "#ff0000",
  white_color = "white",
  colors = c(
    red = "#A70000",
    purple = "#88398a",
    orange = "#ff8811",
    green = "#136f63",
    blue = "#4B4FFF",
    white = "#FFFFFF",
    black = "#181818"
  ),
  header_font_google = google_font("Roboto Condensed"),
  text_font_google   = google_font("Roboto Condensed", "300", "300i"),
  code_font_google   = google_font("Ubuntu Mono"),
  text_font_size = "30px",
  code_font_size = "30px",
  )
xaringanExtra::use_share_again()

# clipboard
htmltools::tagList(
  xaringanExtra::use_clipboard(
    button_text = "Copy code <i class=\"fa fa-clipboard\"></i>",
    success_text = "Copied! <i class=\"fa fa-check\" style=\"color: #90BE6D\"></i>",
    error_text = "Not copied 😕 <i class=\"fa fa-times-circle\" style=\"color: #F94144\"></i>"
  ),
  rmarkdown::html_dependency_font_awesome()
)

```



```{r generating-data, echo=FALSE, message=FALSE}
library(dplyr)
library(lme4)
library(merTools)
library(ggplot2)


title_size <- 20
text_size <- 18
my_theme <- theme_classic() +
  theme(axis.title.x = element_text(size = title_size),
        axis.text.x = element_text(size = text_size),
        axis.title.y = element_text(size = title_size),
        axis.text.y = element_text(size = text_size), 
        plot.title = element_text(size = 40),
        legend.title = element_text(size = 14), #change legend title font size
        legend.text = element_text(size = 12))

```


## today 

1. glm

2. glmm

3. model selection tutorial

---
class: middle, center, inverse

# 1. glm


---
## road map

```{r, echo=FALSE, fig.align='center'}
knitr::include_graphics("figs/stats_diagram_bolker.png")
```

---

## linear models


+ linear relationship between x and y


+ variances are equal across all predicted values of the response (homoscedatic)


+ errors are normally distributed


+ errors are independent


---


## generalized linear models


+ a linear mean (of your making)


+ a link function (like an ‘internal’ transformation)


+ an error structure


---
## link function

links your mean function to the scale of the observed data

- response variable $Y$ and explanatory variable(s) $X$

- linear function: $\beta_0 + \beta_1 X$

- $E(Y) = g^{-1}\left(\beta_0 + \beta_1 X\right)$

- the function $g(\cdot)$ is known as the link function

- $g^{-1}(\cdot)$ denotes the inverse of $g(\cdot)$


---
## back to linear regression

`lm` as a special case of `glm`



```{r}
df <- read.csv("data/raw/crawley_regression.csv")

lm(growth ~ tannin, data = df)

```

---
## back to linear regression

`family`: error structure __and__ the link function


```{r}
glm(growth ~ tannin, data = df, family = gaussian(link = identity))
```

---
## link function in gaussian family

The default link function for the normal (Gaussian) distribution is the identity, i.e. for mean  $\mu$ we have:

$$\mu = \beta_0 + \beta_1X$$


---
## Poisson regression

- count data (positive)
- still getting back to glm
- __Gaussian__ error structure and __identity__ link


.pull-left[
$$Y = \beta_0 + \beta_1X + \epsilon$$
$$\epsilon \sim N(0, \sigma^2)$$

]
-
.pull-right[
$$Y \sim N(\mu, \sigma^2)$$

$$\mu = \beta_0 + \beta_1$$

]

---
## Poisson regression

.pull-left[
+ Family: Gaussian

+ Link: identity

$$Y \sim N(\mu, \sigma^2)$$

$$\mu = \beta_0 + \beta_1X$$

]


.pull-right[
+ Family: Poisson

+ Link: log

$$Y \sim Pois(\lambda)$$

$$log \lambda = \beta_0 + \beta_1X$$

$$\lambda = \exp^{\beta_0 + \beta_1X}$$

]

- we will still fit straight lines
- linear for the __log__ transformed observations

---
## Poisson distribution

+ discrete variable, defined on the range [0, 1, $\infty$]

+ mean = $\lambda$

+ variance = $\lambda$

<center>

```{r, echo=FALSE, fig.width=3.5, fig.height=3.5}
p1 <- rpois(n = 1000, lambda = 1)
p2 <- rpois(n = 1000, lambda =  5)
p3 <- rpois(n = 1000, lambda = 10)

hist(p1, main = "1", xlim = c(0, 20))
hist(p2, main = "5", xlim = c(0, 20))
hist(p3, main = "10", xlim = c(0, 20))

```

- as the mean increases, the variance increases also --> heteroscedascity


---
## Poisson regression: cuckoo example

How does nestling mass affect begging rates between the different species?

.pull-left[

```{r, echo=FALSE, out.width='80%'}
knitr::include_graphics("figs/03-cuckoo.png")
```

[Kilner et al 1999](https://www.nature.com/articles/17746)

]

.pull-right[

+ __Mass__: nestling mass of chick in grams
+ __Beg__: begging calls per 6 secs
+ __Species__: Warbler or Cuckoo

]


---
## read the data

```{r}
cuckoo <- read.csv("data/raw/valletta_cuckoo.csv")
summary(cuckoo)
```


---
## understanding the data

```{r, echo=FALSE, fig.align='center'}
ggplot(cuckoo, aes(x = Mass, y = Beg, colour = Species)) + 
  geom_point(size = 3) +
  my_theme
```


---
## let's fit a lm

```{r}
# Fitting a model with an interaction term
cuckoo_lm <- lm(Beg ~ Mass * Species, data = cuckoo)
```

---
## inspecting the lm

<center>

```{r, echo=FALSE, fig.width=7, fig.height=7}
par(mfrow = c(2, 2))
plot(cuckoo_lm, pch = 19, col = 'darkgrey')
par(mfrow = c(1, 1))
```

---
## fitting a Poisson glm

```{r}
cuckoo_glm <- glm(Beg ~ Mass * Species, data = cuckoo,
           family = poisson(link = log))
```


---
## understanding the model

+ model with interaction term:
$$log\lambda = \beta_0 + \beta_1M_1 + \beta_2S_i + \beta_3M_iS_i$$

$M_i$ = nestling mass
$S_i$ = categorical explanatory variable 1 = warbler

+ cuckoo:  $S_i$ = 0
+ warbler: $S_i$ = 1

---
## understanding the model

$$log\lambda = \beta_0 + \beta_1M_1 + \beta_2S_i + \beta_3M_iS_i$$

cuckoo: 
$$log\lambda = \beta_0 + \beta_1M_1$$

warbler: 
$$log\lambda = \beta_0 + \beta_1M_1 + \beta_2S_i + \beta_3M_iS_i$$
$$log\lambda = (\beta_0 + \beta_2) + (\beta_1 + \beta_3)M_i$$


---
## cuckoo glm


.tiny[
```{r}
summary(cuckoo_glm)
```
]

---
## inspecting the glm


<center>

```{r, echo=FALSE, fig.width=14, fig.height=7}
par(mfrow = c(1, 2))
plot(cuckoo_lm, 1)
plot(cuckoo_glm, 1)
par(mfrow = c(1, 1))
```



---
## creating the Poisson regression line

```{r}
newdata <- expand.grid(Mass = seq(min(cuckoo$Mass), max(cuckoo$Mass), length.out = 200),
                       Species = unique(cuckoo$Species))
newdata$Beg <- predict(cuckoo_glm, newdata, type = 'response')

```


---
## creating the Poisson regression line

.pull-left[

```{r}
p <- ggplot(mapping = aes(x = Mass, y = Beg, colour = Species)) + 
  geom_point(data = cuckoo) + 
  geom_line(data = newdata) +
  my_theme
```

]

.pull-right[
```{r, echo=FALSE, ig.align='center'}
p
```

]

---
class: middle, center
# bestiary of error distributions


---

.tiny[

Response variable | Error distribution | Canonical link function | 
------------- | -----| -------|
Continuous positive and negative values | Gaussian/Normal | Identity |
Counts | Poisson | Log | 
Counts with over-dispersion | Negative Binomial, Quasi-Poisson | Log Log | 
Proportions (no. successes/total trials) | Binomial | Logit |
Binary (male/female, alive/dead) | Binomial (Bernoulli) | Logit | 
Proportions or binary with overdispersion | Quasi-Binomial | Logit | 
Time to event (germination, death) | Gamma | Inverse | 

]


---
## parameter estimation

- maximum likelihood
- quasi-likelihood
- Bayesian approaches


---
class: middle, center, inverse

# 2. glmm


---
## glmm

+ modeling variance

+ non-independent data (violates the independence of residuals assumption): 
 + blocks (spatial, temporal, genetic)
 + individual level effects (repeated measures)

+ zero-inflated modes


---
## glmm: bacterial growth

which of four growth media is best for rearing large populations of anthrax?

```{r, echo=FALSE, out.width='70%', fig.align='center'}
knitr::include_graphics("figs/experimental_design.png")
```

---
## reading the data and creating a lm

```{r}
bac <- read.csv("data/raw/valletta_bac.csv")

bac$media <- as.factor(bac$media)
bac$cabinet <- as.factor(bac$cabinet)

bac_lm <- lm(growth ~ media, data = bac)
```


---
## creating a lm

.tiny[

```{r}
summary(bac_lm)
```

]

---
## taking cabinet effect into account

```{r}
bac_lm2 <- lm(growth ~ media + cabinet, data = bac)
```



---
## taking cabinet effect into account

.tiny[

```{r}
summary(bac_lm2)
```

]


---
## creating a glmm


.pull-left[
fixed effect

__media__: 

+ we chose the media to be tested

+ each media has a specific identity

+ we want to estimate the differences in bacterial growth between different media
]

.pull-right[
random effect

__cabinet__: 
+ we don’t care about the identity of each cabinet

+ each cabinet is sampled from a population of possible cabinets 

+ we just want to predict and absorb the variance in bacterial growth rate explained by cabinet


]

---
## glmm using lme4 package

+ the intercept of our linear model will vary according to cabinet

$$Y_i = \beta_0 + \beta_1x_{i1} + \beta_2x_{i2} + \beta_3x_{i3}+  \gamma C_i + \epsilon_i$$ 

$x_{i∗}$ = dummy variables corresponding to levels of media



```{r}
bac_lmer <- lmer(growth ~ media + (1 | cabinet), data = bac)
```




---
## glmm using lme4 package


.tiny[

```{r}
summary(bac_lmer)
```
]

---
## visualizing the glmm

[merTools package](https://cran.r-project.org/web/packages/merTools/vignettes/merToolsIntro.html)


```{r}
feEx <- FEsim(bac_lmer, 1000)
```


---
## visualizing the glmm

.pull-left[

```{r}
pfe <- plotFEsim(feEx) +
  theme_bw() + labs(title = "Coefficient Plot",
                    x = "Median Effect Estimate", y = "Evaluation Rating")
```
]

.pull.right[

```{r, echo=FALSE}
pfe
```

]


---
## todo


`r icons::fontawesome("laptop-code")`

- model selection tutorial

- run code from the slides

- `git add`, `commit`, and `push` of the day



---
class: center, middle

# ¡Thanks!

<center>
`r icons::icon_style(icons::fontawesome("paper-plane", style = "solid"), scale = 1, fill = "#A70000")` [saramortara@gmail.com](mailto:saramortara@gmail.com) | [andreasancheztapia@gmail.com](mailto:andreasancheztapia@gmail.com) 


`r icons::icon_style(icons::fontawesome("twitter", style = "brands"), scale = 1, fill = "#A70000")` [@MortaraSara](https://twitter.com/MortaraSara) | [@SanchezTapiaA](https://twitter.com/SanchezTapiaA) 

`r icons::icon_style(icons::fontawesome("github", style = "brands"), scale = 1, fill = "#A70000")``r icons::icon_style(icons::fontawesome("gitlab", style = "brands"), scale = 1, fill = "#A70000")`  [saramortara](http://github.com/saramortara) |  [andreasancheztapia](http://github.com/andreasancheztapia) 




