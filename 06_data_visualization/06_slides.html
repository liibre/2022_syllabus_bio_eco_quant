<!DOCTYPE html>
<html lang="" xml:lang="">
  <head>
    <title>Graphics with R</title>
    <meta charset="utf-8" />
    <meta name="author" content="Sara Mortara &amp; Andrea Sánchez-Tapia" />
    <meta name="date" content="2022-07-13" />
    <script src="libs/clipboard/clipboard.min.js"></script>
    <link href="libs/shareon/shareon.min.css" rel="stylesheet" />
    <script src="libs/shareon/shareon.min.js"></script>
    <link href="libs/xaringanExtra-shareagain/shareagain.css" rel="stylesheet" />
    <script src="libs/xaringanExtra-shareagain/shareagain.js"></script>
    <link href="libs/xaringanExtra-clipboard/xaringanExtra-clipboard.css" rel="stylesheet" />
    <script src="libs/xaringanExtra-clipboard/xaringanExtra-clipboard.js"></script>
    <script>window.xaringanExtraClipboard(null, {"button":"Copy code <i class=\"fa fa-clipboard\"><\/i>","success":"Copied! <i class=\"fa fa-check\" style=\"color: #90BE6D\"><\/i>","error":"Not copied 😕 <i class=\"fa fa-times-circle\" style=\"color: #F94144\"><\/i>"})</script>
    <link href="libs/font-awesome/css/all.css" rel="stylesheet" />
    <link href="libs/font-awesome/css/v4-shims.css" rel="stylesheet" />
    <link rel="stylesheet" href="xaringan-themer.css" type="text/css" />
  </head>
  <body>
    <textarea id="source">
class: center, middle, inverse, title-slide

.title[
# Graphics with R
]
.author[
### Sara Mortara &amp; Andrea Sánchez-Tapia
]
.institute[
### re.green | ¡liibre!
]
.date[
### 2022-07-13
]

---



&lt;style type="text/css"&gt;
.tiny .remark-code { /*Change made here*/
    font-size: 50% !important;
}
&lt;/style&gt;






## today

- graphics' good practices

- graphics grammar

- examples with `ggplot2` 


---
class: middle
## data visualization has many don'ts

.pull-left[
&lt;img src="06_slides_files/figure-html/unnamed-chunk-1-1.png" width="400" style="display: block; margin: auto;" /&gt;
]

--

.pull-right[
&lt;img src="06_slides_files/figure-html/unnamed-chunk-2-1.png" width="400" style="display: block; margin: auto;" /&gt;
]

---

## many

.pull-left[
&lt;img src="06_slides_files/figure-html/pie3d-1.png" width="400" style="display: block; margin: auto;" /&gt;
]

--

.pull-right[

&lt;img src="06_slides_files/figure-html/unnamed-chunk-3-1.png" width="400" style="display: block; margin: auto;" /&gt;

]

---
## there's always a better option than a pie chart

.pull-left[

&lt;img src="06_slides_files/figure-html/barplot-1.png" width="400" style="display: block; margin: auto;" /&gt;

]

--

.pull-right[

&lt;img src="06_slides_files/figure-html/barplot2-1.png" width="400" style="display: block; margin: auto;" /&gt;

]

---
## barplots are not always very informative

&lt;img src="06_slides_files/figure-html/unnamed-chunk-4-1.png" width="400" style="display: block; margin: auto;" /&gt;

---
## better with error bars...

&lt;img src="06_slides_files/figure-html/unnamed-chunk-5-1.png" width="400" style="display: block; margin: auto;" /&gt;

---
## but maybe don't even make a barplot

&lt;img src="06_slides_files/figure-html/unnamed-chunk-6-1.png" width="400" style="display: block; margin: auto;" /&gt;

---
##  ...or maybe don't even make a graph


&lt;img src="06_slides_files/figure-html/unnamed-chunk-7-1.png" width="400" style="display: block; margin: auto;" /&gt;

---
## make a table 

|Treatment| Effect|
|-----------|--------|
|1 |6.7 `\(\pm\)` 0.4 | 
|2 |12.3 `\(\pm\)` 0.98 | 

---
## or say it in the text 

&lt;center&gt;
The effect of treatment 2 (12.3 ± 0.98) was
higher than treatment 1 (6.7 ± 0.4).

---
## do not fool your reader


&lt;img src="figs/0176-fox-original.jfif.jpeg" width="600" style="display: block; margin: auto;" /&gt;

[Free range statistics](http://freerangestats.info/blog/2020/04/06/crazy-fox-y-axis)


---
## do not fool your reader

&lt;img src="figs/fox_corrected.png" width="600" style="display: block; margin: auto;" /&gt;

[Free range statistics](http://freerangestats.info/blog/2020/04/06/crazy-fox-y-axis)


---
## some basic tips in general

+ only make plots when you really need to

--

+ don't spend more ink and colors than you need to

--

+ don't fool your reader (no y-axis tampering, no undue transformation)

--

+ show error measures


---
## some basic tips in R


+ make your y-axis labels horizontal and visible

--

+ avoid colored backgrounds

--

+ use dots for scatterplots

--

+ add names to `xlab` and `ylab`

--

+ save to png and pdf formats


---

## the grammar of graphics

.pull-left[
- a theoretical deconstruction for data graphics

- foundation for software (ggplot2, [vega-lite](https://vega.github.io/vega-lite/))

- how to design the system that allow building beautiful, correct graphics
]

.pull-right[

&lt;img src="figs/grammar_of_graphics.jpg" width="300" style="display: block; margin: auto;" /&gt;

]


---
background-image: url("figs/logo.png")
background-position: 98% 2%
background-size: 100px

## the grammar of graphics: ggplot2


Hadley Wickham, Danielle Navarro, and Thomas Lin Pedersen

.pull-left[

- [https://ggplot2-book.org/](https://ggplot2-book.org/) 

- not a cookbook ([R Graphics Cookbook, Winston Chang](https://r-graphics.org/))

- underlying theory

- power to tailor any plot

]


.pull-right[
&lt;img src="figs/book.jpg" width="200" style="display: block; margin: auto;" /&gt;

]

---

## graphics


Type of graphic | R base | 
----------|------------|
barplot | barplot() | 
histogram | histogram() | 
density | plot(density()) | 
quantile-quantile |	qqnorm()	| 
boxplot | boxplot() | 
scatterplot | plot() | 


---
## grammar: decompose graphics into its constituints

- theme
- coordinates
- facets
- geometries
- scales
- statistics
- mapping
- data
    - it will require data in a variable/colum observation/row format



---
class: inverse, middle, center 

background-image: url("figs/logo.png")
background-position: 98% 2%
background-size: 100px/

## ggplot2


---

## barplot

- ToothGrowth data sets
- the effect of Vitamin C on Tooth growth in Guinea pigs
- len: Tooth length
- dose: Dose in milligrams (0.5, 1, 2)


.tiny[


```r
library(ggplot2)

df &lt;- data.frame(dose = c("D0.5", "D1", "D2"),
                 len = c(4.2, 10, 29.5))
df
```

```
##   dose  len
## 1 D0.5  4.2
## 2   D1 10.0
## 3   D2 29.5
```


]


---
## create a barplot



.tiny[



```r
bar1 &lt;- ggplot(data = df, mapping = aes(x = dose, y = len)) +
  geom_bar(stat = "identity")
bar1
```

&lt;img src="06_slides_files/figure-html/unnamed-chunk-13-1.png" width="400" style="display: block; margin: auto;" /&gt;

]


---
## create a barplot



.tiny[



```r
# Data and mapping can be given both as global (in ggplot()) or per layer
bar1 &lt;- ggplot() +
  geom_bar(data = df, mapping = aes(x = dose, y = len), 
           stat = "identity")
bar1
```

&lt;img src="06_slides_files/figure-html/unnamed-chunk-14-1.png" width="400" style="display: block; margin: auto;" /&gt;

]


---
## create a horizontal barplot



```r
bar1 + coord_flip()
```

&lt;img src="06_slides_files/figure-html/unnamed-chunk-15-1.png" width="400" style="display: block; margin: auto;" /&gt;



---

## change the bar color


```r
ggplot(data = df, mapping = aes(x = dose, y = len)) +
  geom_bar(stat = "identity", fill = "red")
```

&lt;img src="06_slides_files/figure-html/unnamed-chunk-16-1.png" width="400" style="display: block; margin: auto;" /&gt;


---

## change the bar color using the hexcode


```r
ggplot(data = df, mapping = aes(x = dose, y = len)) +
  geom_bar(stat = "identity", fill = "#A70000")
```

&lt;img src="06_slides_files/figure-html/unnamed-chunk-17-1.png" width="400" style="display: block; margin: auto;" /&gt;


---
## changing the theme


.tiny[


```r
ggplot(data = df, mapping = aes(x = dose, y = len)) +
  geom_bar(stat = "identity", fill = "#A70000") + 
  theme_classic()
```

&lt;img src="06_slides_files/figure-html/unnamed-chunk-18-1.png" width="400" style="display: block; margin: auto;" /&gt;

]



---
## defining a parameters for a theme


```r
title_size &lt;- 18
text_size &lt;- 16
my_theme &lt;- theme_classic() +
  theme(axis.title.x = element_text(size = title_size),
        axis.text.x = element_text(size = text_size),
        axis.title.y = element_text(size = title_size),
        axis.text.y = element_text(size = text_size))
```


---
## changing labels and size


.tiny[


```r
ggplot(data = df, mapping = aes(x = dose, y = len)) +
  geom_bar(stat = "identity", fill = "#A70000") + 
  labs(x = "Dose", y = "Tooth length (cm)") +
  my_theme
```

&lt;img src="06_slides_files/figure-html/unnamed-chunk-20-1.png" width="400" style="display: block; margin: auto;" /&gt;

]


---
## do we need color?


.tiny[


```r
ggplot(data = df, aes(x = dose, y = len, fill = dose)) +
  geom_bar(stat = "identity") + 
  labs(x = "Dose", y = "Tooth length (cm)") +
  scale_fill_manual(values = c("#E69F00", "#56B4E9", "#CC79A7")) +
  my_theme
```

&lt;img src="06_slides_files/figure-html/unnamed-chunk-21-1.png" width="400" style="display: block; margin: auto;" /&gt;

]

---
## barplot with subcategories

Adding with each of two delivery methods: 
- orange juice (OJ)
- ascorbic acid (VC)

.tiny[

```r
df2 &lt;- data.frame(supp = rep(c("VC", "OJ"), each = 3),
                  dose = rep(c("D0.5", "D1", "D2"), 2),
                  len = c(6.8, 15, 33, 4.2, 10, 29.5))
head(df2)
```

```
##   supp dose  len
## 1   VC D0.5  6.8
## 2   VC   D1 15.0
## 3   VC   D2 33.0
## 4   OJ D0.5  4.2
## 5   OJ   D1 10.0
## 6   OJ   D2 29.5
```
]

---
## barplot with subcategories

.tiny[

```r
ggplot(data = df2, aes(x = dose, y = len, fill = supp)) +
  geom_bar(stat = "identity") +
  scale_fill_brewer(palette = "Paired") +
  my_theme
```

&lt;img src="06_slides_files/figure-html/unnamed-chunk-23-1.png" width="400" style="display: block; margin: auto;" /&gt;

]

---
## scatterplot


.tiny[


```r
data("faithful")
# Basic scatterplot
ggplot(data = faithful, 
       mapping = aes(x = eruptions, y = waiting)) + 
  geom_point() + my_theme
```

&lt;img src="06_slides_files/figure-html/unnamed-chunk-24-1.png" width="400" style="display: block; margin: auto;" /&gt;
]

---

## scatterplot


.tiny[


```r
# Data and mapping can be given both as global (in ggplot()) or per layer
ggplot() + 
  geom_point(mapping = aes(x = eruptions, y = waiting),
             data = faithful) + my_theme
```

&lt;img src="06_slides_files/figure-html/unnamed-chunk-25-1.png" width="400" style="display: block; margin: auto;" /&gt;
]

---

## aesthetic linked to data aes()

.tiny[


```r
ggplot(faithful) + 
  geom_point(aes(x = eruptions, y = waiting, colour = eruptions &lt; 3)) +
  my_theme
```

&lt;img src="06_slides_files/figure-html/unnamed-chunk-26-1.png" width="400" style="display: block; margin: auto;" /&gt;
]


---
## set color to a value outside `aes()`

.tiny[

```r
ggplot(faithful) + 
  geom_point(aes(x = eruptions, y = waiting),
             colour = 'steelblue') + 
  my_theme
```

&lt;img src="06_slides_files/figure-html/unnamed-chunk-27-1.png" width="400" style="display: block; margin: auto;" /&gt;
]

---
## histogram


.tiny[


```r
ggplot(faithful) + 
  geom_histogram(aes(x = eruptions), bins = 40) +
  my_theme
```

&lt;img src="06_slides_files/figure-html/unnamed-chunk-28-1.png" width="400" style="display: block; margin: auto;" /&gt;
]


---
## scales

.tiny[


```r
ggplot(mpg) + 
  geom_point(aes(x = displ, y = hwy)) + 
  scale_x_continuous(breaks = c(3, 5, 6)) + 
  scale_y_continuous(trans = 'log10') +
  my_theme
```

&lt;img src="06_slides_files/figure-html/unnamed-chunk-29-1.png" width="400" style="display: block; margin: auto;" /&gt;

]

---
## facets


.tiny[


```r
ggplot(mpg) + 
  geom_point(aes(x = displ, y = hwy)) + 
  facet_wrap(~ class) + my_theme
```

&lt;img src="06_slides_files/figure-html/unnamed-chunk-30-1.png" width="400" style="display: block; margin: auto;" /&gt;

]

---
## facets

.tiny[


```r
ggplot(mpg) + 
  geom_point(aes(x = displ, y = hwy)) + 
  facet_grid(year ~ drv) + my_theme
```

&lt;img src="06_slides_files/figure-html/unnamed-chunk-31-1.png" width="400" style="display: block; margin: auto;" /&gt;

]


---

### plot composition

.tiny[


```r
msleep &lt;- na.omit(msleep)
p1 &lt;- ggplot(msleep) + 
  geom_boxplot(aes(x = sleep_total, y = vore, fill = vore)) +
  my_theme
p1 
```

&lt;img src="06_slides_files/figure-html/unnamed-chunk-32-1.png" width="400" style="display: block; margin: auto;" /&gt;
]

---
## plot composition

.tiny[


```r
p2 &lt;- ggplot(msleep) + 
  geom_bar(aes(y = vore, fill = vore)) +
  my_theme
p2
```

&lt;img src="06_slides_files/figure-html/unnamed-chunk-33-1.png" width="400" style="display: block; margin: auto;" /&gt;
]

---
## plot composition

.tiny[


```r
p3 &lt;- ggplot(msleep) + 
  geom_point(aes(x = bodywt, y = sleep_total, colour = vore)) + 
  scale_x_log10() +
  my_theme
p3
```

&lt;img src="06_slides_files/figure-html/unnamed-chunk-34-1.png" width="400" style="display: block; margin: auto;" /&gt;

]

---
## patchwork



```r
library(patchwork)
p1 + p2 + p3
```

&lt;img src="06_slides_files/figure-html/unnamed-chunk-35-1.png" width="400" style="display: block; margin: auto;" /&gt;

---
## patchwork

.tiny[

```r
(p1 | p2) / p3 + plot_layout(guides = 'collect') + 
  plot_annotation(title = 'Mammalian sleep patterns',
                  tag_levels = 'A')
```

&lt;img src="06_slides_files/figure-html/unnamed-chunk-36-1.png" width="400" style="display: block; margin: auto;" /&gt;
]


---
## density ridges

.tiny[


```r
library(ggridges)
df_diamonds &lt;- diamonds[1:100, c("color", "depth")]
ggplot(df_diamonds, aes(x = depth, y = color)) +
  geom_density_ridges() + my_theme
```

&lt;img src="06_slides_files/figure-html/unnamed-chunk-37-1.png" width="400" style="display: block; margin: auto;" /&gt;

]


---
## other resources


.pull-left[
- color palletes:
  - [Color Brewer](http://colorbrewer2.org/)
  - [Adobe Color ](https://color.adobe.com/create)
  - [COLOR lovers](https://www.colourlovers.com/)
  
- icons for graphics:
  - [The noun project](https://thenounproject.com/)
  - [iconmonst](https://iconmonstr.com/)
]


.pull-right[
- R packges:
  - `wesanderson`
  - `RColorBrewer`
  - `swatches`
  - `colourlovers`
]


---
class: inverse, middle, center

# aRt

---

&lt;img src="figs/dani.png" width="1000" style="display: block; margin: auto;" /&gt;

useR!2021


---

&lt;img src="figs/antonio_sanchez.png" width="1000" style="display: block; margin: auto;" /&gt;

useR!2021

---
## todo


&lt;svg viewBox="0 0 640 512" style="height:1em;fill:currentColor;position:relative;display:inline-block;top:.1em;" xmlns="http://www.w3.org/2000/svg"&gt;  &lt;path d="M255.03 261.65c6.25 6.25 16.38 6.25 22.63 0l11.31-11.31c6.25-6.25 6.25-16.38 0-22.63L253.25 192l35.71-35.72c6.25-6.25 6.25-16.38 0-22.63l-11.31-11.31c-6.25-6.25-16.38-6.25-22.63 0l-58.34 58.34c-6.25 6.25-6.25 16.38 0 22.63l58.35 58.34zm96.01-11.3l11.31 11.31c6.25 6.25 16.38 6.25 22.63 0l58.34-58.34c6.25-6.25 6.25-16.38 0-22.63l-58.34-58.34c-6.25-6.25-16.38-6.25-22.63 0l-11.31 11.31c-6.25 6.25-6.25 16.38 0 22.63L386.75 192l-35.71 35.72c-6.25 6.25-6.25 16.38 0 22.63zM624 416H381.54c-.74 19.81-14.71 32-32.74 32H288c-18.69 0-33.02-17.47-32.77-32H16c-8.8 0-16 7.2-16 16v16c0 35.2 28.8 64 64 64h512c35.2 0 64-28.8 64-64v-16c0-8.8-7.2-16-16-16zM576 48c0-26.4-21.6-48-48-48H112C85.6 0 64 21.6 64 48v336h512V48zm-64 272H128V64h384v256z"&gt;&lt;/path&gt;&lt;/svg&gt;

- create and run `Rmd` file `graphics.Rmd` 


- `git add`, `commit`, and `push` of the day


---
class: center, middle

# ¡Thanks!

&lt;center&gt;
&lt;svg viewBox="0 0 512 512" style="position:relative;display:inline-block;top:.1em;fill:#A70000;height:1em;" xmlns="http://www.w3.org/2000/svg"&gt;  &lt;path d="M476 3.2L12.5 270.6c-18.1 10.4-15.8 35.6 2.2 43.2L121 358.4l287.3-253.2c5.5-4.9 13.3 2.6 8.6 8.3L176 407v80.5c0 23.6 28.5 32.9 42.5 15.8L282 426l124.6 52.2c14.2 6 30.4-2.9 33-18.2l72-432C515 7.8 493.3-6.8 476 3.2z"&gt;&lt;/path&gt;&lt;/svg&gt; [saramortara@gmail.com](mailto:saramortara@gmail.com) | [andreasancheztapia@gmail.com](mailto:andreasancheztapia@gmail.com) 


&lt;svg viewBox="0 0 512 512" style="position:relative;display:inline-block;top:.1em;fill:#A70000;height:1em;" xmlns="http://www.w3.org/2000/svg"&gt;  &lt;path d="M459.37 151.716c.325 4.548.325 9.097.325 13.645 0 138.72-105.583 298.558-298.558 298.558-59.452 0-114.68-17.219-161.137-47.106 8.447.974 16.568 1.299 25.34 1.299 49.055 0 94.213-16.568 130.274-44.832-46.132-.975-84.792-31.188-98.112-72.772 6.498.974 12.995 1.624 19.818 1.624 9.421 0 18.843-1.3 27.614-3.573-48.081-9.747-84.143-51.98-84.143-102.985v-1.299c13.969 7.797 30.214 12.67 47.431 13.319-28.264-18.843-46.781-51.005-46.781-87.391 0-19.492 5.197-37.36 14.294-52.954 51.655 63.675 129.3 105.258 216.365 109.807-1.624-7.797-2.599-15.918-2.599-24.04 0-57.828 46.782-104.934 104.934-104.934 30.213 0 57.502 12.67 76.67 33.137 23.715-4.548 46.456-13.32 66.599-25.34-7.798 24.366-24.366 44.833-46.132 57.827 21.117-2.273 41.584-8.122 60.426-16.243-14.292 20.791-32.161 39.308-52.628 54.253z"&gt;&lt;/path&gt;&lt;/svg&gt; [@MortaraSara](https://twitter.com/MortaraSara) | [@SanchezTapiaA](https://twitter.com/SanchezTapiaA) 

&lt;svg viewBox="0 0 496 512" style="position:relative;display:inline-block;top:.1em;fill:#A70000;height:1em;" xmlns="http://www.w3.org/2000/svg"&gt;  &lt;path d="M165.9 397.4c0 2-2.3 3.6-5.2 3.6-3.3.3-5.6-1.3-5.6-3.6 0-2 2.3-3.6 5.2-3.6 3-.3 5.6 1.3 5.6 3.6zm-31.1-4.5c-.7 2 1.3 4.3 4.3 4.9 2.6 1 5.6 0 6.2-2s-1.3-4.3-4.3-5.2c-2.6-.7-5.5.3-6.2 2.3zm44.2-1.7c-2.9.7-4.9 2.6-4.6 4.9.3 2 2.9 3.3 5.9 2.6 2.9-.7 4.9-2.6 4.6-4.6-.3-1.9-3-3.2-5.9-2.9zM244.8 8C106.1 8 0 113.3 0 252c0 110.9 69.8 205.8 169.5 239.2 12.8 2.3 17.3-5.6 17.3-12.1 0-6.2-.3-40.4-.3-61.4 0 0-70 15-84.7-29.8 0 0-11.4-29.1-27.8-36.6 0 0-22.9-15.7 1.6-15.4 0 0 24.9 2 38.6 25.8 21.9 38.6 58.6 27.5 72.9 20.9 2.3-16 8.8-27.1 16-33.7-55.9-6.2-112.3-14.3-112.3-110.5 0-27.5 7.6-41.3 23.6-58.9-2.6-6.5-11.1-33.3 2.6-67.9 20.9-6.5 69 27 69 27 20-5.6 41.5-8.5 62.8-8.5s42.8 2.9 62.8 8.5c0 0 48.1-33.6 69-27 13.7 34.7 5.2 61.4 2.6 67.9 16 17.7 25.8 31.5 25.8 58.9 0 96.5-58.9 104.2-114.8 110.5 9.2 7.9 17 22.9 17 46.4 0 33.7-.3 75.4-.3 83.6 0 6.5 4.6 14.4 17.3 12.1C428.2 457.8 496 362.9 496 252 496 113.3 383.5 8 244.8 8zM97.2 352.9c-1.3 1-1 3.3.7 5.2 1.6 1.6 3.9 2.3 5.2 1 1.3-1 1-3.3-.7-5.2-1.6-1.6-3.9-2.3-5.2-1zm-10.8-8.1c-.7 1.3.3 2.9 2.3 3.9 1.6 1 3.6.7 4.3-.7.7-1.3-.3-2.9-2.3-3.9-2-.6-3.6-.3-4.3.7zm32.4 35.6c-1.6 1.3-1 4.3 1.3 6.2 2.3 2.3 5.2 2.6 6.5 1 1.3-1.3.7-4.3-1.3-6.2-2.2-2.3-5.2-2.6-6.5-1zm-11.4-14.7c-1.6 1-1.6 3.6 0 5.9 1.6 2.3 4.3 3.3 5.6 2.3 1.6-1.3 1.6-3.9 0-6.2-1.4-2.3-4-3.3-5.6-2z"&gt;&lt;/path&gt;&lt;/svg&gt;&lt;svg viewBox="0 0 512 512" style="position:relative;display:inline-block;top:.1em;fill:#A70000;height:1em;" xmlns="http://www.w3.org/2000/svg"&gt;  &lt;path d="M105.2 24.9c-3.1-8.9-15.7-8.9-18.9 0L29.8 199.7h132c-.1 0-56.6-174.8-56.6-174.8zM.9 287.7c-2.6 8 .3 16.9 7.1 22l247.9 184-226.2-294zm160.8-88l94.3 294 94.3-294zm349.4 88l-28.8-88-226.3 294 247.9-184c6.9-5.1 9.7-14 7.2-22zM425.7 24.9c-3.1-8.9-15.7-8.9-18.9 0l-56.6 174.8h132z"&gt;&lt;/path&gt;&lt;/svg&gt;  [saramortara](http://github.com/saramortara) |  [andreasancheztapia](http://github.com/andreasancheztapia)
    </textarea>
<style data-target="print-only">@media screen {.remark-slide-container{display:block;}.remark-slide-scaler{box-shadow:none;}}</style>
<script src="https://remarkjs.com/downloads/remark-latest.min.js"></script>
<script>var slideshow = remark.create({
"highlightStyle": "github",
"highlightLines": true,
"countIncrementalSlides": false,
"ratio": "16:9"
});
if (window.HTMLWidgets) slideshow.on('afterShowSlide', function (slide) {
  window.dispatchEvent(new Event('resize'));
});
(function(d) {
  var s = d.createElement("style"), r = d.querySelector(".remark-slide-scaler");
  if (!r) return;
  s.type = "text/css"; s.innerHTML = "@page {size: " + r.style.width + " " + r.style.height +"; }";
  d.head.appendChild(s);
})(document);

(function(d) {
  var el = d.getElementsByClassName("remark-slides-area");
  if (!el) return;
  var slide, slides = slideshow.getSlides(), els = el[0].children;
  for (var i = 1; i < slides.length; i++) {
    slide = slides[i];
    if (slide.properties.continued === "true" || slide.properties.count === "false") {
      els[i - 1].className += ' has-continuation';
    }
  }
  var s = d.createElement("style");
  s.type = "text/css"; s.innerHTML = "@media print { .has-continuation { display: none; } }";
  d.head.appendChild(s);
})(document);
// delete the temporary CSS (for displaying all slides initially) when the user
// starts to view slides
(function() {
  var deleted = false;
  slideshow.on('beforeShowSlide', function(slide) {
    if (deleted) return;
    var sheets = document.styleSheets, node;
    for (var i = 0; i < sheets.length; i++) {
      node = sheets[i].ownerNode;
      if (node.dataset["target"] !== "print-only") continue;
      node.parentNode.removeChild(node);
    }
    deleted = true;
  });
})();
// add `data-at-shortcutkeys` attribute to <body> to resolve conflicts with JAWS
// screen reader (see PR #262)
(function(d) {
  let res = {};
  d.querySelectorAll('.remark-help-content table tr').forEach(tr => {
    const t = tr.querySelector('td:nth-child(2)').innerText;
    tr.querySelectorAll('td:first-child .key').forEach(key => {
      const k = key.innerText;
      if (/^[a-z]$/.test(k)) res[k] = t;  // must be a single letter (key)
    });
  });
  d.body.setAttribute('data-at-shortcutkeys', JSON.stringify(res));
})(document);
(function() {
  "use strict"
  // Replace <script> tags in slides area to make them executable
  var scripts = document.querySelectorAll(
    '.remark-slides-area .remark-slide-container script'
  );
  if (!scripts.length) return;
  for (var i = 0; i < scripts.length; i++) {
    var s = document.createElement('script');
    var code = document.createTextNode(scripts[i].textContent);
    s.appendChild(code);
    var scriptAttrs = scripts[i].attributes;
    for (var j = 0; j < scriptAttrs.length; j++) {
      s.setAttribute(scriptAttrs[j].name, scriptAttrs[j].value);
    }
    scripts[i].parentElement.replaceChild(s, scripts[i]);
  }
})();
(function() {
  var links = document.getElementsByTagName('a');
  for (var i = 0; i < links.length; i++) {
    if (/^(https?:)?\/\//.test(links[i].getAttribute('href'))) {
      links[i].target = '_blank';
    }
  }
})();
// adds .remark-code-has-line-highlighted class to <pre> parent elements
// of code chunks containing highlighted lines with class .remark-code-line-highlighted
(function(d) {
  const hlines = d.querySelectorAll('.remark-code-line-highlighted');
  const preParents = [];
  const findPreParent = function(line, p = 0) {
    if (p > 1) return null; // traverse up no further than grandparent
    const el = line.parentElement;
    return el.tagName === "PRE" ? el : findPreParent(el, ++p);
  };

  for (let line of hlines) {
    let pre = findPreParent(line);
    if (pre && !preParents.includes(pre)) preParents.push(pre);
  }
  preParents.forEach(p => p.classList.add("remark-code-has-line-highlighted"));
})(document);</script>

<script>
slideshow._releaseMath = function(el) {
  var i, text, code, codes = el.getElementsByTagName('code');
  for (i = 0; i < codes.length;) {
    code = codes[i];
    if (code.parentNode.tagName !== 'PRE' && code.childElementCount === 0) {
      text = code.textContent;
      if (/^\\\((.|\s)+\\\)$/.test(text) || /^\\\[(.|\s)+\\\]$/.test(text) ||
          /^\$\$(.|\s)+\$\$$/.test(text) ||
          /^\\begin\{([^}]+)\}(.|\s)+\\end\{[^}]+\}$/.test(text)) {
        code.outerHTML = code.innerHTML;  // remove <code></code>
        continue;
      }
    }
    i++;
  }
};
slideshow._releaseMath(document);
</script>
<!-- dynamically load mathjax for compatibility with self-contained -->
<script>
(function () {
  var script = document.createElement('script');
  script.type = 'text/javascript';
  script.src  = 'https://mathjax.rstudio.com/latest/MathJax.js?config=TeX-MML-AM_CHTML';
  if (location.protocol !== 'file:' && /^https?:/.test(script.src))
    script.src  = script.src.replace(/^https?:/, '');
  document.getElementsByTagName('head')[0].appendChild(script);
})();
</script>
  </body>
</html>
