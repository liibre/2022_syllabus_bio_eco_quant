---
title: "Exploratory data analysis"
author: "Sara Mortara & Andrea Sánchez-Tapia"
institute: "re.green | ¡liibre!"
date: "2022-07-13"
output:
  xaringan::moon_reader:
    lib_dir: libs
    css: xaringan-themer.css
    nature:
      highlightStyle: github
      highlightLines: true
      countIncrementalSlides: false
      ratio: '16:9'
self-contained: true
---

```{css css-config, echo = FALSE}
.tiny .remark-code { /*Change made here*/
  font-size: 50% !important;
}
```

```{r setup, include=FALSE}
options(htmltools.dir.version = FALSE)
```

```{r xaringanthemer, include = FALSE}
library(xaringanthemer)
xaringanthemer::style_mono_accent(
  base_color = "#A70000",
  #  primary_color = "#A70000",
  #secondary_color = "#ff0000",
  white_color = "white",
  colors = c(
    red = "#A70000",
    purple = "#88398a",
    orange = "#ff8811",
    green = "#136f63",
    blue = "#4B4FFF",
    white = "#FFFFFF",
    black = "#181818"
  ),
  header_font_google = google_font("Roboto Condensed"),
  text_font_google   = google_font("Roboto Condensed", "300", "300i"),
  code_font_google   = google_font("Ubuntu Mono"),
  text_font_size = "30px",
  code_font_size = "30px",
  )
xaringanExtra::use_share_again()

# clipboard
htmltools::tagList(
  xaringanExtra::use_clipboard(
    button_text = "Copy code <i class=\"fa fa-clipboard\"></i>",
    success_text = "Copied! <i class=\"fa fa-check\" style=\"color: #90BE6D\"></i>",
    error_text = "Not copied 😕 <i class=\"fa fa-times-circle\" style=\"color: #F94144\"></i>"
  ),
  rmarkdown::html_dependency_font_awesome()
)

```


## today

- exploratory data analysis

- descriptive statistics

- exploratory graphics

- variable relationships

---

## Explanatory Data Analysis - John Tukey

.pull-left[
```{r, echo=FALSE, fig.align='center', out.width=300}
knitr::include_graphics("figs/tukey.jpg")
```
]

.pull-right[
```{r, echo=FALSE, fig.align='center'}
knitr::include_graphics("figs/John_Tukey.jpg")
```
]

---
## get to know your data!

```{r, echo=FALSE, fig.align='center'}
knitr::include_graphics("figs/pee.jpg")
```

---
## goals of EDA

1. control data quality
--


2. suggest hypotheses for observed patterns
--


3. support the choice of statistical procedures for hypothesis testing
--


4. assess whether the data meet the assumptions of the chosen statistical procedures
--


5. indicate new studies and hypotheses

---

## alert!


EDA does not mean 

```{r, fig.align='center', echo=FALSE, out.width=500}
knitr::include_graphics("figs/tortura.jpg")
```

it is assumed that the researcher has formulated *a priori*  __hypotheses__  supported by __theory__



---
## tips


- there is no recipe!
--


- can take between 20-50% of analysis time
--


- can be started during data collection
--


- visual techniques are widely used
--

---

## the importance of graphics and the Anscombe quartet


- created by mathematician Francis Ascombe
--


- 4 datasets with the same descriptive statistics but very different graphically

```{r, echo=FALSE, fig.align='center'}
knitr::include_graphics("figs/Francis_Anscombe.jpeg")
```



---

## Anscombe data

```{r, echo=FALSE}
library(wesanderson)
cor <- wes_palette("Rushmore1") #("FantasticFox1")
```


.tiny[
```{r, echo=TRUE}
# the dataset already exists inside R
data("anscombe")
```

```{r, echo=TRUE}
# mean
apply(anscombe, 2, var)

# variance
apply(anscombe, 2, var)
```
]


---
## let's take a look into the data


```{r,echo=FALSE}
anscombe
```


---
## correltion between x and y 

.tiny[

```{r, echo=TRUE}
# correlation
cor(anscombe$x1, anscombe$y1)
cor(anscombe$x2, anscombe$y2)
cor(anscombe$x3, anscombe$y3)
cor(anscombe$x4, anscombe$y4)
```

]

---
## coefficients of the linear model


.tiny[

```{r, echo=TRUE}
# correlation
coef(lm(anscombe$x1 ~ anscombe$y1))
coef(lm(anscombe$x2 ~ anscombe$y2))
coef(lm(anscombe$x3 ~ anscombe$y3))
coef(lm(anscombe$x4 ~ anscombe$y4))
```

]


---
## now let's actually look into the Anscombe data



```{r, echo=FALSE, fig.align='center'}
cor.c <- "grey40"
par(mfrow = c(2, 2), bty = 'l', las = 1, mar = c(3.8, 3.8, 2, 2))
plot(y1 ~ x1, data = anscombe, pch = 19, col = cor[2], cex = 1.8)
abline(lm(y1 ~ x1, data = anscombe), lwd = 2, col = cor.c)
plot(y2 ~ x2, data = anscombe, pch = 19, col = cor[3], cex = 1.8)
abline(lm(y2 ~ x2, data = anscombe), lwd = 2, col = cor.c)
plot(y3 ~ x3, data = anscombe, pch = 19, col = cor[4], cex = 1.8)
abline(lm(y3 ~ x3, data = anscombe), lwd = 2, col = cor.c)
plot(y4 ~ x4, data = anscombe, pch = 19, col = cor[5], cex = 1.8)
abline(lm(y4 ~ x4, data = anscombe), lwd = 2, col = cor.c)
par(mfrow = c(1, 1))
```


---
## guiding questions

1. Where is the data centered? How is the data distributed? Are the data symmetrical, asymmetrical, bimodal?
--


2. Are there outliers?
--


3. Do the variables follow a normal distribution?
--


4. Are there relationships between the variables? Are the relationships between variables linear?
--


5. Do variables need to be transformed?
--


6. Was the sampling effort the same for each observation or variable?


---
class: inverse,  middle, center

# descriptive statistics



---
## questions to ask the data 

1. are there are missing values i.e. (__NA__s)? Are they really missing?
 
2. area there many __zeroes__?

3. where is the data centered? how are they spread? are they symmetrical? skewed, bimodal?

4. are there extreme values (outliers)?

5. what is the distribution of the variable?


---
## descriptive statistics 


| Parameter | Description | R function |
|------|-------------|--------|
| average | arithmetic mean | mean() |
| median | core value | median() |
| mode | most frequent value | sort(table(), decreasing = TRUE)[1] |
| standard deviation | variation around the mean | sd() |
| quantiles | cut points dividing a probability distribution | quantile() |




---
class: inverse,  middle, center

# exploratory graphics



---

## reading data in R


```{r, echo=TRUE}
# reading data generated in the last class
all_data <- read.csv("data/processed/03_Pavoine_full_table.csv")
# reading environmental data
envir <- read.csv("data/raw/cestes/envir.csv")
# environmental data without site
envir.vars <- envir[, -1]
```


---

## visualizing data in a boxplot



```{r, fig.align='center', echo=TRUE, fig.width=6, fig.height=6}

boxplot(all_data$Abundance)
```


---

## going back to the data

```{r}
summary(all_data$Abundance)

# how many zeroes
sum(all_data$Abundance == 0)

# what proportion?
sum(all_data$Abundance == 0)/nrow(all_data)

```



---

## understanding the boxplot


```{r, echo=FALSE, fig.show='asis', fig.align='center', fig.width=10, fig.height=8}

set.seed(2)
par(bty = "n")
bp <- rnorm(1000, 0, 0.1)
boxplot(bp, yaxt = "n", xlim = c(0, 3), ylim = c(-0.3, 0.3))
text(x = 1.82, y = min(bp), "last point (-1,5 x IIQ)", cex = .9)
text(x = 1.72, y = quantile(bp)[2], "first quartile", cex = .9)
text(x = 1.72, y = median(bp), "median", cex = .9)
text(x = 1.72, y = quantile(bp)[4], "third quartile", cex = .9)
text(x = 1.82, y = bp[203], "first point (+1,5 x IIQ)", cex = .9)
arrows(x0 = 0.68, x1 = 0.68, y0 = quantile(bp)[2], y1 = quantile(bp)[4], code = 3, length = 0.05)
text(x = 0.54, y = median(bp), "IQR", cex = .9)
```

---

## visualizing data in a histogram



```{r, fig.align='center', echo=TRUE, fig.width=6, fig.height=6}
hist(all_data$Abundance)
```



---

## types of histogram


.tiny[

```{r, echo=TRUE, fig.width=11, fig.height=5.5, fig.align='center'}
par(mfrow = c(1,2))
hist(all_data$Abundance)
hist(all_data$Abundance, probability = TRUE)
par(mfrow = c(1,1))
```

]



---
## classes of histogram


.tiny[


```{r, echo=TRUE, fig.width=12, fig.height=4, fig.align='center'}
par(mfrow = c(1,3))
hist(all_data$Abundance, 
     breaks = seq(0, max(all_data$Abundance), length = 3))
hist(all_data$Abundance,  
     breaks = seq(0, max(all_data$Abundance), length = 5))
hist(all_data$Abundance)
par(mfrow = c(1,1))
```


]


---

## empirical probability density curves

represents the function that describes the probability of finding a certain value


```{r, echo=TRUE, fig.width=5, fig.height=5, fig.align='center'}
hist(all_data$Abundance, probability = TRUE )
```


---

## empirical probability density curves


```{r dens, fig.width=6, fig.height=6, fig.align='center'}
plot(density(all_data$Abundance))
```


---

## does the distribution fit the data?


discrete and asymmetric distribution --> Poisson?


```{r, echo=TRUE}
# maximum of abundance
ab.max <- max(all_data$Abundance)
# lambda
ab.med <- mean(all_data$Abundance)
```

---
## does the  __Poisson__ distribution fit the data?


```{r, echo=TRUE, fig.width=5, fig.height=5, fig.align='center'}

hist(all_data$Abundance, probability = TRUE)
points(dpois(0:ab.max, ab.med), col = cor[5])
lines(dpois(0:ab.max, ab.med), col = cor[5])

```


---
## statistical distributions: Gaussian or normal



```{r norm, echo=FALSE, fig.align='center'}

set.seed(42)
a <- rnorm(200, 8, 1)
b <- rnorm(200, 8, 1.5)
c <- rnorm(200, 8, 2.5)
curve(dnorm(x, mean(a), sd(a)), col = "darkblue", lwd = 2,
      ylim = c(0, 0.5), xlim = c(4, 12),
      xaxt = "n", yaxt = 'n', xlab = '', ylab = '')
curve(dnorm(x, mean(b), sd(b)), col = "darkred", lwd = 2,
      ylim = c(0,0.5), xlim = c(4, 12), add = TRUE)
curve(dnorm(x, mean(c), sd(c)), col = "darkgreen", lwd = 2,
      ylim = c(0,0.5), xlim = c(4, 12), add = TRUE)
legend('topright', lty = 1, lwd = 2, 
       legend = paste0("sd=", c(1, 1.5, 2.5)),
       col = c("darkblue", "darkred", "darkgreen"), bty = 'n')


```



---
## why is sampling important?


```{r norm-sampling, echo = FALSE, fig.width=12, fig.height=4, fig.align='center'}
par(mfrow = c(1, 3))
a <- rnorm(200, 8, 1)
hist(a, prob = TRUE, main = "N = 200")
curve(dnorm(x, mean(a), sd(a)),
      col = "darkred", lwd = 2, add = TRUE)

a <- rnorm(100, 8, 1)
hist(a, prob = TRUE, main = "N = 200")
curve(dnorm(x, mean(a), sd(a)), 
      col = "darkred", lwd = 2, add = TRUE)

a <- rnorm(10, 8, 1)
hist(a, prob = TRUE, main = "N = 200")
curve(dnorm(x, mean(a), sd(a)),
      col = "darkred", lwd = 2, add = TRUE)
```


---
class: inverse, middle, center

# relationships between variables


---

## scatter plot


```{r dispersao, fig.align='center', fig.width=6, fig.height=6}
plot(Clay ~ Silt, data = envir.vars, pch = 19)
```


---

## correlation between variables


.tiny[


```{r correlacao}
cor(envir.vars)
```

]


---
## correlation between variables

```{r, echo=FALSE, out.width=800, fig.align='center'}
knitr::include_graphics("figs/Correlation_examples2.svg")
```


[By DenisBoigelot](https://commons.wikimedia.org/w/index.php?curid=15165296)


---

## correlation between variables


.tiny[


```{r pairs, fig.align='center'}
pairs(envir.vars)
```

]


---
## even better visualization



```{r cor, echo=FALSE, message=FALSE, fig.align='center'}
library(GGally)
ggpairs(envir.vars)
```


---
class: center, middle

# and what are the paths for the data analysis?

your __[ H Y P O T H E S I S ]__



---

## after the __[ H Y P O T H E S I S ]__, what are the paths?

1. understand the data well
--


2. variable response is normal? --> __lm__ and other parametric analysis
--


3. variable response has another distribution -->  non-parametric analysis, __glm__
--


4. hierarchical predictor variables? --> __(g)lmm__
--


5. pseudo-replication in space or time -->  __(g)lmm__
--


---
## todo


`r icons::fontawesome("laptop-code")`

- create and run script `04_eda.R` 

- `git add`, `commit`, and `push` of the day


---
class: center, middle

# ¡Thanks!

<center>
`r icons::icon_style(icons::fontawesome("paper-plane", style = "solid"), scale = 1, fill = "#A70000")` [saramortara@gmail.com](mailto:saramortara@gmail.com) | [andreasancheztapia@gmail.com](mailto:andreasancheztapia@gmail.com) 


`r icons::icon_style(icons::fontawesome("twitter", style = "brands"), scale = 1, fill = "#A70000")` [@MortaraSara](https://twitter.com/MortaraSara) | [@SanchezTapiaA](https://twitter.com/SanchezTapiaA) 

`r icons::icon_style(icons::fontawesome("github", style = "brands"), scale = 1, fill = "#A70000")``r icons::icon_style(icons::fontawesome("gitlab", style = "brands"), scale = 1, fill = "#A70000")`  [saramortara](http://github.com/saramortara) |  [andreasancheztapia](http://github.com/andreasancheztapia) 

