---
title: "About"
description: |
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = FALSE)
```

__Sara Mortara PhD__  `r icons::fontawesome("github")` [saramortara](http://github.com/saramortara) — `r icons::fontawesome("twitter")`  [\@MortaraSara](https://twitter.com/MortaraSara) - I am a Brazilian biologist (she/her) with a doctorate degree in ecology, working with ecological modeling, biodiversity informatics, and reproducibility in ecology. Currently, I work in the private sector as a researcher building R packages and workflows to access restoration and conservation strategies in the Tropics. I have been using R since 2009.

__Andrea Sánchez-Tapia PhD__ `r icons::fontawesome("github")` [andreasancheztapia](http://github.com/andreasancheztapia) — `r icons::fontawesome("twitter")`  [\@SanchezTapiaA](https://twitter.com/SanchezTapiaA) - I am a Colombian biologist (she/her) with a background in Biodiversity Informatics, Ecological Niche Modeling, and Plant Ecology.
I hold a BSc in Biology (UNAL, Colombia), a MSc in Ecology (UFRJ, Brazil), and a PhD in Botany (ENBT-JBRJ, Brazil).
I have a deep interest in Open and Responsible Science. I have been using R since 2009.
