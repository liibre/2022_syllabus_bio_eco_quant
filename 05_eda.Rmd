---
title: "Exploratory data analysis in R"
description: |
author:
  - name: Sara Mortara
    affiliation: re.green | ¡liibre!
    url: {}
    orcid_id: 0000-0001-6221-7537
  - name: Andrea Sánchez-Tapia
    orcid_id: 0000-0002-3521-4338
    affiliation: ¡liibre!
    affiliation_url: {}
    url: https://andreasancheztapia.netlify.app
citation_url: https://scientific-computing.netlify.app/05_eda.html    
date: 2022-07-13
---


```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = FALSE)
```


# Slides

```{r slides}
xaringanExtra::embed_xaringan("05_eda/05_slides.html")
```

# Tutorial

- Find [here](05_eda/05_tutorial.html) the complete tutorial

- Download [here](https://gitlab.com/liibre/2022_scientific_computing_intro/-/raw/main/R/04_eda.R) the R script to add to your project

