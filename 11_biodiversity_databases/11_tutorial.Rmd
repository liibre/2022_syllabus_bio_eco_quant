---
title: "Basic workflow for biodiversity data download and cleaning using R"
author:
  - "Sara Mortara"
  - "Andrea Sánchez-Tapia"
date: "7/27/2022"
output:
  distill::distill_article:
    toc: true
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE,
                      message = FALSE,
                      warning = FALSE,
                      cache = TRUE)
```

```{r xaringanExtra-clipboard, echo=FALSE}
htmltools::tagList(
  xaringanExtra::use_clipboard(
    button_text = "Copy code <i class=\"fa fa-clipboard\"></i>",
    success_text = "Copied! <i class=\"fa fa-check\" style=\"color: #90BE6D\"></i>",
    error_text = "Not copied :/ <i class=\"fa fa-times-circle\" style=\"color: #F94144\"></i>"
  ),
  rmarkdown::html_dependency_font_awesome()
)
```

## Ocurrence downloads




# Loading packages


For this tutorial, you will need to install the R packages: `rgbif`, `Taxonstand` `CoordinateCleaner` and `maps`. If you don't have them installed use the following commands:

```{r, eval=FALSE}
install.packages("rgbif")
install.packages("Taxonstand")
install.packages("CoordinateCleaner")
install.packages("maps")
```

Then, we'll start loading the packages.  

```{r loading pkg}
library(rgbif)
library(Taxonstand)
library(CoordinateCleaner)
library(maps)
```

# Getting the data

First, let's download the data of a Primulaceae species from South America *Myrsine coriacea* (Sw.) R.Br.



```{r occs}
library(rgbif)
library(dplyr)
species <- "Myrsine coriacea"
occs <- occ_search(scientificName = species,
                   limit = 100000, 
                   basisOfRecord = "PRESERVED_SPECIMEN")
names(occs)
```

The occurrences are saved in `occs$data`. Let's create a new object from this table 

```{r}
myrsine.data <- occs$data
```


In the raw data, we have `r nrow(myrsine.data)` records. 

Column names returned from gbif follow the DarwinCore standard (https://dwc.tdwg.org). 

```{r col-names}
#| eval: false
colnames(myrsine.data)
```

## Exporting raw data

In order to guarantee the documentation of all steps, saving the raw data is essential. We will create a directory to save data and then export the data as csv (text file separated by comma).  

```{r save-raw}
dir.create("data/raw/", recursive = TRUE)
write.csv(myrsine.data, 
          "data/raw/myrsine_data.csv", 
          row.names = FALSE)
```

# Checking species taxonomy

Let's check the unique entries for the species name we just searched.

```{r sp-name}
sort(unique(myrsine.data$scientificName))
```

In this particular case, we have a species with a long history of synonyms. In the gbif data, there is already a column showing the currently accepted taxonomy:

```{r sp-accepted}
table(myrsine.data$taxonomicStatus)
```

We can also check which of the names are accepted or not: 

```{r names-accepted}
table(myrsine.data$scientificName, myrsine.data$taxonomicStatus)
```

Let's use the function `TPL()` from package `taxonstand` to check if the taxonomic updates in the gbif data are correct. This function receives a vector containing a list of species and performs both orthographical and nomenclature checking. Nomenclature checking follows [The Plant List](http://www.theplantlist.org/). 

We will first generate a list with unique species names and combine it to the data. This is preferable because we do not need to check more than once the same name and, in the case of working with several species, it will make the workflow faster. 

```{r taxonstand}
species.names <- unique(myrsine.data$scientificName) 
dim(species.names)
tax.check <- TPL(species.names)
```

Let's check the output:

```{r tax-out}
tax.check
```

Note that the function adds several new variables to the input data and creates columns such as `New.Genus` and `New.Species` with the accepted name. We should adopt these names if the column `New.Taxonomic.status` is filled with "Accepted"

We will merge the new genus and species and then add them to the original data. 

```{r merge}
# creating new object w/ original and new names after TPL
new.tax <- data.frame(scientificName = species.names, 
                      genus.new.TPL = tax.check$New.Genus, 
                      species.new.TPL = tax.check$New.Species,
                      status.TPL = tax.check$Taxonomic.status,
                      scientificName.new.TPL = paste(tax.check$New.Genus,
                                                     tax.check$New.Species)) 
# now we are merging raw data and checked data
myrsine.new.tax <- merge(myrsine.data, new.tax, by = "scientificName")
```

## Exporting data after taxonomy check

To guarantee the documentation of all steps, we will export the data after the taxonomy check. 

```{r}
dir.create("data/processed/", recursive = TRUE)
write.csv(myrsine.new.tax, 
          "data/processed/data_taxonomy_check.csv", 
          row.names = FALSE)
```

# Checking species' coordinates

First, let's inspect visually the coordinates in the raw data. 

```{r}
plot(decimalLatitude ~ decimalLongitude, data = myrsine.data, asp = 1)
map(, , , add = TRUE)
```

Now we will use the the function `clean_coordinates()` from the `CoordinateCleaner` package to clean the species records. This function checks for common errors in coordinates such as institutional coordinates, sea coordinates, outliers, zeros, centroids, etc. This function does not accept not available information (here addressed as "NA") so we will first select only data that have a numerical value for both latitude and longitude. 

Note: at this moment having a specific ID code for each observation is essential. The raw data already provides an ID in the column `gbifID`. 

```{r coord-prep}
myrsine.coord <- myrsine.data[!is.na(myrsine.data$decimalLatitude) 
                   & !is.na(myrsine.data$decimalLongitude),]
```


Now that we don't have NA in latitude or longitude, we can perform the coordinate cleaning. This new dataset has `r nrow(myrsine.coord)` occurrences.

```{r coord-clean}
# output w/ only potential correct coordinates
geo.clean <- clean_coordinates(x = myrsine.coord, 
                               lon = "decimalLongitude",
                               lat = "decimalLatitude",
                               species = "species", 
                               value = "clean")
table(myrsine.coord$country)
table(geo.clean$country)
```

Let's plot the output of the clean data. 

```{r map-plot}
par(mfrow = c(1, 2))
plot(decimalLatitude ~ decimalLongitude, data = myrsine.data, asp = 1)
map(, , , add = TRUE)
plot(decimalLatitude ~ decimalLongitude, data = geo.clean, asp = 1)
map(, , , add = TRUE)
par(mfrow = c(1, 1))
```

When setting `value = clean` it returns only the potentially correct coordinates. For checking and reproducibility we want to save all the output with the flags generated by the routine. Let's try a different output. 

```{r coord-clean-2}
myrsine.new.geo <- clean_coordinates(x = myrsine.coord, 
                                  lon = "decimalLongitude",
                                  lat = "decimalLatitude",
                                  species = "species", 
                                  value = "spatialvalid")
table(myrsine.new.geo$.summary)
tail(names(myrsine.new.geo))
```

Then, we merge the raw data with the cleaned data.

```{r}
# merging w/ original data
dim(myrsine.data)
dim(myrsine.new.geo)
myrsine.new.geo2 <- merge(myrsine.data, myrsine.new.geo, 
                       all.x = TRUE) 
dim(myrsine.new.geo2)
full_join(myrsine.data, myrsine.new.geo)
```

```{r geoclean}

plot(decimalLatitude ~ decimalLongitude, data = myrsine.new.geo2, asp = 1, 
     col = if_else(myrsine.new.geo2$.summary, "green", "red"))
map(, , , add = TRUE)

```


## Exporting the data after coordinate check

```{r}

write.csv(myrsine.new.geo2, 
          "data/processed/myrsine_coordinate_check.csv", 
          row.names = FALSE)
```


## Save the dataset as shapefile

We can also save the dataset as a shapefile, we will transform the data frame in a sf object. 


```{r}
library(tmap)
library(sf)
myrsine.final <- left_join(myrsine.coord, myrsine.new.geo2)
nrow(myrsine.final)

myrsine_sf <- st_as_sf(myrsine.final, coords = c("decimalLongitude", "decimalLatitude"))
st_crs(myrsine_sf)
myrsine_sf <- st_set_crs(myrsine_sf, 4326)
st_crs(myrsine_sf)
#dir.create("data/shapefiles", recursive = T)
#st_write(myrsine_sf, dsn = "data/shapefiles/myrsine.shp")
```

## Plot with `tmap`

`tmap` will use the sf object.
We can add the shapefile to the map we created yesterday: 



```{r, eval = T}
data(World)

SAm_map <- World %>% 
  filter(continent %in% c("South America", "North America")) %>% 
  tm_shape() +
  tm_borders()
 

SAm_map +
tm_shape(myrsine_sf) + 
  tm_bubbles(size = 0.2, 
             col = ".summary")
```


## Interactive mode in tmap

The option `tmap_mode("view")` creates interactive maps. For this, we need to transform the data frame into a sf shapefile.  


```{r, eval = FALSE}
tmap_mode("view")
World %>% 
  filter(continent %in% c("South America", "North America")) %>% 
  tm_shape() +
  tm_borders() + 
  tm_shape(myrsine_sf) + 
  tm_bubbles(size = 0.2)
```


