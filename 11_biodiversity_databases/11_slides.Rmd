---
title: "Biodiversity databases"
subtitle: "Serrapilheira/ICTP-SAIFR Training Program in Quantitative Biology and Ecology"
author: 
  - "Andrea Sánchez-Tapia & Sara Mortara"
date: "27 July 2022"
output:
  xaringan::moon_reader:
    lib_dir: libs
    css: xaringan-themer.css
    nature:
      highlightStyle: github
      highlightLines: true
      countIncrementalSlides: false
      ratio: '16:9'
autocontained: true
---


```{css css-config, echo = FALSE}
.tiny .remark-code { /*Change made here*/
  font-size: 50% !important;
}
```

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = FALSE)
options(htmltools.dir.version = FALSE)
library(xaringanthemer)
library(knitr)
library(ggplot2)
co <- "#A70000"
xaringanthemer::style_mono_accent(
  base_color = co,
  #  primary_color = "#A70000",
  #secondary_color = "#ff0000",
  white_color = "white",
  colors = c(
    red = "#A70000",
    purple = "#88398a",
    orange = "#ff8811",
    green = "#136f63",
    blue = "#4B4FFF",
    white = "#FFFFFF",
    black = "#181818"
  ),
  header_font_google = google_font("Roboto Condensed"),
  text_font_google   = google_font("Roboto Condensed", "300", "300i"),
  code_font_google   = google_font("Ubuntu Mono"),
  text_font_size = "30px",
  code_font_size = "30px",
)
xaringanExtra::use_share_again()
```

```{r xaringanExtra-clipboard, echo=FALSE}
htmltools::tagList(
  xaringanExtra::use_clipboard(
    button_text = "Copy code <i class=\"fa fa-clipboard\"></i>",
    success_text = "Copied! <i class=\"fa fa-check\" style=\"color: #90BE6D\"></i>",
    error_text = "Not copied :/ <i class=\"fa fa-times-circle\" style=\"color: #F94144\"></i>"
  ),
  rmarkdown::html_dependency_font_awesome()
)
```

## today

1. Biodiversity databases: basic concepts

--

2. DarwinCore standard

--

3. Biodiversity data cleaning

--

4. A workflow in R


---
class: inverse, middle, center 

# 1. Biodiversity databases: basic concepts

---
## Biodiversity data
### Museums, herbaria, collections

.pull-left[
```{r biod1, out.width=500, alt= "A shelf of the ichtyology collection at the National Museum (UFRJ), Rio de Janeiro, with samples fixed and stored in liquid"}
knitr::include_graphics("./figs/mz_ictio.jpg")
```
]

.pull-right[
```{r biod2, out.width=400, alt= "A shelf of the Rio de Janeiro Botanical Gardn herbarium (RB), with exsicattae. A person holds one open exsicatta, showing the collected plant"}
include_graphics("./figs/herbarioRB.jpg")
```
]

---
.pull-left[
## Who creates these data?

- Researchers: taxonomists, ecologists, field biologists
- Undergrad and grad school courses
- Amateur collectors, citizen science, using apps or not


]

--

.pull-right[
## Who uses them?

- Researchers
- Curators
- The overall community (e.g., bird watchers)

]

--

+ A __massive__ amount of data

--

+ New research areas from the analysis and synthesis of these data: Biogeography, macroecology

--

+ __However__: heterogeneous quality, inequality between countries and institutions


---

```{r bi, echo = F}
knitr::include_graphics("figs/bi.png")
```

---
## Biodiversity databases


- [GBIF](https://www.gbif.org/) (Global Biodiversity Information Facility)

--

- Country-wise biodiversity information systems: 
    + `r emo::ji("brazil")` [speciesLink](http://splink.cria.org.br/), Sistema de Informações sobre Biodiversidade Brasileira [SiBBr](https://www.sibbr.gov.br/)
    + `r emo::ji("colombia")` Sistema de Información sobre Biodiversidad [SibColombia](https://biodiversidad.co/)
    + `r emo::ji("mexico")` Sistema Nacional de Información sobre Biodiversidad [SNIB](https://snib.mx/)
    + `r emo::ji("argentina")` Sistema de Información de Biodiversidad [SIB](https://sib.gob.ar/#!/)

--

- Countries that participate via ministries, agencies

--

- Other systems: 

    - Natureserve/ BISON in the USA
    - [iNaturalist](https://www.inaturalist.org/)


---
background-image: url("https://docs.ropensci.org/rfishbase/logo.svg")
background-size: 150px
background-position: 90% 10%

## By taxonomic group

+ [World Flora Online](http://www.worldfloraonline.org/)

--

+ [Plants of the world](http://plantsoftheworldonline.org/) 

--

+ [International Plant Name Index (IPNI)](https://www.ipni.org/)

--

+ [Fishbase](https://www.fishbase.in/)

--

+ [Mammal Diversity](https://www.mammaldiversity.org/)



---
## `r include_graphics("./figs/Ropensci.png")`

> Transforming science through open data, software & reproducibility

--

- R packages to make data available, data cleaning, APIs

--

- __Package peer-review__

--

- Packages related to biodiversity data retrieval, manipulation, and standardization: `finch`, `rgbif`, `taxize`, `taxview` ...


---
## Looking at GBIF data

Let's download the data of a tree species from South America *Myrsine coriacea* from the Primulaceae family.

```{r loading pkg}
#| eval: false
#| warning: false
#| message: false
#| echo: true
library(rgbif)
library(dplyr)
species <- "Myrsine coriacea"
occs <- occ_search(scientificName = species)
names(occs)
#glimpse(occs)
#names(occs$data)
```

Column names returned from gbif follow the DarwinCore standard (https://dwc.tdwg.org).

---
## Possible fields


+ Species (taxonomic information)

--

+ Locality, coordinates, geographic

--

+ Collector notes and species attributes, IUCN status

--

+ Collectors, determiners, and authors' names



---
class: inverse, middle, center 

# 2.DarwinCore Standard

---
## [DarwinCore](https://dwc.tdwg.org/)

.pull-left[

- goal: __facilitate the sharing of information about biological diversity__ by providing identifiers, labels, and definitions

- maintained by a working group, adopted globally by GBIF and most collections, constant evolution

- [_reference guide_](https://dwc.tdwg.org/terms/): `Taxon`, `Event`, `Identification`, `Location`

]

.pull.right[
```{r darwin, fig.align='center', out.width = 350}
include_graphics("./figs/darwin.jpeg")
```
]

 <!-- ö more practical stuff--> 



---

```{r dwc-scheme, out.width=900, fig.align='center'}
include_graphics("./figs/dwc_scheme.png")
```

---
## Multi-file [DwC scheme](https://dwc.tdwg.org/)

+ a ZIP file with:

  + different data tables (__relational databases__)
  + description file (__XML__)
  + metadata files (__EML__ [Ecological Metadata Language](https://eml.ecoinformatics.org/))

--

+ __common columns__ for each table, `ID`


---


```{r gbif2, fig.width=3}
include_graphics("./figs/IPT_GBIF.png")
```

+ IPT is used to publish your data, __either in one single table__ or __extended__: species lists, field work samplings etc. Datasets are given a __permanent identifier and citation__

---
## `r include_graphics("./figs/IPT.png")`


> Brazil Flora G (2020): Brazilian Flora 2020 project - Projeto Flora do Brasil 2020. v393.274. Instituto de Pesquisas Jardim Botanico do Rio de Janeiro. Dataset/Checklist. doi:10.15468/1mtkaw URL: http://ipt.jbrj.gov.br/jbrj/

- 49.343 species

- 136.314 taxa

- Information about: distribution, habitat, endemism, references, synonyms

- IPT in `R`: package __finch__ ([Chamberlain 2020](https://CRAN.R-project.org/package=finch))

---
class: inverse, middle, center 

# 3. Data cleaning



---
## Primary data are not clean or updated

- Names and classifications change in time (synonyms):

  - International Commission on Zoological Nomenclature ([ICZN](https://www.iczn.org/)),  International Code of Botanical Nomenclature, [International Association for Plant Taxonomy](https://www.iapt-taxon.org/icbn/main.htm) 
  
  - The names of places: __toponyms__, we need _gazetteers_
  
- Taxonomic identification errors, georreferencing, typographic/orthographic

- Missing coordinates or localities

- Imprecise coordinates, badly attributed


---

## Spatial error


```{r gbif-sea, out.width=800, fig.align='center'}
include_graphics("figs/GBIF_g002.png")
```

[Yesson et al 2007](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC2043490/)



---

## Zero latitude and longitude

```{r gbif-zero,  out.width=800, fig.align='center'}
include_graphics("figs/GBIF_g003.png")
```

[Yesson et al 2007](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC2043490/)

---
## Taxonomic, nomenclatural erros

.pull-left[

+ Original vs. accepted name 

- Different taxonomic concepts / backbones, premisses, measure units.

- New errors arising in processing and junction

- Duplicate data vs. _duplicatae_

]

.pull-right[
```{r paubrasilia, fig.align='right', out.width=450}
knitr::include_graphics("figs/paubrasil.png")
```

]


---
## Data cleaning workflow

Check for

1. format
2. completeness
3. sense (dimensions, ranges)
4. outliers (geographic, temporary, environmental)


---
## Good practices: 

+ Always flag 🚩 additions, corrections, __never modify__ original data

+ Document all steps -> reproducibility

+ Cite packages and data sources



---

## Know your shortfalls

> informed ignorance is a powerful research tool

```{r hortal, fig.align='center'}
include_graphics("./figs/Hortaletal2015.png")
```

---

## References


Chapman, A. D. (2005). Principles and methods of data cleaning. GBIF.

Hortal, J., de Bello, F., Diniz-Filho, J. A. F., Lewinsohn, T. M., Lobo, J. M., & Ladle, R. J. (2015). Seven shortfalls that beset large-scale knowledge of biodiversity. Annual Review of Ecology, Evolution, and Systematics, 46, 523-549.

Yesson, C., Brewer, P. W., Sutton, T., Caithness, N., Pahwa, J. S., Burgess, M., ... & Culham, A. (2007). How global is the global biodiversity information facility?. PloS one, 2(11), e1124.

Wieczorek, J., Bloom, D., Guralnick, R., Blum, S., Döring, M., Giovanni, R., ... & Vieglais, D. (2012). Darwin Core: an evolving community-developed biodiversity data standard. PloS one, 7(1), e29715.

---
class: center, middle
# ¡Thanks!

<center>
`r icons::icon_style(icons::fontawesome("paper-plane", style = "solid"), scale = 1, fill = co)` [andreasancheztapia@gmail.com](mailto:andreasancheztapia@gmail.com) | [saramortara@gmail.com](mailto:saramortara@gmail.com)


`r icons::icon_style(icons::fontawesome("twitter", style = "brands"), scale = 1, fill = co)` [@SanchezTapiaA](https://twitter.com/SanchezTapiaA) | [@MortaraSara](https://twitter.com/MortaraSara)

`r icons::icon_style(icons::fontawesome("github", style = "brands"), scale = 1, fill = co)``r icons::icon_style(icons::fontawesome("gitlab", style = "brands"), scale = 1, fill = co)`
[andreasancheztapia](http://github.com/andreasancheztapia) | [saramortara](http://github.com/saramortara)
