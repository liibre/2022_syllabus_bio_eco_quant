---
title: "Introduction to biological diversity analysis"
description: |
author:
  - name: Andrea Sánchez-Tapia
    orcid_id: 0000-0002-3521-4338
    affiliation: ¡liibre!
    affiliation_url: {}
    url: https://andreasancheztapia.netlify.app
  - name: Sara Mortara
    affiliation: re.green
    url: {}
    orcid_id: 0000-0001-6221-7537
citation_url: https://scientific-computing.netlify.app/12_diversity_metrics_1.html
date: 2022-07-28
---

```{r xaringanExtra-clipboard, echo=FALSE}
htmltools::tagList(
  xaringanExtra::use_clipboard(
    button_text = "Copy code <i class=\"fa fa-clipboard\"></i>",
    success_text = "Copied! <i class=\"fa fa-check\" style=\"color: #90BE6D\"></i>",
    error_text = "Not copied :/ <i class=\"fa fa-times-circle\" style=\"color: #F94144\"></i>"
  ),
  rmarkdown::html_dependency_font_awesome()
)
xaringanExtra::use_share_again()
```



# Slides

```{r slides}
#| eval: true
#| echo: false
#| include: true
xaringanExtra::embed_xaringan("12_diversity_metrics_1/12_slides.html")
```

# Packages

```{r}
#| eval: false
install.packages("vegan")
```

# References

+ Pavoine, S., Bonsall, M.B., 2011. Measuring biodiversity to explain community assembly: a unified approach. Biological Reviews 86, 792–812. https://doi.org/10.1111/j.1469-185X.2010.00171.x
