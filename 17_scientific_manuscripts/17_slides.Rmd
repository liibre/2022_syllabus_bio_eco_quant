---
title: "Reproducible scientific manuscripts and reports"
subtitle: "Serrapilheira/ICTP-SAIFR Training Program in Quantitative Biology and Ecology"
author: "Andrea Sánchez-Tapia & Sara Mortara"
date: '10 August 2022'
output:
  xaringan::moon_reader:
    lib_dir: libs
    css: xaringan-themer.css
    nature:
      highlightStyle: github
      highlightLines: true
      countIncrementalSlides: false
      ratio: '16:9'
---

background-image: url("figs/rmarkdown.png")
background-position: 98% 2%
background-size: 100px

```{r setup, include=FALSE}
options(htmltools.dir.version = FALSE)
knitr::opts_chunk$set(echo = FALSE)
```

```{r xaringan-themer, include = FALSE}
library(xaringanthemer)
co <- "#A70000"
style_duo_accent(
  primary_color = co,
  secondary_color = "#ff0000",
  white_color = "white",
  inverse_header_color = "white",
  inverse_background_color = "#A70000",
  header_font_google = google_font("Roboto Condensed"),
  text_font_google   = google_font("Roboto Condensed", "300", "300i"),
  code_font_google   = google_font("Ubuntu Mono"), text_font_size = "28px"
)
```

```{r xaringanExtra-clipboard, echo=FALSE}
htmltools::tagList(
  xaringanExtra::use_clipboard(
    button_text = "Copy code <i class=\"fa fa-clipboard\"></i>",
    success_text = "Copied! <i class=\"fa fa-check\" style=\"color: #90BE6D\"></i>",
    error_text = "Not copied :/ <i class=\"fa fa-times-circle\" style=\"color: #F94144\"></i>"
  ),
  rmarkdown::html_dependency_font_awesome()
)
xaringanExtra::use_share_again()
```


## Writing and data analysis workflows

+ Non-linear: Come back and forth between coauthors, updating with updated results

--

+ __Copy and pasting results is error prone!__

--

+ Formatting, reference citing, adding figures can be cumbersome 

--

+ Depending on the area, we are mostly used to MS Office, Google docs and similar for collaborative writing

--

+ In some areas, researchers use $\LaTeX$ but it has a learning curve


---

## A friendly writing workflow would allow us to

--

+ Write as we analyze

--

+ Integrate code and text: _Literate programming_ ([Knuth 1984](http://www.literateprogramming.com/knuthweb.pdf))

--

+ Facilitate coauthors __revision__

--

+ __Update__ if there are changes

--

+ Allow __citing__, __formatting__ and __submitting__ 

--

####  We can use the pros of working with scripts + version control and be transparent 

---
## Where are you today?

1. Do you use already a reference manager?

2. Do you use `.bib` and know how to create one?

3. Do you know about __rmarkdown__?

3. Do you know how to cite from `.bib` in markdown?

3. Do you know about $\LaTeX$?

4. Do you use $\LaTeX$ locally or using Overleaf? 

5. Do you use `git` to version control your writing process?


---
## <huge> $\LaTeX$ </huge>

+ Document preparation system with __high typographic quality__

--

+ Text files with `.tex` extension. Output format: __PDF__

--

+ Text files: can be used with __version control__ such as `git`

--

+ Deals correctly with math, scientific, algebraic __notation__

--

+ __Typesetting__ (pagination, figure posititoning, general formatting)

--

+ $\LaTeX$ files can be __submitted__ directly to some journals +  dissertation templates in many institutions

--

#### A good way to learn and collaborate: Overleaf https://www.overleaf.com/ 


---
background-image: url(https://bookdown.org/yihui/rmarkdown-cookbook/images/workflow.png)
background-size: contain
## markdown and rmarkdown

.footnote[https://bookdown.org/yihui/rmarkdown-cookbook/rmarkdown-process.html]

---
## Markdown markup

.pull-left[
`text`  
`__bold__` and `**bold**`  
`superscript^2^`    
`~~strikethrough~~`   
`[link](www.rstudio.com)`  
`# Heading 1`  
`## Heading 2`  
]

.pull-right[
text
__bold__  and **bold**  
superscript<sup>2</sup>    
~~strikethrough~~   
[link](www.rstudio.com) 
# Heading 1  
## Heading 2  
]

---
## Markdown markup

.pull-left[
`+ unnumbered list`  
`+ unnumbered list`

`1. numbered list`  
`1. numbered list`
]

.pull-right[
+ unnumbered list  
+ unnumbered list

1. numbered list
1. numbered list

]

And all __html__ commands!

`<center> </center>`  

`You can hide text <!-- with html comments -->`  

You can hide text <!-- with html comments -->

---
background-image: url(https://raw.githubusercontent.com/ThinkR-open/remedy/master/reference/figures/thinkr-hex-remedy.png)
background-size: 150px
background-position: 98% 2%

## help when writing: package __remedy__

```{r}
#| eval: false
remotes::install_github("ThinkR-open/remedy")
```

+ Documentation: https://github.com/ThinkR-open/remedy

+ Installs as an RStudio addin

+ You can modify the keyboard shortcuts

`Tools > Modify keyboard shortcuts`

+ `ctrl + b` -> bold
+ `ctrl + i` -> italic
+ `ctrl + k` -> link
+ `ctrl + 1` -> h1 (level 1 header)
+ `ctrl + 2` -> h1 (level 2 header)
+ `ctrl + r` -> insert chunk

---
## To add external figures

+ Native markdown: `![alt-text](/figs/fig.png)`

--

+ HTML: `<img src ---->`

--

+ $\LaTeX$: `\includegraphics{}`

--
 
+ __For all formats:__ `knitr::include_graphics(/figs/fig.png)`
(inside a  _chunk_)

--

#### Markdown has support for alternative text! `fig.alt =` parameter
  

---
## Tables


.pull-left[

Format

`| name1 | name2 | name3 |`  
`|:-----:|:-----:|:-----:|`  
`| text1 | text2 | text3 |`  
`| text1 | text2 | text3 |`  
]

.pull-right[
Result

| name1 | name2 | name3 |
|:-----:|:-----:|:-----:|
| text1 | text2 | text3 |
| text1 | text2 | text3 |

]

--

+ From R chunks:
    + `knitr::kable()` 
    + Package [kableExtra](https://cran.r-project.org/web/packages/kableExtra/vignettes/awesome_table_in_html.html)

--

+ Some services to create tables with the format: __[markdown table generator](http://www.tablesgenerator.com/markdown_tables)__
and HackMD (good support for tables).


---
class: center, middle, inverse

# Bibliographical reference management

---
background-image: url("https://www.zotero.org/static/images/theme/zotero-logo.1519224037.svg")
background-size: 20%
background-position: 90% 0%


## Reference managers

+ Save citations, publication files (optional), assist in the citation and bibliographies

--

+ __Zotero__, __Mendeley__, EndNote, Reference Manager  

--

+ To cite in $\LaTeX$ and markdown: [BibTeX](http://www.bibtex.org/), File format: `.bib`

--

```{r remedy001}
toBibtex(citation("vegan"))
```


---
## Using zotero or mendeley

+ Reference storage and management in __a local app__

--

+ A __browser extension__ _downloads_ citation and PDF if available

--

+ A __connector__ for MS Word and LibreOffice

--

+ A __citation styles__ repository: [Zotero style repository](https://www.zotero.org/styles). File extension: `.csl` 

--

+ A __site__ that creates citations from URL or DOI [https://zbib.org/](https://zbib.org/)

--

+ A way to create bibliographies in `.bib` __BiBTex__, __Zotero__(`BetterBibTex`), and __Mendeley__: create `.bib`  for each collection, keeps updated


---
## To cite documents in __rmarkdown__

+ With `refs.bib` and `style.csl` in `/citation` folder include `.bib` and `.csl` in the YAML header:
  + `bibliography: ../citation/refs.bib`
  + `csl: ../citation/nature.csl`

--

+ Cite format: `@name_word_year` (check in the reference manager for the __citation key__)

--

  + Ex. `[@dignazio_data_2020]` -> (D'Ignazio & Klein 2020)
  + Ex. `@dignazio_data_2020` -> D'Ignazio & Klein (2020)
  + Ex. `[@noble_algorithms_2018, @dignazio_data_2020]` -> (Noble 2018, D'Ignazio & Klein 2020)

--

+ Default: the bibliography is added at the end of the text, but can be placed anywhere with: `<div id="refs"></div>` 



---
## Creating a `.bib` in Mendeley

```{r, out.width=600, fig.align='center'}
knitr::include_graphics("./figs/mendeley.png")
```

---
## Creating a `.bib` in Zotero

```{r,out.width=600, fig.align='center'}
knitr::include_graphics("./figs/exportar_bibtex.png")
```




---
background-image: url("./figs/0newcollection.png")
background-size: 100%

__Create the collection__

---
background-image: url("./figs/1tukey_amazon.png")
background-size: 100%

__You can download directly from the internet, even Amazon__

---
background-image: url("./figs/2salvando.png")
background-size: 100%

__Click to save__

---
background-image: url("./figs/3app.png")
background-size: 100%

__In the app, BetterBibTex creates the key__

---
background-image: url("./figs/4export.png")
background-size: 100%

__Export the collection__

---
background-image: url("./figs/5export.png")
background-size: 100%

__BetterBibTeX - keep updated!__


---
background-image: url("./figs/6bib.png")
background-size: 80%

__the .bib is a text file__

---
background-image: url("./figs/7csl.png")
background-size: 100%

__download citation styles__

---
background-image: url("./figs/7_bib_e_csl.png")
background-size: 100%





---
# Other packages based in markdown

- __bookdown__ create whole books from several `.Rmd`

- __pagedown__ templates for thesis, CVs, in html

- __rticles__: $\LaTeX$ templates, accepted by journals (plos, Elsevier, Frontiers, etc.) only PDF!

```r
remotes::install_github("rstudio/rticles")
rticles::journals()
```

- __thesisdown__ with university templates. Some variations: __coppedown__, __ufscdown__


---
## Some final tips 

- __Resist the temptation__ to create a single document mixing text and code (__Don't do things just because you can__) 
 
 - _Scripts_ and _outputs_ in separate folders than documents

--

 - Several chapters - several folders, don't write in a single `.Rmd`

--

- Don't choose formats too early but don't leave this to the end


---
## References

+ Rosanna van Hespen: Writing your thesis with R https://www.rosannavanhespen.nl/2016/02/16/writing-your-thesis-with-r-markdown-1-getting-started/

- R Markdown: The Definitive Guide https://bookdown.org/yihui/rmarkdown/

- R markdown cookbook: https://bookdown.org/yihui/rmarkdown-cookbook/

- https://rmarkdown.rstudio.com
https://bookdown.org/yihui/rmarkdown/interactive-documents.html


---
class: center, middle
# ¡Thanks!

<center>
`r icons::icon_style(icons::fontawesome("paper-plane", style = "solid"), scale = 1, fill = co)` [andreasancheztapia@gmail.com](mailto:andreasancheztapia@gmail.com) | [saramortara@gmail.com](mailto:saramortara@gmail.com)


`r icons::icon_style(icons::fontawesome("twitter", style = "brands"), scale = 1, fill = co)` [@SanchezTapiaA](https://twitter.com/SanchezTapiaA) | [@MortaraSara](https://twitter.com/MortaraSara)

`r icons::icon_style(icons::fontawesome("github", style = "brands"), scale = 1, fill = co)``r icons::icon_style(icons::fontawesome("gitlab", style = "brands"), scale = 1, fill = co)`
[andreasancheztapia](http://github.com/andreasancheztapia) | [saramortara](http://github.com/saramortara)

