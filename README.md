[![Netlify Status](https://api.netlify.com/api/v1/badges/cb7f5643-e2c7-4e1a-b239-f7253015ba07/deploy-status)](https://app.netlify.com/sites/scientific-computing/deploys)

https://scientific-computing.netlify.app/

# Introduction to R in Biology and Ecology


This material is part of the Serrapilheira/ICTP-SAIFR Training Program in Quantitative Biology and Ecology held at ICTP from July-December 2022.

The goal of the course is to provide basic tools to create a data analysis project, learn R basics in Biology and Ecology, and help to navigate the R community in the search for specific solutions. The main goal is to make students empowered to create their own scientific workflow for an analytic project in R making use of basic reproducibility tools.

## Program

| Week        | Day 1                    | Day 2                     | Day 3                                        |
|:----------- |:------------------------ |:------------------------- |:-------------------------------------------- |
| July 4-8    | Introduction to R        | Introduction to git       | Scientific workflows using R                 |
| July 11-15  | Data manipulation in R   | Exploratory Data Analysis | Statistical modelling I                      |
| July 18-22  | Statistical modelling II | Graphics with R           | Implementing dynamic and stochastic dynamics |
| July 25-29  | Spatial tools            | Biodiversity databases    | Diversity metrics                            |
| August 1-5  | Diversity metrics II     | Multivariate analysis I   | Multivariate analysis II                     |
| August 8-12 | Predictive modeling      | Scientific reports with R | Open Science, Open Methods, and communities  |



## Authors
Sara Mortara and Andrea Sánchez-Tapia

## License
All resources are available under creative commons licence (CC BY-SA 4.0)

## Project status
This content is currently under development

## References

Annesley, T. M. (2010). Who, What, When, Where, How, and Why: The Ingredients in the Recipe
for a Successful Methods Section. Clinical Chemistry, 56(6), 897–901.
doi:10.1373/clinchem.2010.146589

Baker, M. (2016). Reproducibility crisis. Nature, 533(26), 353-66.

Baumer, B., Cetinkaya-Rundel, M., Bray, A., Loi, L., & Horton, N. J. (2014). R Markdown:
Integrating A Reproducible Analysis Tool into Introductory Statistics. ArXiv:1402.1894 [Stat].
Retrieved from http://arxiv.org/abs/1402.1894 Biotaxa: Online library for taxonomic journals. (n.d.). Retrieved February 4, 2019, from
https://www.biotaxa.org/

Borregaard, M. K., & Hart, E. M. (2016). Towards a more reproducible ecology. Ecography, 39(4),
349–353. doi:10.1111/ecog.02493

Bryan, J. (2018). Project-oriented workflow - Tidyverse. Retrieved February 5, 2019, from
https://www.tidyverse.org/articles/2017/12/workflow-vs-script/

Cassey, P., & Blackburn, T. M. (2006). Reproducibility and repeatability in ecology. BioScience, 56(12), 958-959.

Chamberlain, S. A., & Szöcs, E. (2013). taxize: taxonomic search and retrieval in R.
F1000Research. doi:10.12688/f1000research.2-191.v2

D'Ignazio, C., & Klein, L. F. (2020). Data feminism. MIT Press.

Dixon, P. (2003). VEGAN, a package of R functions for community ecology. Journal of Vegetation Science, 14(6), 927-930.

Gewin, V. (2016). Data sharing: An open mind on open data. Nature, 529(7584), 117–119.

Gotelli, N. J. (2008). A primer of ecology (Vol. 494). Sunderland, MA: Sinauer Associates.

Gotelli, N. J., & Ellison, A. M. (2004). A primer of ecological statistics (Vol. 1). Sunderland: Sinauer Associates.

Hampton, S. E., Anderson, S., Bagby, S. C., Gries, C., Han, X., Hart, E., others. (2014). The tao of
open science for ecology. PeerJ PrePrints. Retrieved from https://peerj.com/preprints/549/

Kembel, Steven W., Peter D. Cowan, Matthew R. Helmus, William K. Cornwell, Helene Morlon, David D. Ackerly, Simon P. Blomberg, and Campbell O. Webb. "Picante: R tools for integrating phylogenies and ecology." Bioinformatics 26, no. 11 (2010): 1463-1464.

Lowndes, J. S. S., Best, B. D., Scarborough, C., Afflerbach, J. C., Frazier, M. R., O’Hara, C. C., … Halpern, B. S. (2017). Our path to better science in less time using open data science tools. Nature Ecology & Evolution, 1(6), s41559-017-0160–017. doi:10.1038/s41559-017-0160

Marwick, B., Boettiger, C., & Mullen, L. (2017). Packaging data analytical work reproducibly using
R (and friends). PeerJ Preprints. doi:/10.7287/peerj.preprints.3192v1

Mislan, K. A. S., Heer, J. M., & White, E. P. (2016). Elevating the status of code in Ecology.
Trends in Ecology & Evolution, 31(1), 4–7. doi:http://dx.doi.org/10.1016/j.tree.2015.11.006

Noble, W. S. (2009). A Quick Guide to Organizing Computational Biology Projects. PLOS
Computational Biology, 5(7), e1000424. doi:10/fbbpkn

Penev, L., Kress, W. J., Knapp, S., Li, D.-Z., & Renner, S. (2010). Fast, linked, and open – the
future of taxonomic publishing for plants: launching the journal PhytoKeys. PhytoKeys, (1),
1–14. doi:10/chv8xq

Peng, R. (2015). The reproducibility crisis in science: A statistical counterattack. Significance,
12(3), 30–32. doi:10.1111/j.1740-9713.2015.00827.x

Piccinini, P. (2015, December 30). R Course. Retrieved February 4, 2019, from
https://pagepiccinini.com/r-course/

Powers, S. M., & Hampton, S. E. (2019). Open science, reproducibility, and transparency in ecology. Ecological Applications, 29(1), e01822.

Reichman, O. J., Jones, M. B., & Schildhauer, M. P. (2011). Challenges and Opportunities of Open Data in Ecology. Science, 331(6018), 703–705. doi:10.1126/science.1197962 

Shade, A., & Teal, T. K. (2015). Computing Workflows for Biologists: A Roadmap. PLoS Biol,
13(11), e1002303. doi:10.1371/journal.pbio.1002303

Soetaert, K., Petzoldt, T., & Setzer, R. W. (2010). Solving differential equations in R: package deSolve. Journal of statistical software, 33, 1-25.

Stevens, M. H. H. (2009). A Primer of Ecology with R. New York: Springer.

Strasser, C. A., & Hampton, S. E. (2012). The fractured lab notebook: undergraduates and
ecological data management training in the United States. Ecosphere, 3(12), art116.
doi:10.1890/ES12-00139.1

Toczydlowski, R. H. (2017). An Efficient Workflow for Collecting, Entering, and Proofing Field
Data: Harnessing Voice Recording and Dictation Software. The Bulletin of the Ecological
Society of America, 98(4), 291–297. doi:10.1002/bes2.1334

Tukey, J. W. (1977). Exploratory Data Analysis (Vol 2).

Vellend, M. (2016). The theory of ecological communities (MPB-57). Princeton University Press.

Wickham, H. (2011). ggplot2. Wiley interdisciplinary reviews: computational statistics, 3(2), 180-185.

Xie, Y., Allaire, J. J., & Grolemund, G. (2018). R markdown: The definitive guide. Chapman and Hall/CRC.

Zuur, A. F., Ieno, E. N., & Elphick, C. S. (2010). A protocol for data exploration to avoid common statistical problems. Methods in Ecology and Evolution, 1(1), 3–14. doi:10/cw57t3







