---
title: "Introduction to Spatial data with R"
subtitle: "Serrapilheira/ICTP-SAIFR Training Program in Quantitative Biology and Ecology"
author: 
  - "Andrea Sánchez-Tapia & Sara Mortara"
date: '26 July 2022'
output: 
  xaringan::moon_reader:
    lib_dir: libs
    css: xaringan-themer.css
    nature:
      highlightStyle: github
      highlightLines: true
      countIncrementalSlides: false
      ratio: '16:9'
---
class: middle

```{r setup, include=FALSE}
options(htmltools.dir.version = FALSE)
```

```{r xaringanExtra-clipboard, echo=FALSE}
htmltools::tagList(
  xaringanExtra::use_clipboard(
    button_text = "Copy code <i class=\"fa fa-clipboard\"></i>",
    success_text = "Copied! <i class=\"fa fa-check\" style=\"color: #90BE6D\"></i>",
    error_text = "Not copied :/ <i class=\"fa fa-times-circle\" style=\"color: #F94144\"></i>"
  ),
  rmarkdown::html_dependency_font_awesome()
)
xaringanExtra::use_share_again()
```

```{r xaringan-themer, include = FALSE}
library(xaringanthemer)
co <-  "#A70000"
xaringanthemer::style_mono_accent(
  base_color = co,
  white_color = "white",
  colors = c(
    red = "#A70000",
    purple = "#88398a",
    orange = "#ff8811",
    green = "#136f63",
    blue = "#4B4FFF",
    white = "#FFFFFF",
    black = "#181818"
  ),
  header_font_google = google_font("Roboto Condensed"),
  text_font_google   = google_font("Roboto Condensed", "300", "300i"),
  code_font_google   = google_font("Ubuntu Mono"),
  text_font_size = "30px",
  code_font_size = "30px",
  )
```

```{r}
#| echo: false
#knitr::include_graphics("figs/R-LadiesGlobal.png", dpi = 500)
#knitr::include_graphics("figs/forwards.png", dpi = 800)
#knitr::include_graphics("figs/logo.jpg", dpi = 1000)
knitr::include_graphics("figs/ast2.png")

```

---
## Introduction

+  __Geographic Information System (GIS)__ platforms. Powerful but point-and-click, some integrations. License needed!

--

+ __Free and open source software for geospatial (FOSS4G)__: R, Python, GRASS, QGIS 

--

+ __Widely used__ external open source geo libraries written in C or C++: `GDAL`, `PROJ`, [`GEOS`](https://libgeos.org/)- Recent changes.

--

+ In R: supported for years, heterogeneous. Classic packages (`rgdal`, `rgeos` and `maptools`) will retire, lots of new packages (`stars`, `terra`, `leaflet`, `mapview`)

--

+ `r emo::ji("package")` __Today (2022)__: `raster`, [`sf`](https://r-spatial.github.io/sf/), [`tmap`](https://github.com/mtennekes/tmap) 


---
class: inverse, middle, center

## Vectorial and raster data

---
## Vectorial data



.pull-left[

```{r, out.width=480}
#| echo: false
knitr::include_graphics("figs/1_map.png")
```

]

.pull-right[


- __points__: (x, y) pairs and multipoints (e.g. all cities in a country)

- __lines__: several pairs (nodes), ordered. Polylines. Multiple (poly)lines (e.g.: a river and its tributaries, all the river systems in a region)

- __polygons__: Ordered polylines, _ending at the starting point_. They can have __holes__, they do not cross themselves. Multiple polygons (e.g.:an archipelago)

]

---
class: center

## Types of vectorial data 

```{r}
#| echo: false
#| out.width: 700
knitr::include_graphics("figs/features.png")
```

---

## File types: shapefiles

+ Vector data are usually stored as __shapefile__ formats

--

+ Three files: `.shp`, `.shx`, `.dbf` and a projection `.prj`

--

+ Other extensions also refered to as shapefiles: Geopackages

--

+ In R: package `sf`.

--

+ `sf::read_sf()`, `sf::write_sf()` 

---

## Raster data

.pull-left[
```{r}
#| echo: false
#| messsage: false
#| warnings: false
library(raster, quietly = TRUE)
if (interactive()) r <- raster("10_spatial_tools/data/BRA_msk_alt/BRA_msk_alt.grd") 
r <- raster("data/BRA_msk_alt/BRA_msk_alt.grd") 
plot(r, main = "Altitude")
```

]

.pull-right[

+ __Continuous space__ divided in a cell grid

+ __One value per cell__: can be the average inside the cell, a sample in the center, the result of an operation including contiguous cells

+ __Spatial extent__,  __resolution__ , (nrows, ncols, nlayers): `r dim(r)`
    
+ A __stack__ of rasters, or __multiband__ rasters

+ In R: `raster`, several filetypes: `.asc`, `.grd`, `.tif`
]


---
## Where to find geospatial data? 

- __Direct download__ in official country organizations (ex. the shapefile for a protected area)

--

- Spatial data __repositories__ (ex. Global Administrative Areas http://gadm.org, administrative information for all countries)

--

- Remote sensing data: Satellite (ex. MODIS, Sentinel, Landsat)

--

- Some R packages have map templates for quick mapping and options for downloading data (ex. `raster`) 



---
class: center, middle

```{r}
#| echo: false
#| out.width: 900
knitr::include_graphics("figs/rspatial.png")
```

### https://rspatialdata.github.io


---

## Coordinate reference systems, datums, projections

+ A __Coordinate Reference System__ is a standard to locate objects in the geographic space

--

+ The current default: __WGS84__ (World Geodetic System 1984).

--

+ Official CRS appropriate for regions and countries. Brazil: South America SAD69, Sirgass 2000, USA: NAD27, NAD83.
 

---
## CRS are composed by

+ A __spheroid__, a model of the shape of the Earth

+ A __datum__, a reference for the coordinates, their origin and unit of measurement

+ A __projection__, how to transform 3-D system to 2-D,
Mercator, UTM, Robinson Lambert, Sinusoidal, Robinson, Albers
    
+ Lots of locally used CRS and one world standard: WGS84. Lots of deprecated/updated CRS. A repository with all the CRS: `https://spatialreference.org/`

WGS: https://spatialreference.org/ref/epsg/4326/

---
## CRS are key for correct spatial analysis

+ When downloading data from official sources, check the __metadata__ available

+ When missing, usually assume __WGS84__ but triple check with the data sources and see __what makes sense in your context__. You an assign a CRS to an object with a missing CRS

###  Assigning a missing CRS does not make it correct and is not the same as re projecting


---
### This is only the beginning

+ Spatial statistics
 
+ Movement ecology 

+ Epidemiology 

+ Biogeography 

+ Geostatistics 



---
## Resources

+ Moraga, Paula. (2019). __Geospatial Health Data: Modeling and Visualization with R-INLA and Shiny.__ Chapman & Hall/CRC Biostatistics Series https://www.paulamoraga.com/book-geospatial/
+ __Spatial Data Science with R__ https://rspatial.org/
+ __Leaflet tutorial__ https://rstudio.github.io/leaflet/
+ Laurie Baker's tutorial with leaflet https://laurielbaker.github.io/DSCA_leaflet_mapping_in_r/slides/leaflet_slides2.html#66
+ __Geocomputation with R__ https://geocompr.robinlovelace.net/
+ RSpatial data: https://rspatialdata.github.io/
+ Fortin MJ, Dale MRT 2005. Spatial Analysis: A guide for Ecologists, Third Edit. ed. New York.

`r emo::ji("heart")`

---
class: center, middle
# ¡Thanks!

<center>
`r icons::icon_style(icons::fontawesome("paper-plane", style = "solid"), scale = 1, fill = co)` [andreasancheztapia@gmail.com](mailto:andreasancheztapia@gmail.com) | [saramortara@gmail.com](mailto:saramortara@gmail.com)


`r icons::icon_style(icons::fontawesome("twitter", style = "brands"), scale = 1, fill = co)` [@SanchezTapiaA](https://twitter.com/SanchezTapiaA) | [@MortaraSara](https://twitter.com/MortaraSara)

`r icons::icon_style(icons::fontawesome("github", style = "brands"), scale = 1, fill = co)``r icons::icon_style(icons::fontawesome("gitlab", style = "brands"), scale = 1, fill = co)`
[andreasancheztapia](http://github.com/andreasancheztapia) | [saramortara](http://github.com/saramortara)
