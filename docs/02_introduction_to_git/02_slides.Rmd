---
title: "Introduction to version control using git"
author: "Sara Mortara & Andrea Sánchez-Tapia"
institute: "re.green | ¡liibre!"
date: "2022-07-06"
output:
  xaringan::moon_reader:
    lib_dir: libs
    css: xaringan-themer.css
    nature:
      highlightStyle: github
      highlightLines: true
      countIncrementalSlides: false
      ratio: '16:9'
self-contained: true
---

```{css css-config, echo = FALSE}
.tiny .remark-code { /*Change made here*/
  font-size: 50% !important;
}
```

```{r xaringanthemer, include = FALSE}
library(xaringanthemer)
xaringanthemer::style_mono_accent(
  base_color = "#A70000",
  #  primary_color = "#A70000",
  #secondary_color = "#ff0000",
  white_color = "white",
  colors = c(
    red = "#A70000",
    purple = "#88398a",
    orange = "#ff8811",
    green = "#136f63",
    blue = "#4B4FFF",
    white = "#FFFFFF",
    black = "#181818"
  ),
  header_font_google = google_font("Roboto Condensed"),
  text_font_google   = google_font("Roboto Condensed", "300", "300i"),
  code_font_google   = google_font("Ubuntu Mono"),
  text_font_size = "30px",
  code_font_size = "30px",
  )
xaringanExtra::use_share_again()

# clipboard
htmltools::tagList(
  xaringanExtra::use_clipboard(
    button_text = "Copy code <i class=\"fa fa-clipboard\"></i>",
    success_text = "Copied! <i class=\"fa fa-check\" style=\"color: #90BE6D\"></i>",
    error_text = "Not copied 😕 <i class=\"fa fa-times-circle\" style=\"color: #F94144\"></i>"
  ),
  rmarkdown::html_dependency_font_awesome()
)

```

## today

+ introduction to git


+ git setup 


+ first `commit`

---

```{r setup, include=FALSE}
options(htmltools.dir.version = FALSE)
```

```{r, eval = F}
project/
    ├── data/
    ├── docs/
    ├── figs/
    ├── R/
        └── script.R  #<<
    ├── output/
    └── README.md
```

---
## how to work?

```{r, eval = F}
project/
    ├── data/
    ├── docs/
    ├── figs/
    ├── R/
        └── script.R  #<<
    ├── output/
        └── result.csv  #<<
    └── README.md
```

---
## how to work?

```{r, eval = F}
project/
    ├── data/
    ├── docs/
    ├── figs/
    ├── R/
        └── script.R  #<<
        └── script_modified.R  #<<
    ├── output/
        └── result.csv  #<<
        └── result2.csv  #<<
    └── README.md
```

---
## how to work?

```{r, eval = F}
project/
    ├── data/
    ├── docs/
    ├── figs/
    ├── R/
        └── script_old.R #<<
        └── script_modified.R  #<<
    ├── output/
        └── result_to_erase.csv #<<  
        └── result2.csv  
        └── result_final.csv  #<<
    └── README.md
```

---
## how to work?

```{r, eval = F}
project/
    ├── data/
    ├── docs/
    ├── figs/
    ├── old/ #<< 
    ├── R/
        └── 20220210_script.R  #<<
    ├── output/
        └── result_final.csv #<<
        └── 20220212_result_final.csv  #<<
    └── README.md
```

---
background-image:url("./figs/phdcomics.gif")
background-size: 40%
---
## and ...

+ what's the difference between `results_1.csv` e `results_2.csv`?


+ the date `20220210_script.R` is the date you created the script? or the last modification? 


+ any file was removed? any chunck of code is missing? 

<center>

```{r, echo = F, out.width=250}
knitr::include_graphics("https://media.giphy.com/media/JIX9t2j0ZTN9S/giphy.gif")
```

</center>

---
## version control 

+ systems that allow: 

  + record changes through time
--


  + go back if a mistake was taken
--


  + understand the difference between two versions of the same file
--


  + colaboration: __share__ analysis and __work collectivelly__ 
  
---

```{r, echo = F, out.width=200}
knitr::include_graphics("./figs/logo-git.png")
```

+ software __libre__
+ distributed (no one has a "main copy")
+ __clones__  and ___branches___ among computers or users

```{r, echo = F, out.width=300}
knitr::include_graphics("./figs/GitHub_Logo.png")
knitr::include_graphics("./figs/gitlab-logo-gray-rgb.png")
knitr::include_graphics("./figs/btibucket.png")
```

+ __web host__ for git repositories (___remotes___)
+ tools to facilitate colaboration


---
```{r, echo = F, out.width=200}
knitr::include_graphics("./figs/logo-git.png")
```

+ detailed control of __text files__: `.txt`, `.csv`, `.md`, `.R`
+ can include other type of files (binary)
+ the user decides __which files__ to include
+ serves __locally__ and __offline__
+ comunicates with __remote servers__ working as _backup_ and distribution (colaboration)

### work > __change__ >  __save a version__ > __send version to remote__

---
## four states

+ _working directory_: file added to be monitored -> modifications will be tracked
--


+ _staging area_: `add` files/changes added
--


+ `commit`: creates a version with added files
--


+ `push`: sends to the remote new commits



__several files per _commit_, several _commits_ per _push___


---
## a basic workflow (without ramifications)

```{r, echo=F}
knitr::include_graphics("./figs/basic.png")
```


---
## a basic workflow (without ramifications)

```{r, echo=F}
knitr::include_graphics("./figs/basic_main.png")
```

--

+ sequential commits at one or several computers
+ you from the past is your best collaborator `<3`


---
class: middle, center

```{r, echo=F}
knitr::include_graphics("./figs/commits.png")
```

---
## basic git commands (__terminal__)

`git init` starts a git repository
`git clone URL` clones an already existent repository

--

`git status` checks the current status

--

`git pull origin main` updates the repository locally

--

`git add filename` adds a new file or changes in a file being tracked

--

`git commit -m "an informative message"` __creates a version__

---
## local conection (git) to a remote (GitHub)

`git remote -v` list the remote

--

`git remote add origin [URL]` add remote

--

`git push origin main` - send local _commits_ to github

--

`git pull origin main` - pull _commits_ from github to the local computer

---
## workflow - Fist time

`git init`  
--

`git remote add`  
--

`(...)`  
--

`git add file1 (new)`  
--

`(...)`  
--

`git add file2 (new)`  
--

`git commit`  
--

`(...)`  
--

`git add file1 (modified)`  
--

`(...)`  
--

`git add file3 (new)`  
--

`git commit`  
--

`git push -u`  

---

## next times

`git pull origin main` (__essential__)   
--

`(...)`  
--

`git add file4`  
`git add file2 (modified)`  
`etc.`  
--

`git commit`  
--

`git push origin main`  


---
class: middle, center

`r icons::fontawesome("laptop-code")`

# lets set git configurations and convert out RStudio project into a git repository

---
## 0. global configuration

```
git config --global user.name [your name]
git config --global user.email [your github email!]
```

---
## 1. project strucutre

```
project/
*    ├── data/
*    │   ├── raw
*    │   └── processed
     ├── docs/
*    ├── figs/
     ├── R/
*    ├── output/
*    └── README.md
```

---
## 2. git init

```
git status
git init
git remote -v
```

---
## 3. git commit

1. Edit your README.md

```
git add README.md
git commit -m "I made the changes because it felt good"
```

---
### 4. RSA key - part 1 RStudio

1. In the RStudio options, look for the option `Preferences > git/svn`
1. Check that git is pointing to a file `git.exe` on windows, mac and linux `/usr/bin/git`
3. If you have never done this, there should be nothing in the RSA key field, click `Create RSA Key`. If you already have something, go to the next step.
5. View the RSA Key, copy the key. It is a key that identifies your computer and we will copy it from GitHub.com

---
### 4. RSA key - part 2 GitHub

1. Log in
2. Find __Settings > SSH and GPG keys > create a ssh__ key
4. In title: your computer name
5. Paste the key that had been copied.
6. Add, OK.

__This key configuration only needs to be done once on each computer.__

---
### 5. Create a new repository on GitHub

1. New repository
2. Enter a name, create as public without a README
3. Copy the SSH key

---
### 6. Adding remote repository

`git remote add origin` + paste content with `ctrl + v`

`git remote add origin git@github.com:AndreaSanchezTapia/blah.git`

`git remote -v`

--- 
### 7. Push and check the remote


`git push -u origin main`

Go to your remote repository and check if the commit has been added to history

---
## exploring GitHub

+ What types of files do you recognize?
+ When the repository was created?
+ When was the last _commit_?

+ GitHub: __Issues__

---
## general tips


+ GitHub __is not a clowd__ for file _backup_
--



+ control only __text files__ (simple text or code)
--



+ use the file `.gitignore` so git can ignore files that won't be tracked and exist only locally 



---
## `.gitignore`

+ __Git will only control what you add__
--


+ __It's not necessary to control all__ (drafts, tests, binary files): the file `.gitignore`
--


+ __Large files__ (*.tiff) will run out the repository space (2G GitHub and Bitbucket, 10G GitLab)
--


+ Binary files (*.pdf, +.docx) can be added but use space and  __git won't track detailed modifications__
--


+ accept general expressions:
  `*.pdf`
  `/docs`
  `*.docx`
  `*.xlsx`

  
---
## from RStudio

`r icons::fontawesome("laptop-code")`


+ open the RStudio project: `git` tab

.pull-left[
![](./figs/git_status.png)
]

---
class: middle, center
## working with _branches_ and _forks_

---
background-image:url("./figs/feature-branch.png")

## feature workflow

---
background-image:url("./figs/gitflow.png")

## _gitflow_: protect the branch `main`

---
background-image:url("./figs/jennifer_gilbert.png")

---
## working with _branches_

+ one user can work in several _branches_


+ several users can work at the same repository using different _branches_


+ each user can have their own copy of the repository ("_fork_") and still collaborate by `merge` and `pull request` from GitHub


---
## working with _branches_

--

`git branch NAME` to create a _branch_ 

--


`git checkout NAME` to switch to a different _branch_

--


`git merge branch1 branch2` merge _branches_

---
## git can be complex

<center>
![](figs/conflicts.jpeg)

---
## conflicts

+ solve manually


+ clone from zero and fix the errors

---
background-image: url(./figs/xkcd.png)
class: center, middle

---
## good practices

1. describe in the commit __why__

1. one big task at each _branch_

1. use `.gitignore` to ingnore files that shouldn't be tracked (ex. specific OS files)

1. use the mode `diff` to follow changes in the code

1. report code errors in `issues` at GitHub


---

## today: 

+ set git in our computer

+ converted our RStudio project into a git repository

+ created a remote repository for our local repository

+ modified a file and saved in one _commit_

+ will start using this workflow structure


```{r, echo=FALSE}
knitr::include_graphics("https://acegif.com/wp-content/uploads/2020/b72nv6/partyparrt-21.gif")
```


---
class: center, middle
# ¡Thanks!

<center>
`r icons::icon_style(icons::fontawesome("paper-plane", style = "solid"), scale = 1, fill = "#A70000")` [saramortara@gmail.com](mailto:saramortara@gmail.com) | [andreasancheztapia@gmail.com](mailto:andreasancheztapia@gmail.com) 


`r icons::icon_style(icons::fontawesome("twitter", style = "brands"), scale = 1, fill = "#A70000")` [@MortaraSara](https://twitter.com/MortaraSara) | [@SanchezTapiaA](https://twitter.com/SanchezTapiaA) 

`r icons::icon_style(icons::fontawesome("github", style = "brands"), scale = 1, fill = "#A70000")``r icons::icon_style(icons::fontawesome("gitlab", style = "brands"), scale = 1, fill = "#A70000")`  [saramortara](http://github.com/saramortara) |  [andreasancheztapia](http://github.com/andreasancheztapia) 
