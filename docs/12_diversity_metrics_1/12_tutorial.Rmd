---
title: "Biological diversity in R"
author:
  - "Andrea Sánchez-Tapia"
  - "Sara Mortara"
date: "7/28/2022"
output:
  distill::distill_article:
    toc: true
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

```{r xaringanExtra-clipboard, echo=FALSE}
htmltools::tagList(
  xaringanExtra::use_clipboard(
    button_text = "Copy code <i class=\"fa fa-clipboard\"></i>",
    success_text = "Copied! <i class=\"fa fa-check\" style=\"color: #90BE6D\"></i>",
    error_text = "Not copied :/ <i class=\"fa fa-times-circle\" style=\"color: #F94144\"></i>"
  ),
  rmarkdown::html_dependency_font_awesome()
)
```

# Back to cestes data

```{r}
library(readr)
files_path <- list.files(path = "12_diversity_metrics_1/data/raw/cestes", 
                         pattern = ".csv", 
                         full.names = TRUE)
if (!interactive())
  files_path <- list.files(path = "data/raw/cestes",
                           pattern = ".csv",
                           full.names = TRUE)
files <- purrr::map(files_path, read_csv)
names(files)
com <- files[[1]]

```

# Riqueza de espécies


```{r}
library(vegan)
binary_com <- com
#binary_com > 0
binary_com[binary_com > 0] <- 1
binary_com
rowSums(binary_com)
binary2 <- decostand(com, "pa")
rowSums(binary2)
riqueza <- vegan::specnumber(com)
```

# Índices de diversidade

The community matrix of CESTES 

```{r}
dim(com)
shannon <- vegan::diversity(com)
simpson <- vegan::diversity(com, index = "simpson")
plot(shannon, simpson)
plot(shannon ~ riqueza)
plot(simpson ~ riqueza)
```

## Hill numbers


Common diversity indices are special cases of Rényi diversity

$$Ha = 1/(1-a) * log \sum(p^a)$$

### Functional diversity

Taxonomic diversity indices are based on the assumption that species belong to one species or the other. 

This can be thought as a distance matrix between individuals, individuals of the same species have a distance of zero between them, individuals of different species have a distance of 1 between them. 

When analyzing functional traits  <!-- we need a definition!--> the distance between individuals in the trait space varies in the 0-1 interval. 

These traits can be continuous, but vary in different scales, or they can be categorical, and appropriate distance measures have to be used to deal with this difference. Gower distance is a common distance metric used in trait-based ecology. 

Regarding phylogenies, the distance between individuals belonging to different species (taxa) can be expressed in million years or number of nodes. 

Phylogenies that are properly calibrated will improve these calculations. 


```{r}
head(traits)
dplyr::glimpse(traits)
summary(traits)
plot(traits$Anemo)
plot(traits$Entomo)
??gower
library(cluster)
gow <- daisy(traits[,-1], metric = "gower")
library(FD)
help(FD)

gow2 <- gowdis(traits[,-1])
identical(gow, gow2)
dbFD(traits[,-1], )
library(dplyr)
comm <- files[[1]]
traits <- files[[5]]
trait <- traits[,-1] %>% 
mutate(height = MaxHeight - MinHeight)  %>% 
  select(-MaxHeight, -MinHeight) %>% as.data.frame()
rownames(trait) <- traits$Sp
trait
spt <- t(trait)
gower <- ade4::disc(spt)
is(gower)
library(vegan)
??pcoa
gower
metaMDS(gower)
gow <- cluster::daisy(trait, metric = "gower")
dist(spt)
pcoa <- cmdscale(gow)
plot(pcoa)
text(pcoa, labels = traits$Sp)
clust <- hclust(gow, method = "ward.D2")
plot(clust, hang = -1, )
```

