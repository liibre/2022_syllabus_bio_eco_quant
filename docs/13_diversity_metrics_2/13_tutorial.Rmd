---
title: "From taxonomical to functional and phylogenetic diversity in R"
author:
  - "Andrea Sánchez-Tapia"
  - "Sara Mortara"
date: "8/04/2022"
output:
  distill::distill_article:
    toc: true
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

```{r xaringanExtra-clipboard, echo=FALSE}
htmltools::tagList(
  xaringanExtra::use_clipboard(
    button_text = "Copy code <i class=\"fa fa-clipboard\"></i>",
    success_text = "Copied! <i class=\"fa fa-check\" style=\"color: #90BE6D\"></i>",
    error_text = "Not copied :/ <i class=\"fa fa-times-circle\" style=\"color: #F94144\"></i>"
  ),
  rmarkdown::html_dependency_font_awesome()
)
```

# Back to cestes data

Remember our good practices: you do not save workspaces and every script is self-contained. 
In previous classes you read the CESTES database and obtained an object comm (for the community matrix). 

Execute this code at the top of your new script and bring the `trait` dataframe too. 

```{r read_cestes, echo=FALSE}
library(readr)
files_path <- list.files(path = "12_diversity_metrics_1/data/raw/cestes", 
                         pattern = ".csv", 
                         full.names = TRUE)
if (!interactive())
  files_path <- list.files(path = "data/raw/cestes",
                           pattern = ".csv",
                           full.names = TRUE)
files <- purrr::map(files_path, read_csv)
names(files) <- c("comm", "coord", "envir", "splist", "traits")
comm <- data.frame(files[[1]])
traits <- data.frame(files[[5]])
```

We are going to start at this stage: comm and traits exist and are data.frames: 

```{r}
head(comm)[,1:6]
head(traits)[,1:6]
```

You can add rownames instead of having columns:
```{r}
rownames(comm)[1:6]
rownames(comm) <- paste0("Site", comm[,1])
comm <- comm[,-1]
head(comm)[,1:6]
```

Transform column `traits$Sp` into the rownames of the dataframe. 

```{r, echo = F}
#rownames(traits)
#traits$Sp
rownames(traits) <- traits$Sp
traits <- traits[,-1]
#head(traits)[,1:6]
```

We want this output: 

```{r}
head(traits)[,1:6]
```



# Species richness

Species richness can be calculated with the `vegan` package:

```{r richness1, echo = F}
library(vegan)
binary_com <- comm
#binary_com > 0
binary_com[binary_com > 0] <- 1
#rowSums(binary_com)
binary2 <- decostand(comm, "pa")
#rowSums(binary2)
```


```{r richness}
library(vegan)
richness <- vegan::specnumber(comm)
```

# Taxonomic diversity

Taxonomic measures can be calculated using `diversity()` function:

```{r diversity}
shannon <- vegan::diversity(comm)
simpson <- vegan::diversity(comm, index = "simpson")
```



### Functional diversity

Taxonomic diversity indices are based on the assumption that species belong to one species or the other. 

This can be thought as a distance matrix between individuals, where individuals of the same species have a distance of zero between them, individuals of different species have a distance of 1 between them. 


When analyzing functional traits the distance between individuals is no longer determined by their belonging to a species, but to their position in the trait space. 

These traits can be continuous, but vary in different scales, or they can be categorical, and appropriate distance measures have to be used to deal with this difference. __Gower distance__ is a common distance metric used in trait-based ecology. 

```{r gower_distance}
library(cluster)
library(FD)
gow <- cluster::daisy(traits, metric = "gower")
gow2 <- FD::gowdis(traits)
#implementations in R vary and the literature reports extensions and modifications
identical(gow, gow2) #not the same but why?
class(gow) #different classes
class(gow2)
plot(gow, gow2, asp = 1) #same values
```


```{r, echo = F, eval = F}
#install.packages("SYNCSA")
library(SYNCSA)
data(ADRS)

rao.diversity(ADRS$community)
diversity(ADRS$community, "simpson")

rao.diversity(ADRS$community, traits = ADRS$traits)
rao.diversity(ADRS$community, traits = ADRS$traits, phylodist = )
```

# Rao's quadratic entropy calculations in R

## Using package SYNCSA 

```{r syncsa}
#install.packages(SYNCSA)
library(SYNCSA)
tax <- rao.diversity(comm)
fun <- rao.diversity(comm, traits = traits)
#plot(fun$Simpson,fun$FunRao, pch = 19, asp = 1)
#abline(a = 0, b = 1)
```

## Calculating FD indices with package `PD`

```{r FD, warning=FALSE, message=FALSE}
#install.packages("FD")
library(FD)
#we can use the distance matrix to calculate functional diversity indices
FuncDiv1 <- dbFD(x = gow, a = comm, messages = F)
#the returned object has Villéger's indices and Rao calculation
names(FuncDiv1)
#We can also do the calculation using the traits matrix directly
FuncDiv <- dbFD(x = traits, a = comm, messages = F)
```


## To calculate phylogenetic diversity

+ Have a megatree 
+ Adjust the branch lengths
+ 
```{r}
library(phytools)
library(picante)
library(vegan)
library(ape)
library(picante)
library(brranching)


# ####TODO ESTO EN EL DIRECTORIO DE PHYLOMATIC/PHYLOCOM - ABRIR CON TEXTWRANGLER
cd ~/OneDrive/Documentos/Caparaó/3\ results/output/phylogenetic\ diversity
#esto es con el nuevo r20120829 modificado y con los ages nuevos
./phylocom bladj -f r20120829mod.new > r20120829b.new

./phylomatic -f r20120829b.new -t taxaselected.txt > treefinal1.new
#no encontró algunas cosas pero f.u.

#ahora sí se deja limpiar ¬¬
./phylocom cleanphy -f treefinal1.new > treefinal2.new
#entonces limpio en R
#phy1 <- read.newick("./output/phylogenetic diversity/treefinal2.new")
#phy <- collapse.singles(phy1)
#plot(phy)
#phy2 <- drop.tip(phy = phy1,"_")
#write.tree(phy2,"./output/phylogenetic diversity/treefinal2.new")
#y me devuelvo a terminal
./phylocom bladj -t taxaselected.txt -f treefinal2.new > tree3.new
#funcionó
#leyendo el output de phylocom
phy0 <- read.newick("./output/phylogenetic diversity/tree3.new")
plot(phy0)
#get rid of singletons
phy <- collapse.singles(phy0)
plot(phy)
#check tree out
is.ultrametric(phy)
#should be true
is.rooted(phy)
#should be true
is.binary.tree(phy)
phy
#should be false
#write tree to file
write.tree(phy,"./output/phylogenetic diversity/treesel4.new")
tree4 <- read.newick( "./output/phylogenetic diversity/treesel4.new")
par(mfrow = c(1, 1), mar = c(2, 2, 1, 1))
plot(tree4, cex = 0.8)

##import plot data and phylogeny
source("./R/genera_BlocosPhylocom.R")
pruned <- prune.sample(Blocks.phylocom, tree4)
pruned$tip.label %in% names(Blocks.phylocom)
write.tree(pruned, "./output/phylogenetic diversity/finaltree.new")
Phydist <- cophenetic.phylo(pruned)
write.csv(Phydist,"./output/phylogenetic diversity/phydist.csv")

com.ord <- Blocks.phylocom[,pruned$tip.label]#ordena a comunidade na ordem na árvore


names(com.ord) %in% pruned$tip.label
setdiff(names(com.ord), pruned$tip.label)
setdiff(pruned$tip.label, names(com.ord))

PD <- pd(com.ord,pruned)
head(PD)
plot(PD$PD ~ PD$SR)

SES.PD <- ses.pd(samp = com.ord, tree = pruned, null.model = "taxa.labels", runs = 999)
head(SES.PD)
plot(SES.PD$pd.obs.z)
abline(h = c(-1.97, 1.97), col = "red")
plot(SES.PD$pd.obs.p)

mpd.test <- mpd(com.ord,cophenetic(pruned),abundance.weighted = T)
plot(mpd.test)
BR <- bloco.roseta.df(rownames(com.ord))
BR
plot(mpd.test ~ BR$roseta)
pd

plot(SES.MPD$mpd.obs.z)
head(SES.MPD)
plot(SES.MPD$mpd.obs.p)
dim(SES.MPD)
plot(SES.MPD$mpd.obs.p)

plot(pruned)
rownames(com.ord)
ses.mpd(samp = com.ord,mpd.test)

MELODIC.phylo <- melodic(samp = com.ord,dis = cophenetic(pruned),type = "both")
plot(MELODIC.phylo$presence$rao)
SES.MPD <- ses.mpd(samp = com.ord, null.model = "trialswap",
                   abundance.weighted = F, dis = Phydist, runs = 999)

## Next: How to we summarize visually, interpret community composition and trait data? 


```{r , echo = F, include = F}
library(vegan)
pcoa <- cmdscale(gow)
plot(pcoa)
text(pcoa)
```



```{r, echo = F, include = F}
library(vegan)
bray <- vegdist(comm, "bray")
clust <- hclust(bray, method = "ward.D2")
plot(clust, hang = -1, )
```


