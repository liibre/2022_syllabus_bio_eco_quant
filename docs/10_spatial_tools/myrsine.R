install.packages("usethis")
usethis::edit_r_environ()
#GBIF_USER = "andreast"
#GBIF_PWD = "gbI113129"
#GBIF_EMAIL = "andreasancheztapia@gmail.com"

install.packages("rgbif")
library(rgbif)
library(dplyr)
species <- "Myrsine coriacea"
key <- name_suggest(q = "Myrsine coriacea", rank = "species")
occ_search(taxonKey=key, limit=2)
occs <- rgbif::occ_search(scientificName = species)
names(occs)
str(occs)
glimpse(occs)
names(occs$data)
write_csv(occs$data, )

# get some occurrences using the search API
occ_search(scientificName = "Calopteryx splendens")
occ_search(scientificName = "Calopteryx splendens",country = "DK")
occ_search(scientificName = "Calopteryx splendens",country = "DK",year="1999,2005")

# or get just the data
occ_data(scientificName = "Calopteryx splendens")
occ_data(scientificName = "Calopteryx splendens",country = "DK")
occ_data(scientificName = "Calopteryx splendens",country = "DK",year="1999,2005")

# look up a single occurrence record
occ_get(key=855998194) # not many reasons to do this but it exists
