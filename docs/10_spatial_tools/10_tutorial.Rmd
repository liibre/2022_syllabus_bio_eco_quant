---
title: "Introduction to spatial data in R"
author:
  - "Andrea Sánchez-Tapia"
  - "Sara Mortara"
date: "7/26/2022"
output:
  distill::distill_article:
    toc: true
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

```{r xaringanExtra-clipboard, echo=FALSE}
htmltools::tagList(
  xaringanExtra::use_clipboard(
    button_text = "Copy code <i class=\"fa fa-clipboard\"></i>",
    success_text = "Copied! <i class=\"fa fa-check\" style=\"color: #90BE6D\"></i>",
    error_text = "Not copied :/ <i class=\"fa fa-times-circle\" style=\"color: #F94144\"></i>"
  ),
  rmarkdown::html_dependency_font_awesome()
)
```

## Examining an `sf` object

`tmap` package is one of the current strongest packages to build thematic maps in R. It has a syntax similar to ggplot and works with `sf` objects.

There are several sources of administrative boundaries data.
We are going to load `World`, an internal dataset in 
contained in package `tmap`. It is an sf object. 

The quickest map in `tmap` can be done like this:

```{r}
library(sf)
library(tmap)
library(dplyr)
data(World)
# package tmap has a syntax similar to ggplot. The functions start all with tm_ 
tm_shape(World) + 
  tm_borders()
```








A simple examination with `head()`, `str()` or `dplyr::glimpse()` can tell us the class, dimensions, and other characteristics of object World. 

```{r}
#| eval: false
#head(World)
names(World)
class(World)
dplyr::glimpse(World)
```

What happens when you execute plot(World)? How would you plot only one of the variables? What happens when you execute 

```{r}
plot(World[1])
plot(World[,1])
plot(World[1,])
plot(World["pop_est"])
```

### The geometry "column" and geometries as objects

A key difference between data frames and sf objects is the presence of `geometry`, that looks like a column when printing the summary of any sf object

```{r}
head(World[, 1:4])
```

When calling `World$geometry`, however, we perceive it's not a single column. Actually, geometries are a class in their own, `sfc` 

```{r}
class(World)
class(World$geometry)
```

We don't need to understand the deep structure of geometry right now, the `sf` package has extensive vignettes you can explore. 
For practical purposes, the existence of geometries and sf objects like data.frames facilitates a lot the manipulation of `sf` objects, including extracting and assigning geometries to existant data frames and the possibility to drop the geometries, (literally, function `sf::st_drop_geometry()` and use the data frame)

```{r}
head(sf::st_coordinates(World))
no_geom <- sf::st_drop_geometry(World)
class(no_geom)
#bounding boxes
st_bbox(World)
```




### Manipulating sf objects 

For practical purposes, sf objects work like data.frames or tibbles, you can subset, filter, merge, join, and create new variables. 

For example, imagine I want to plot only countries in South America or have different colors for them

You can filter depending on the columns, for example, all the countries in South America: 

```{r}
names(World)
unique(World$continent)
World %>% 
  filter(continent == "South America") %>% 
  tm_shape() + 
  tm_borders()
```

You can also create new variables and use them in your maps: 

```{r}
World %>%
  mutate(our_countries = if_else(iso_a3 %in% c("COL","BRA", "MEX"), "red", "grey")) %>%
  tm_shape() +
  tm_borders() +
  tm_fill(col = "our_countries") +
  tm_add_legend("fill",
                "Countries",
                col = "red")
```



### A quick note about `sp` objects 


`sp` objects are going to be replaced with `sf` objects and packages `rgdal`, `rgeos` will be archived by the end of 2023. _However_, it is extremely possible that the data and tutorials you find use sp objects rather than sf and that you may have to go transform them into sf objects (function `sf::st_as_sf()`. 

Data






## Loading, ploting, and saving a shapefile from the disk


```{r}
#install.packages("rnaturalearth")
#install.packages("remotes")
#remotes::install_github("ropensci/rnaturalearthhires")
library(rnaturalearth)
library(rnaturalearthhires)
bra <- ne_states(country = "brazil", returnclass = "sf")
plot(bra)
dir.create("data/shapefiles", recursive = TRUE)
st_write(obj = bra, dsn = "data/shapefiles/bra.shp", delete_layer = TRUE)
```

Check the files that are created:
  .shp, .shx, .dbf, .cpg, prj.

To read again this shapefile, you would execute: 

```{r}
bra2 <- read_sf("data/shapefiles/bra.shp")
class(bra)
class(bra2)
plot(bra)
plot(bra2)
```




## Loading, ploting, and saving a raster from the disk

```{r}
library(raster)
dir.create(path = "data/raster/", recursive = TRUE)
tmax_data <- getData(name = "worldclim", var = "tmax", res = 10, path = "data/raster/")
plot(tmax_data)
is(tmax_data) #the data are a raster stack, several rasters piled 
dim(tmax_data)
extent(tmax_data)
res(tmax_data)
```

## To move forward

This is really the tip of the iceberg. 
Choose your own adventure: 

+ Check the vignettes in `sf`
+ Check the tutorials in `rspatialdata`
+ Check the multiple CRAN TaskViews regarding spatial data



---
## Palettes

+ Palettes in map are key to convey the information. 

+ Gradual, divergent or categorical

+ Color-blind safe!

+ Options: https://colorbrewer2.org, with a helper package in R, `RColorBrewer`

```r
#| eval: false
library(RColorBrewer)
display.brewer.all(type = "seq")
display.brewer.all(type = "div")

```
