---
title: "Overleaf workflow"
author: "Andrea Sánchez-Tapia"
date: "6/7/2022"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

## Introduction

Collaboration for writing scientific and technical documents can be difficult if coauthors come from different backgrounds, that use different writing tools.

Some teams rotate and edit one document and rely on comments and revision tools, others prefer using online editors and finish polishing the format offline.

Some of these workflows are not very reproducible. 

In physics, statistics, and mathematics, the use of literate programming tools such as $\LaTeX$ is commonplace, but this is not the case in the biological and social sciences. 

