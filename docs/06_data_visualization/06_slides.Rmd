---
title: "Graphics with R"
author: "Sara Mortara & Andrea Sánchez-Tapia"
institute: "re.green | ¡liibre!"
date: "2022-07-13"
output:
  xaringan::moon_reader:
    lib_dir: libs
    css: xaringan-themer.css
    nature:
      highlightStyle: github
      highlightLines: true
      countIncrementalSlides: false
      ratio: '16:9'
self-contained: true
---


```{css css-config, echo = FALSE}
.tiny .remark-code { /*Change made here*/
    font-size: 50% !important;
}
```

```{r setup, include=FALSE, message=FALSE}
options(htmltools.dir.version = FALSE)
knitr::opts_chunk$set(echo = FALSE, eval = TRUE, fig.show = 'asis', 
                      fig.align = 'center', out.width = 400)
```

```{r xaringanthemer, include = FALSE}
library(xaringanthemer)
xaringanthemer::style_mono_accent(
  base_color = "#A70000",
  #  primary_color = "#A70000",
  #secondary_color = "#ff0000",
  white_color = "white",
  colors = c(
    red = "#A70000",
    purple = "#88398a",
    orange = "#ff8811",
    green = "#136f63",
    blue = "#4B4FFF",
    white = "#FFFFFF",
    black = "#181818"
  ),
  header_font_google = google_font("Roboto Condensed"),
  text_font_google   = google_font("Roboto Condensed", "300", "300i"),
  code_font_google   = google_font("Ubuntu Mono"),
  text_font_size = "30px",
  code_font_size = "30px",
)
xaringanExtra::use_share_again()

# clipboard
htmltools::tagList(
  xaringanExtra::use_clipboard(
    button_text = "Copy code <i class=\"fa fa-clipboard\"></i>",
    success_text = "Copied! <i class=\"fa fa-check\" style=\"color: #90BE6D\"></i>",
    error_text = "Not copied 😕 <i class=\"fa fa-times-circle\" style=\"color: #F94144\"></i>"
  ),
  rmarkdown::html_dependency_font_awesome()
)

```


## today

- graphics' good practices

- graphics grammar

- examples with `ggplot2` 


---
class: middle
## data visualization has many don'ts

.pull-left[
```{r, echo = F}
library(lattice)
library(latticeExtra)
d <- read.table(text = ' x   y     z
t1   5   high
t1   2   low
t1   4   med
t2   8   high
t2   1   low
t2   3   med
t3  50   high
t3  12   med
t3  35   low', header = TRUE)
d$x <- factor(d$x)
d$z <- factor(d$z)
levels(d$z) <- c("high", "med", "low")
cloud(y ~ x + z, d,
      panel.3d.cloud = panel.3dbars,
      col.facet = d$z,
      xbase = 0.4,
      ybase = 0.4,
      scales = list(arrows = FALSE, col = 1),
      par.settings = list(axis.line = list(col = "transparent")),
      main = "my 3D plot")
```
]

--

.pull-right[
```{r, echo = F, message=F, warning=F, out.width=400}
library(ggplot2)
pal <- wesanderson::wes_palette(3, name = "Zissou1", type = "continuous")
p <- ggplot(data=d, aes(x=x, y=y, fill=z)) +
  geom_bar(stat="identity") +
  scale_fill_manual(values=pal[3:1])+
  theme_classic() 
p
```
]

---

## many

.pull-left[
```{r pie3d, echo=FALSE}
library(plotrix)
slices <- c(10, 12, 4, 16, 8) 
lbls <- c("US", "UK", "Australia", "Germany", "France")
pct <- round(slices / sum(slices) * 100)
lbls <- paste(lbls, pct) # add percents to labels 
lbls <- paste(lbls, "%", sep = "") # ad % to labels 
pie3D(slices,labels = lbls, explode = 0.1,
      main = "Pie Chart of Countries ")
```
]

--

.pull-right[

```{r, echo = F}
pie(slices,labels = lbls, col=rainbow(length(lbls)),
    main="Pie Chart of Countries")
```

]

---
## there's always a better option than a pie chart

.pull-left[

```{r barplot, echo=FALSE}
nomes <- c("US", "UK", "Australia", "Germany", "France")
names(pct) <- nomes
perc <- paste0(pct, "%")
par(mai=c(1,1,1,1))
barplot(pct, horiz=TRUE, las=1, xlim=c(0, max(pct)+3))
text(x=pct+1.2,
     y=c(0.7, 1.9, 3.1, 4.3, 5.5),
     labels=perc)
```

]

--

.pull-right[

```{r barplot2, echo=FALSE}
nomes <- c("US", "UK", "Australia", "Germany", "France")
perc <- paste0(sort(pct), "%")
names(pct) <- nomes
par(mai=c(1,1,1,1))
barplot(sort(pct), horiz=TRUE, las=1, xlim=c(0, max(pct)+3))
text(x=sort(pct)+1.2,
     y=c(0.7, 1.9, 3.1, 4.3, 5.5),
     labels=perc)
```

]

---
## barplots are not always very informative

```{r, echo = F, fig.align='center'}
set.seed(42)
d2 <- data.frame(name = letters[1:5],
                 value = sample(seq(4, 15), 5),
                 sd = c(1, 0.2, 3, 2, 4))
barplot(value ~ name, data = d2, las = 1, bty = 'l')
```

---
## better with error bars...

```{r, echo = F, fig.align='center'}
barplot(value ~ name, data = d2, las = 1,
        bty = 'l', ylim = c(0, 18))
arrows(x0 = c(0.66, 1.88, 3.07, 4.25, 5.48),
       y0 = d2$value + d2$sd,
       y1 = d2$value - d2$sd, angle = 90, length = 0.1, code = 3)
```

---
## but maybe don't even make a barplot

```{r, echo = F, fig.align='center'}
plot(x = 1:5, d2$value, las = 1, bty = 'l', ylim = c(0, 18),
     pch = 19, xaxt = 'n',
     xlab = "names", ylab = "value")
axis(1, at = 1:5, labels = d2$name)
arrows(x0 = 1:5,
       y0 = d2$value + d2$sd,
       y1 = d2$value - d2$sd,
       angle = 90, length = 0.05, code = 3)
```

---
##  ...or maybe don't even make a graph


```{r, echo = F, fig.align='center'}
d3 <- data.frame(media = c(6.7, 12.3),
                 sd = c(0.4, 0.98),
                 tratamento = c(1, 2))
plot(media ~ c(1,2), data = d3, las = 1, bty = 'l',
     xlab = "tratamento", pch = 19, xaxt = 'n',
     ylim = c(2, 15))
axis(1, at = 1:2, labels = c("1", "2"))
arrows(x0 = 1:2,
       y0 = d3$media + d3$sd,
       y1 = d3$media - d3$sd,
       angle = 90, length = 0.05, code = 3)
```

---
## make a table 

|Treatment| Effect|
|-----------|--------|
|1 |`r d3$media[1]` $\pm$ `r d3$sd[1]` | 
|2 |`r d3$media[2]` $\pm$ `r d3$sd[2]` | 

---
## or say it in the text 

<center>
The effect of treatment 2 (12.3 ± 0.98) was
higher than treatment 1 (6.7 ± 0.4).

---
## do not fool your reader


```{r, out.width = 600}
knitr::include_graphics("figs/0176-fox-original.jfif.jpeg")
```

[Free range statistics](http://freerangestats.info/blog/2020/04/06/crazy-fox-y-axis)


---
## do not fool your reader

```{r, out.width = 600}
knitr::include_graphics("figs/fox_corrected.png")
```

[Free range statistics](http://freerangestats.info/blog/2020/04/06/crazy-fox-y-axis)


---
## some basic tips in general

+ only make plots when you really need to

--

+ don't spend more ink and colors than you need to

--

+ don't fool your reader (no y-axis tampering, no undue transformation)

--

+ show error measures


---
## some basic tips in R


+ make your y-axis labels horizontal and visible

--

+ avoid colored backgrounds

--

+ use dots for scatterplots

--

+ add names to `xlab` and `ylab`

--

+ save to png and pdf formats


---

## the grammar of graphics

.pull-left[
- a theoretical deconstruction for data graphics

- foundation for software (ggplot2, [vega-lite](https://vega.github.io/vega-lite/))

- how to design the system that allow building beautiful, correct graphics
]

.pull-right[

```{r, out.width=300, echo=FALSE}
knitr::include_graphics("figs/grammar_of_graphics.jpg")
```

]


---
background-image: url("figs/logo.png")
background-position: 98% 2%
background-size: 100px

## the grammar of graphics: ggplot2


Hadley Wickham, Danielle Navarro, and Thomas Lin Pedersen

.pull-left[

- [https://ggplot2-book.org/](https://ggplot2-book.org/) 

- not a cookbook ([R Graphics Cookbook, Winston Chang](https://r-graphics.org/))

- underlying theory

- power to tailor any plot

]


.pull-right[
```{r, out.width=200}
knitr::include_graphics("figs/book.jpg")
```

]

---

## graphics


Type of graphic | R base | 
----------|------------|
barplot | barplot() | 
histogram | histogram() | 
density | plot(density()) | 
quantile-quantile |	qqnorm()	| 
boxplot | boxplot() | 
scatterplot | plot() | 


---
## grammar: decompose graphics into its constituints

- theme
- coordinates
- facets
- geometries
- scales
- statistics
- mapping
- data
    - it will require data in a variable/colum observation/row format



---
class: inverse, middle, center 

background-image: url("figs/logo.png")
background-position: 98% 2%
background-size: 100px/

## ggplot2


---

## barplot

- ToothGrowth data sets
- the effect of Vitamin C on Tooth growth in Guinea pigs
- len: Tooth length
- dose: Dose in milligrams (0.5, 1, 2)


.tiny[

```{r, echo = TRUE}

library(ggplot2)

df <- data.frame(dose = c("D0.5", "D1", "D2"),
                 len = c(4.2, 10, 29.5))
df
```


]


---
## create a barplot



.tiny[


```{r, echo = TRUE}
bar1 <- ggplot(data = df, mapping = aes(x = dose, y = len)) +
  geom_bar(stat = "identity")
bar1
```

]


---
## create a barplot



.tiny[


```{r, echo = TRUE}
# Data and mapping can be given both as global (in ggplot()) or per layer
bar1 <- ggplot() +
  geom_bar(data = df, mapping = aes(x = dose, y = len), 
           stat = "identity")
bar1
```

]


---
## create a horizontal barplot


```{r, echo = TRUE}
bar1 + coord_flip()
```



---

## change the bar color

```{r, echo = TRUE}
ggplot(data = df, mapping = aes(x = dose, y = len)) +
  geom_bar(stat = "identity", fill = "red")

```


---

## change the bar color using the hexcode

```{r, echo = TRUE}
ggplot(data = df, mapping = aes(x = dose, y = len)) +
  geom_bar(stat = "identity", fill = "#A70000")

```


---
## changing the theme


.tiny[

```{r, echo = TRUE}
ggplot(data = df, mapping = aes(x = dose, y = len)) +
  geom_bar(stat = "identity", fill = "#A70000") + 
  theme_classic()

```

]



---
## defining a parameters for a theme

```{r, echo=TRUE}
title_size <- 18
text_size <- 16
my_theme <- theme_classic() +
  theme(axis.title.x = element_text(size = title_size),
        axis.text.x = element_text(size = text_size),
        axis.title.y = element_text(size = title_size),
        axis.text.y = element_text(size = text_size))
```


---
## changing labels and size


.tiny[

```{r, echo=TRUE}
ggplot(data = df, mapping = aes(x = dose, y = len)) +
  geom_bar(stat = "identity", fill = "#A70000") + 
  labs(x = "Dose", y = "Tooth length (cm)") +
  my_theme
```

]


---
## do we need color?


.tiny[

```{r, echo=TRUE}
ggplot(data = df, aes(x = dose, y = len, fill = dose)) +
  geom_bar(stat = "identity") + 
  labs(x = "Dose", y = "Tooth length (cm)") +
  scale_fill_manual(values = c("#E69F00", "#56B4E9", "#CC79A7")) +
  my_theme
```

]

---
## barplot with subcategories

Adding with each of two delivery methods: 
- orange juice (OJ)
- ascorbic acid (VC)

.tiny[
```{r, echo=TRUE}
df2 <- data.frame(supp = rep(c("VC", "OJ"), each = 3),
                  dose = rep(c("D0.5", "D1", "D2"), 2),
                  len = c(6.8, 15, 33, 4.2, 10, 29.5))
head(df2)

```
]

---
## barplot with subcategories

.tiny[
```{r, echo=TRUE}
ggplot(data = df2, aes(x = dose, y = len, fill = supp)) +
  geom_bar(stat = "identity") +
  scale_fill_brewer(palette = "Paired") +
  my_theme
```

]

---
## scatterplot


.tiny[

```{r, echo=TRUE}
data("faithful")
# Basic scatterplot
ggplot(data = faithful, 
       mapping = aes(x = eruptions, y = waiting)) + 
  geom_point() + my_theme
```
]

---

## scatterplot


.tiny[

```{r, echo=TRUE}
# Data and mapping can be given both as global (in ggplot()) or per layer
ggplot() + 
  geom_point(mapping = aes(x = eruptions, y = waiting),
             data = faithful) + my_theme
```
]

---

## aesthetic linked to data aes()

.tiny[

```{r, echo=TRUE}
ggplot(faithful) + 
  geom_point(aes(x = eruptions, y = waiting, colour = eruptions < 3)) +
  my_theme
```
]


---
## set color to a value outside `aes()`

.tiny[
```{r, echo=TRUE}
ggplot(faithful) + 
  geom_point(aes(x = eruptions, y = waiting),
             colour = 'steelblue') + 
  my_theme
```
]

---
## histogram


.tiny[

```{r, echo=TRUE}
ggplot(faithful) + 
  geom_histogram(aes(x = eruptions), bins = 40) +
  my_theme
```
]


---
## scales

.tiny[

```{r, echo = TRUE}
ggplot(mpg) + 
  geom_point(aes(x = displ, y = hwy)) + 
  scale_x_continuous(breaks = c(3, 5, 6)) + 
  scale_y_continuous(trans = 'log10') +
  my_theme
```

]

---
## facets


.tiny[

```{r, echo = TRUE}
ggplot(mpg) + 
  geom_point(aes(x = displ, y = hwy)) + 
  facet_wrap(~ class) + my_theme
```

]

---
## facets

.tiny[

```{r, echo = TRUE}
ggplot(mpg) + 
  geom_point(aes(x = displ, y = hwy)) + 
  facet_grid(year ~ drv) + my_theme
```

]


---

### plot composition

.tiny[

```{r, echo=TRUE}
msleep <- na.omit(msleep)
p1 <- ggplot(msleep) + 
  geom_boxplot(aes(x = sleep_total, y = vore, fill = vore)) +
  my_theme
p1 

```
]

---
## plot composition

.tiny[

```{r, echo=TRUE}
p2 <- ggplot(msleep) + 
  geom_bar(aes(y = vore, fill = vore)) +
  my_theme
p2
```
]

---
## plot composition

.tiny[

```{r, echo=TRUE}
p3 <- ggplot(msleep) + 
  geom_point(aes(x = bodywt, y = sleep_total, colour = vore)) + 
  scale_x_log10() +
  my_theme
p3
```

]

---
## patchwork


```{r, echo=T}
library(patchwork)
p1 + p2 + p3
```

---
## patchwork

.tiny[
```{r, echo=T}
(p1 | p2) / p3 + plot_layout(guides = 'collect') + 
  plot_annotation(title = 'Mammalian sleep patterns',
                  tag_levels = 'A')
```
]


---
## density ridges

.tiny[

```{r, echo=TRUE, message=FALSE}
library(ggridges)
df_diamonds <- diamonds[1:100, c("color", "depth")]
ggplot(df_diamonds, aes(x = depth, y = color)) +
  geom_density_ridges() + my_theme
```

]


---
## other resources


.pull-left[
- color palletes:
  - [Color Brewer](http://colorbrewer2.org/)
  - [Adobe Color ](https://color.adobe.com/create)
  - [COLOR lovers](https://www.colourlovers.com/)
  
- icons for graphics:
  - [The noun project](https://thenounproject.com/)
  - [iconmonst](https://iconmonstr.com/)
]


.pull-right[
- R packges:
  - `wesanderson`
  - `RColorBrewer`
  - `swatches`
  - `colourlovers`
]


---
class: inverse, middle, center

# aRt

---

```{r, fig.align='center', out.width=1000}
knitr::include_graphics("figs/dani.png")
```

useR!2021


---

```{r, fig.align='center', out.width=1000}
knitr::include_graphics("figs/antonio_sanchez.png")
```

useR!2021

---
## todo


`r icons::fontawesome("laptop-code")`

- create and run `Rmd` file `graphics.Rmd` 


- `git add`, `commit`, and `push` of the day


---
class: center, middle

# ¡Thanks!

<center>
`r icons::icon_style(icons::fontawesome("paper-plane", style = "solid"), scale = 1, fill = "#A70000")` [saramortara@gmail.com](mailto:saramortara@gmail.com) | [andreasancheztapia@gmail.com](mailto:andreasancheztapia@gmail.com) 


`r icons::icon_style(icons::fontawesome("twitter", style = "brands"), scale = 1, fill = "#A70000")` [@MortaraSara](https://twitter.com/MortaraSara) | [@SanchezTapiaA](https://twitter.com/SanchezTapiaA) 

`r icons::icon_style(icons::fontawesome("github", style = "brands"), scale = 1, fill = "#A70000")``r icons::icon_style(icons::fontawesome("gitlab", style = "brands"), scale = 1, fill = "#A70000")`  [saramortara](http://github.com/saramortara) |  [andreasancheztapia](http://github.com/andreasancheztapia) 


