---
title: "Introduction to R"
author: "Sara Mortara & Andrea Sánchez-Tapia"
institute: "re.green | ¡liibre!"
date: "2022-07-05"
output:
  xaringan::moon_reader:
    lib_dir: libs
    css: xaringan-themer.css
    nature:
      highlightStyle: github
      highlightLines: true
      countIncrementalSlides: false
      ratio: '16:9'
self-contained: true
---

```{css css-config, echo = FALSE}
.tiny .remark-code { /*Change made here*/
  font-size: 50% !important;
}
```

```{r setup, include=FALSE}
options(htmltools.dir.version = FALSE)
```

```{r xaringanthemer, include = FALSE}
library(xaringanthemer)
xaringanthemer::style_mono_accent(
  base_color = "#A70000",
  #  primary_color = "#A70000",
  #secondary_color = "#ff0000",
  white_color = "white",
  colors = c(
    red = "#A70000",
    purple = "#88398a",
    orange = "#ff8811",
    green = "#136f63",
    blue = "#4B4FFF",
    white = "#FFFFFF",
    black = "#181818"
  ),
  header_font_google = google_font("Roboto Condensed"),
  text_font_google   = google_font("Roboto Condensed", "300", "300i"),
  code_font_google   = google_font("Ubuntu Mono"),
  text_font_size = "30px",
  code_font_size = "30px",
  )
xaringanExtra::use_share_again()

# clipboard
htmltools::tagList(
  xaringanExtra::use_clipboard(
    button_text = "Copy code <i class=\"fa fa-clipboard\"></i>",
    success_text = "Copied! <i class=\"fa fa-check\" style=\"color: #90BE6D\"></i>",
    error_text = "Not copied 😕 <i class=\"fa fa-times-circle\" style=\"color: #F94144\"></i>"
  ),
  rmarkdown::html_dependency_font_awesome()
)

```


## welcome | bienvenidas | bem-vindas


<center>
.pull-left[
```{r, out.width= 400, echo=FALSE}
knitr::include_graphics("figs/andrea.jpeg")
```

Andrea Sánchez-Tapia (she/her)

]

</center>

<center>
.pull-right[
```{r, out.width= 300, echo=FALSE}
knitr::include_graphics("figs/sara.jpg")
```

Sara Mortara (she/her)
]

</center>

---
class: middle
background-image: url("figs/logo_jbrj.png")
background-position: 98% 2%
background-size: 100px


### Graduate course _Data analysis' projects using R_

.pull-left[

```{r, out.width= 400, echo=FALSE}
knitr::include_graphics("./figs/turma.JPG")
```


```{r, out.width= 150, fig.align = "center", echo=FALSE}
knitr::include_graphics("./figs/chevron.png")
#knitr::include_graphics("./figs/liibre.png")
```

]

.pull-right[
__Good practices__ in data analysis

```{r, out.width=200, fig.align='center', echo=FALSE}
knitr::include_graphics("./figs/rstudio.jpg")
```

```{r, out.width= 150, echo=FALSE}
knitr::include_graphics("./figs/logo-git.png")
knitr::include_graphics("./figs/GitHub_Logo.png")
knitr::include_graphics("./figs/btibucket.png")
knitr::include_graphics("./figs/gitlab-logo-gray-rgb.png")
```

```{r, out.width= 100, echo=FALSE}
knitr::include_graphics("./figs/latex.jpeg")
knitr::include_graphics("./figs/bibtex.jpeg")
knitr::include_graphics("./figs/zotero.svg")
```
```{r, out.width= 50, echo=FALSE}
knitr::include_graphics("./figs/rmarkdown.png")
```

```{r, out.width= 60, fig.align='center', eval=FALSE, echo=FALSE}
knitr::include_graphics("./figs/xaringan.png")
```
]


---

## overview of the course

<center>

```{r, echo=FALSE}
DiagrammeR::grViz("
digraph nicegraph {

  # graph, node, and edge definitions
  graph [compound = true, nodesep = .5, ranksep = .25,
         color = crimson]

  node [fontname = Helvetica, fontcolor = darkslategray,
        shape = rectangle, fixedsize = false, 
        color = darkslategray, fontsize = 30]

  edge [color = grey, arrowhead = none, arrowtail = none]

  # subgraph for basic tools
  subgraph cluster0 {
    node [fixedsize = true, width = 4.3, fontsize = 24]
    '@@1-1' -> '@@1-2' -> '@@1-3' -> '@@1-4' -> '@@1-5' -> '@@1-6' 
  }

  # subgraph for statistical modelling
  subgraph cluster1 {
    node [fixedsize = true, width = 4.3, fontsize = 24]
    '@@2-1' -> '@@2-2' 
  }
  
    # subgraph bio-eco-tools
  subgraph cluster2 {
    node [fixedsize = true, width = 4.3, fontsize = 24]
    '@@3-1' -> '@@3-2' -> '@@3-3' -> '@@3-4' -> '@@3-5' -> '@@3-6' 
  }

    # subgraph for open
  subgraph cluster3 {
    node [fixedsize = true, width = 4.3, fontsize = 24]
    '@@4-1' -> '@@4-2' 
  }

  'Scientific programming'             [width = 5, fontsize = 30, height = 0.75, style = filled, fillcolor = '#A70000', fontcolor = white]
  'Basic tools' [fontsize = 24, style = filled, fillcolor = dimgray, fontcolor = white, width = 4.4, height = 0.6]
  'Statistical modelling' [fontsize = 24, style = filled, fillcolor = dimgray, fontcolor = white, width = 4.4, height = 0.6]
  'Biology and ecology toolkit' [fontsize = 24, style = filled, fillcolor = dimgray, fontcolor = white, width = 4.5, height = 0.6]
  'Open science' [fontsize = 24, style = filled, fillcolor = dimgray, fontcolor = white, width = 4.4, height = 0.6]
  'Scientific programming' -> 'Basic tools' 
  'Scientific programming' -> 'Statistical modelling'
  'Scientific programming' ->   'Biology and ecology toolkit' 
  'Scientific programming' ->   'Open science'
  'Basic tools' -> '@@1-1'            [lhead = cluster0]
  'Statistical modelling' -> '@@2-1'        [lhead = cluster1]
  'Biology and ecology toolkit' -> '@@3-1'        [lhead = cluster2]
  'Open science' -> '@@4-1'        [lhead = cluster3]

}

[1]: c('R', 'git', 'workflows', 'exploratory data analysis', 'data manipulation', 'data visualization')
[2]: c('linear models', 'model evaluation')
[3]: c('two species dynamics', 'spatial R', 'biodiversity databases', 'diversity metrics', 'multivariate analysis', 'predictive modelling')
[4]: c('reproducible reports', 'methods and community')

", width = 1280*0.8)
```

</center>

---
# today

- overview of R and RStudio


- run a script from a specific location (pwd)


- read an external file in R

---

## why learn `R`?

+ the language of choice for academic statisticians

--

+ **_libre_ software**: free and free-to-be-used-and-modified for any means -> one of the pillars of open science

--

+ __script-based__: reproducibility, easier to scale up your analyses, transparency (track errors), great way to learn about methods

--

+ __interdisciplinary and modular__: lots of code written by area specialists. At the core of its philosophy is a smooth transition from user to programmer

--

+ __communication__ with other tools: manuscripts, presentations, apps and dashboards


---
## why learn `R`?

+ communication with __other programming languages__ (ex. __reticulate__ to run python scripts) 

--

+ great __graphic capabilities__!

--


+ __official support__: help in documentation, mailing lists

--

+ __an active and welcoming community__:  email lists, Stack Overflow, [RStudio community](community.rstudio.com/), useR groups, .purple[R-Ladies+] chapters, Slack communities, `r icons::fontawesome("twitter")` `#rstats`

<center>
```{r remedy001, out.width=150, echo = FALSE}
knitr::include_graphics("https://raw.githubusercontent.com/rladies/branding-materials/main/stickers/rainbow-inclusive.png")
```

---
## `R` has a modular structure: __packages__


+ `R` __base__ installation includes base packages developed and maintained by the __`R` Core Development Team__

--

+ other packages are created by the community and hosted in __CRAN__ (The Comprehensive `R` Archive Network) or Bioconductor, GitHub, rOpenSci.org

--

+ to install packages from CRAN: `install.packages("tidyverse")`

---
background-image:url("figs/00_rstudio.png")
background-size: 55%
# Running R in RStudio


`r icons::fontawesome("laptop-code")`

---
class: inverse, middle, center
# Setup and project organization

---
## working directory

+ you have to tell R where you will be working, so that it understands where to read tables, where to save outputs etc: __working directory__

--

+ `getwd()` in the console

--

+ the default is __"home"__: check general options and the "File" tab

--

+ you can tell R and RStudio where you want to work with `setwd()`

--

+ even better: instead of opening RStudio open an __R script__ or a __RStudio project__ (just as you would in MS Word   `r icons::fontawesome("file-word")`)

---
## project organization and best practices

+ projects are better organized if we use __one folder per project__ and  __subfolders__ within our working directory

--

+ we shouldn't modify __raw data files__ but __save processed data__ (and the corresponding scripts)

--

+ instead of __absolute paths__ we should use __relative paths__:
  + `.` "here"
  + `./figs` a subfolder called `figs`
  + the upper level `..`
  
--

+ avoid `C:users/your_name/your_file_structure/your_working_directory`

---
## In this and the following sessions

```

project/
*    ├── data/
*    │   ├── raw
*    │   └── processed
     ├── docs/
*    ├── figs/
     ├── R/
*    ├── output/
*    └── README.md
```



---
## RStudio projects 

RStudio projects create a __.Rproj__ file in your folder that acts as a shortcut for your projects


<right>
`r icons::fontawesome("laptop-code")`
</right>

+ unzip the .zip file into a folder of your preference


+ open the .Rproj file and run `getwd()`




```{r gif, eval=FALSE, out.width=300, echo = FALSE, fig.align='center'}
knitr::include_graphics("https://media.giphy.com/media/3oriO04qxVReM5rJEA/giphy.gif")
```


---
class:middle, inverse, center
# introduction to R 

---
## introduction to R 

+ `<-` is the assignment operation in R and it does not return output it __creates objects__ that are saved in the workspace
(`Alt + -`)

--

+ overwriting objects __does not affect other objects__ 

--

+  __naming object tips__: 
 + don't begin with a number or symbol.
 + there are forbidden words 
 + be consistent with your __coding style__! 
 + avoid dots
 + __name functions as verbs and objects as nouns__

--

+ you can see which objects are saved in the workspace by using `ls()`

---

## about the workspace

+ R creates __objects__ that occupy RAM memory: the __workspace__

--

+ the __workspace__ can be saved and loaded between sessions BUT

--

+ __you can lose track of how you created the objects in the workspace__

--

+ `#goodpractices` don't save the workspace

---
background-image:url("https://raw.githubusercontent.com/AndreaSanchezTapia/UCMerced_R/main/slides/figs/0a_setup.png")
background-size: 60%
background-position: 100% 100%

.pull-left[in the general options]

---
class: inverse, middle, center
# functions, arguments and understanding the  help

---
## functions and arguments

```
weight_kg <- sqrt(10)
round(3.14159)
args(round)
```

---
## if you know the name of the function
```
help(function)
?function
args(function)
```

+ select the name of the function and click __F1__

Check the structure of the help file:

+ Description
+ Usage
+ Arguments
+ Details

---
## if you don't know the name of the function

```
??kruskal
```

(or search it - google it - duckduckgo it)

---
## the structure of a function help

`args(function)`

+ The arguments of a function are coded:
  + in order
  + with or without default settings
  
--

You can either
+ use the arguments in order, without naming them
+ use the first arguments without naming them and then some optional arguments, with name


---
## data types in R

<small>
```{r datatypes}
animals  <- c("mouse", "rat", "dog")
weight_g <- c(50, 60, 65, 82)
```

--

```{r remedy003}
class(animals)
```

--

```{r class}
class(weight_g)
```
</small>

--

`character` and `numeric` but also `logical` and `integer` ("whole" numbers, with no decimal component, in N), `complex`, and others. 

---
class: center, middle

`r icons::icon_style(icons::fontawesome("laptop-code"), scale = 3)`

---
## subsetting vectors

+ R is __1-indexed__ and intervals are closed (not half-open)

```{r, results = 'show'}
animals <- c("mouse", "rat", "dog", "cat")
animals[2]
```

+ Subsetting is done with brackets `[]`

```{r subset}
animals[c(3, 2)]
```

---
## conditional subsetting

```{r, results = 'show'}
weight_g <- c(21, 34, 39, 54, 55)
weight_g[c(TRUE, FALSE, FALSE, TRUE, TRUE)]
```

Nobody works like this, instead we use __logical clauses__ to __generate__ these logical vectors

---


## logical clauses

+ equality or not: `==`, `!=`
+ inequalities: `<`. `>`, `<=`, `>=`
+ union (OR) `|`
+ intersection (AND) `&`
+ belonging `%in%`
+ differences between sets: `setdiff()`
+ negation works `!`: "not in" `!a %in% b`

---
## comparing vectors

<small>
```{r recycling}
animals      <- c("mouse", "rat", "dog", "cat")
more_animals <- c("rat", "cat", "dog", "duck", "goat")
animals %in% more_animals
```

---
##comparing vectors

<small>
```{r recycling2}
animals      <- c("mouse", "rat", "dog", "cat")
more_animals <- c("rat", "cat", "dog", "duck", "goat")
animals == more_animals
```

+ Vectors are compared __one by one AND recycled__ when one of them is shorter, so use `%in%` when you want to check __belonging to a set__

---
## missing data

<small>
```{r na}
heights <- c(2, 4, 4, NA, 6)
mean(heights)
max(heights)
mean(heights, na.rm = TRUE)
max(heights, na.rm = TRUE)
```

---

## data structures

+ __vector__: lineal arrays (one dimension: only length)

--

+ __factors__: vectors (one-dimensional) representing __categorical variables__ and thus having __levels__

--

+ __matrices__: arrays of vectors -> the same type (all numeric or all character, for instance) (two dimensions: width and length)

--

+ __data frames__: two-dimensional arrays but might be of combined types (i.e., column 1 with names, column 2 with numbers)

--

+ __arrays__ are similar to matrices and dataframes but may be three-dimensional ("layered" data frames)

--

+ __list__: literally a list of anything (a list of data frames, or different objects)

---
class: inverse, middle, center

# Getting help in R

---
## Getting help

+ `help()` and `vignette()`
+ community forums
+ comminity slack (R-Ladies+, LatinR...)
+ stack overflow


---
## Other sources of help

+ Taskviews 

[https://cran.r-project.org/web/views/](https://cran.r-project.org/web/views/)


---
class: center, middle
# ¡Thanks!

<center>
`r icons::icon_style(icons::fontawesome("paper-plane", style = "solid"), scale = 1, fill = "#A70000")` [saramortara@gmail.com](mailto:saramortara@gmail.com) | [andreasancheztapia@gmail.com](mailto:andreasancheztapia@gmail.com) 


`r icons::icon_style(icons::fontawesome("twitter", style = "brands"), scale = 1, fill = "#A70000")` [@MortaraSara](https://twitter.com/MortaraSara) | [@SanchezTapiaA](https://twitter.com/SanchezTapiaA) 

`r icons::icon_style(icons::fontawesome("github", style = "brands"), scale = 1, fill = "#A70000")``r icons::icon_style(icons::fontawesome("gitlab", style = "brands"), scale = 1, fill = "#A70000")`  [saramortara](http://github.com/saramortara) |  [andreasancheztapia](http://github.com/andreasancheztapia) 


