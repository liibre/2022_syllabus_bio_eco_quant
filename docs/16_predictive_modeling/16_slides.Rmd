---
title: "Predictive modeling in R"
subtitle: "Serrapilheira/ICTP-SAIFR Training Program in Quantitative Biology and Ecology"
author: 
  - "Andrea Sánchez-Tapia & Sara Mortara"
date: '9 August 2022'
output: 
  xaringan::moon_reader:
    lib_dir: libs
    css: xaringan-themer.css
    nature:
      highlightStyle: github
      highlightLines: true
      countIncrementalSlides: false
      ratio: '16:9'
---
class: middle

```{r setup, include=FALSE}
options(htmltools.dir.version = FALSE)
```

```{r xaringanExtra-clipboard, echo=FALSE}
htmltools::tagList(
  xaringanExtra::use_clipboard(
    button_text = "Copy code <i class=\"fa fa-clipboard\"></i>",
    success_text = "Copied! <i class=\"fa fa-check\" style=\"color: #90BE6D\"></i>",
    error_text = "Not copied :/ <i class=\"fa fa-times-circle\" style=\"color: #F94144\"></i>"
  ),
  rmarkdown::html_dependency_font_awesome()
)
xaringanExtra::use_share_again()
```

```{r xaringan-themer, include = FALSE}
library(xaringanthemer)
co <- "#A70000"
xaringanthemer::style_mono_accent(
  base_color = co,
  #  primary_color = "#A70000",
  #secondary_color = "#ff0000",
  white_color = "white",
  colors = c(
    red = "#A70000",
    purple = "#88398a",
    orange = "#ff8811",
    green = "#136f63",
    blue = "#4B4FFF",
    white = "#FFFFFF",
    black = "#181818"
  ),
  header_font_google = google_font("Roboto Condensed"),
  text_font_google   = google_font("Roboto Condensed", "300", "300i"),
  code_font_google   = google_font("Ubuntu Mono"),
  text_font_size = "30px",
  code_font_size = "30px",
)
```
    
    
## Supervised vs. unsupervised statistical learning

+ __Supervised statistical learning__ involves building a statistical model for predicting, or estimating an output based on one or more inputs. The output is known (labelled) and set apart from the building model procedure: it is there to _supervise_ the performance of the model

--

+ __Unsupervised statistical learning__ involves building a statistical model based on one or more inputs without a supervising output dataset. PCA and cluster analysis are unsupervised statistical learning methods, according to this classification.

---

## Regression vs classification

+ Some problems involve fitting linear models and minimizing some error measure (Mean Squares)

+ Some others involve putting entities in categories and checking if the categories were correctly assigned

---
## Algorithms


+ (Generalized) mixed linear models
+ Classification: logistic regression, linear discriminant analysis, K-nearest neighbors

+ Support vector machines

+ Tree based: 
    + CART
    + RandomForests
    + Boosted regression trees

---
## General workflow

0. split the data into training and testing sets

TEST DATA cannot participate from the model fitting step.

---
## Split data

+ Cross validation

--

+ Bootstrap

--

+ Jacknife (Leave-one-out)

---
# Model calibration

+ Split the training data further, into model training and calibration sets. 
