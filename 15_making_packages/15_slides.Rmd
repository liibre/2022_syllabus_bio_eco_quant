---
title: "Creating R packages"
subtitle: "Serrapilheira/ICTP-SAIFR Training Program in Quantitative Biology and Ecology"
author: 
  - "Andrea Sánchez-Tapia & Sara Mortara"
date: '9 Ago 2022'
output:
  xaringan::moon_reader:
    lib_dir: libs
    css: xaringan-themer.css
    nature:
      highlightStyle: github
      highlightLines: true
      countIncrementalSlides: false
      ratio: '16:9'
self_contained: true
---
background-image: url("figs/logo.png")
background-position: 90% 5%
background-size: 100px

```{r setup, include=FALSE}
options(htmltools.dir.version = FALSE)
options(servr.daemon = TRUE)#para que no bloquee la sesión
```

```{r xaringan-themer, include=FALSE, warning=FALSE}
library(xaringanthemer)
style_duo_accent(
  primary_color = "#A70000",
  secondary_color = "#A70000",
  colors = c(
    red = "#A70000",
    purple = "#88398a",
    orange = "#ff8811",
    green = "#136f63",
    blue = "#4B4FFF",
    white = "#FFFFFF",
    black = "#181818"
  ),
  text_bold_color = "#181818",
  header_font_google = google_font("Roboto Condensed"),
  text_font_google = google_font("Roboto Condensed", "300", "300i"),
  code_font_google = google_font("Fira Mono"), text_font_size = "28px"
)
xaringanExtra::use_share_again()
```
```{r xaringanExtra-clipboard, echo=FALSE}
htmltools::tagList(
  xaringanExtra::use_clipboard(
    button_text = "Copy code <i class=\"fa fa-clipboard\"></i>",
    success_text = "Copied! <i class=\"fa fa-check\" style=\"color: #90BE6D\"></i>",
    error_text = "Not copied :/ <i class=\"fa fa-times-circle\" style=\"color: #F94144\"></i>"
  ),
  rmarkdown::html_dependency_font_awesome()
)
```


background-image: url("figs/chevron.png")
background-position: 98% 2%
background-size: 150px

### Why create R packages?

+ R philosophy: smooth transition from user to developer

--

+ Movement towards __diversifying the backgrounds of the developers__

--

+ Useful to share __sets of functions__ with a similar purpose, clean and usable __datasets__, __internal functions__ in labs or companies, or whole analyses

--

+ __Learning experience__ about portability, transparency, testing, contribution and collaboration

--

+ Knowing how packages are built allows you to __contribute__ to other people's packages


---
background-image: url("figs/rpackages.png")
background-size: 250px
background-position: 90% 50%

## An R package 

+ Has a special folder structure and files

+ Specific documentation for functions, datasets, vignettes, and additional articles
    
+ Needs a license

+ Can be built using tests

+ Submission to CRAN or keeping in GitHub

---
# File structure

```bash
.
├── .buildignore # Binary files and folders that won't be built
*├── DESCRIPTION  
*├── NAMESPACE    
*├── R/           # Functions
*├── man/         # Documentation
├── data/         # For data, called like: data(cars)
├── inst/         # Installation files
├── vignettes/   
└── README.md    
```

+ Some elements are mandatory (DESCRIPTION, NAMESPACE, R/, man/)
+ New project inside RStudio
+ Package __usethis__: `usethis::create_package()`

---
## Notes: 

+ DESCRIPTION is edited by hand

+ Functions need to be documented inside each `.R` file, the rest of the files are created automatically by packages __devtools__ and __roxygen2__

+ Package __roxygen2__ will transform comments in `.R` into documentation inside `/man` and will fill the NAMESPACE, indicating which functions will be __imported__ or __exported__ by the package

---
background-image: url("figs/devtools.png")
background-position: 98% 2%
background-size: 150px

## During package development


+ The documentation is built using `devtools::document()`. 

+ Check how the function populates the `/man` subfolder.

--

+ When your package is not installed but you want to check it, __load it__: `devtools::load_all()` 

--

+ CRAN makes some automatic checks when you submit. Run these tests early and often, so you don't have trouble at the end: `devtools::check()` 

--

+ A package can be installed directly from GitHub `remotes::install_github("profile/nameofthepackage")`


---

## More features

+ You can add tests for the functions with __testthat__

--

+ You can create a website for your package using __pkgdown__

--

+ Package __usethis__ automatizes lots of actions and creates the files needed to use __GitHub actions__

<center>
```{r, echo=FALSE}
knitr::include_graphics("figs/pkgdown.png", dpi = 150)
knitr::include_graphics("figs/usethis.png", dpi = 150)
knitr::include_graphics("figs/testthat.png",dpi = 72)

```

</center>
