---
title: "Literate programming and reproducible scientific manuscripts"
author:
  - name: Andrea Sánchez-Tapia
    orcid_id: 0000-0002-3521-4338
    affiliation: ¡liibre!
    affiliation_url: {}
    url: https://andreasancheztapia.netlify.app
  - name: Sara Mortara
    affiliation: re.green
    url: {}
    orcid_id: 0000-0001-6221-7537
date: 2022-08-10
citation_url: https://scientific-computing.netlify.app/17_scientific_manuscripts.html
output: distill::distill_article
---


```{r xaringanExtra-clipboard, echo=FALSE}
htmltools::tagList(
  xaringanExtra::use_clipboard(
    button_text = "Copy code <i class=\"fa fa-clipboard\"></i>",
    success_text = "Copied! <i class=\"fa fa-check\" style=\"color: #90BE6D\"></i>",
    error_text = "Not copied :/ <i class=\"fa fa-times-circle\" style=\"color: #F94144\"></i>"
  ),
  rmarkdown::html_dependency_font_awesome()
)
```

# Slides

```{r slides}
#| eval: true
#| echo: false
#| include: true
xaringanExtra::embed_xaringan("17_scientific_manuscripts/17_slides.html")
```

# Packages

```{r}
#| eval: false
remotes::install_github("ThinkR-open/remedy")
install.packages("knitr")
#If you use RStudio, install remedy and check how it works
```

1. Create a new Rmd document in `File > New file > R Markdown...` (or a .Rmd text file) and knit it to check if your computer has any dependencies missing
 
## Today: choose your own adventure

1. Learn about reference managers
1. Learn about __rmarkdown__
1. Learn about $\LaTeX$
1. Know what `.bib` is and know how to create one 
1. Know hoe to cite bibliography using `.bib` in __rmarkdown__ or $\LaTeX$
1. __Use version control for writing__
1. Learn about [Quarto](https://quarto.org/docs/tools/rstudio.html)


# Reference managers

Do you use a reference manager? 

If you do not, download and install __Zotero__ (https://zotero.org)

Then install __BetterBibTex__ https://retorque.re/zotero-better-bibtex/


If you use __Mendeley__, find the option to export your library to BibTeX (.bib) [help](https://service.elsevier.com/app/answers/detail/a_id/27743/supporthub/mendeley/p/16075/)

# Files

A sample .bib file can be found [here](17_scientific_manuscripts/citation/AED.bib)

A .csl citation style file can be found [here](17_scientific_manuscripts/citation/journal-of-ecology.csl)

More styles can be downloaded from the [Zotero style repository](https://www.zotero.org/styles)


# References

+ Rosanna van Hespen: Writing your thesis with R https://www.rosannavanhespen.nl/2016/02/16/writing-your-thesis-with-r-markdown-1-getting-started/

- R Markdown: The Definitive Guide https://bookdown.org/yihui/rmarkdown/

- R markdown cookbook: https://bookdown.org/yihui/rmarkdown-cookbook/

- R Markdown documentation: https://rmarkdown.rstudio.com
https://bookdown.org/yihui/rmarkdown/interactive-documents.html

