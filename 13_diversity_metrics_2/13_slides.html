<!DOCTYPE html>
<html lang="" xml:lang="">
  <head>
    <title>From taxonomic to functional and phylogenetic diversity</title>
    <meta charset="utf-8" />
    <meta name="author" content="Andrea Sánchez-Tapia" />
    <meta name="date" content="2022-08-03" />
    <script src="libs/header-attrs/header-attrs.js"></script>
    <script src="libs/clipboard/clipboard.min.js"></script>
    <link href="libs/xaringanExtra-clipboard/xaringanExtra-clipboard.css" rel="stylesheet" />
    <script src="libs/xaringanExtra-clipboard/xaringanExtra-clipboard.js"></script>
    <script>window.xaringanExtraClipboard(null, {"button":"Copy code <i class=\"fa fa-clipboard\"><\/i>","success":"Copied! <i class=\"fa fa-check\" style=\"color: #90BE6D\"><\/i>","error":"Not copied :/ <i class=\"fa fa-times-circle\" style=\"color: #F94144\"><\/i>"})</script>
    <link href="libs/font-awesome/css/all.css" rel="stylesheet" />
    <link href="libs/font-awesome/css/v4-shims.css" rel="stylesheet" />
    <link href="libs/shareon/shareon.min.css" rel="stylesheet" />
    <script src="libs/shareon/shareon.min.js"></script>
    <link href="libs/xaringanExtra-shareagain/shareagain.css" rel="stylesheet" />
    <script src="libs/xaringanExtra-shareagain/shareagain.js"></script>
    <link rel="stylesheet" href="xaringan-themer.css" type="text/css" />
  </head>
  <body>
    <textarea id="source">
class: center, middle, inverse, title-slide

.title[
# From taxonomic to functional and phylogenetic diversity
]
.subtitle[
## Serrapilheira/ICTP-SAIFR Training Program in Quantitative Biology and Ecology
]
.author[
### Andrea Sánchez-Tapia
]
.date[
### 3 August 2022
]

---










### So far: 

+ Organized repository

+ Map made in `tmap` for Spatial Analysis class

+ Code w/calculations from last class



---
## Creating functions

+ Think of the _application in real life_ of the function. Ex. If the function gets a single vector, or a data frame, it should work.

+ R functions tend to work in vectors, test at the vector or the matrix/dataframe scale first. 


---


```r
my_shannon &lt;- function(x) {
  pi &lt;- x/sum(x)
  H &lt;- -sum(pi * log(pi))
}

my_simpson &lt;- function(x) {
  pi &lt;- x/sum(x)
  Simp &lt;- 1 - sum(pi^2)
}

my_diversity &lt;- function(x, index = "shannon") {
  pi &lt;- x/sum(x)
  if (index == "shannon") d &lt;- -sum(pi * log(pi))
  if (index == "simpson") d &lt;- 1 -   sum(pi^2)
  return(d)
}
```

---

## Biological diversity measures

+ __Biodiversity__ is a complex multifaceted concept that includes scales in space and time, and entities such as __species__, __traits__ and __evolutionary units__ (Pavoine and Bonsall 2011)

--

+ Diversity indexes have a __richness__ and __evenness__ components 

--

+ Mathematical properties: 

    + Must increase when more _new_ elements are added to the sample

    + Must be maximal when all the elements have the same frequency (__evenness__)

+ Some indices are proportional to sample size: a mathematical artifact


---
## Taxonomic diversity indices


+ Shannon diversity index


$$
H = - \sum_{n=1}^{S}p_i \ln p_i
$$

+ Simpson's diversity index would be
$$
1 - \sum p_i^2
$$
+ Inverse Simpson 
$$
1/\sum p_i^2 
$$

---
# Hill numbers, Rényi diversity

+ Common diversity indices are special cases of _Rényi diversity_

$$
H_a = 1/(1-a) * log \sum(p^a)
$$

+ Hill numbers (Hill 1973)

$$
N_a = \exp(H_a)
$$

Some different Hill numbers:
Richness: `\(N_0 = S\)`
Shannon diversity: `\(N_1 = e(H')\)`
Inverse Simpson `\(N_2 = 1/D\)`


---
## Understanding which community is more diverse is not always straightforward

.pull-left[

```r
Community.A &lt;- c(10, 6, 4, 1)
Community.B &lt;- c(17, rep(1, 7))

library(vegan)

diversity(Community.A, "shannon")
diversity(Community.B, "shannon")
diversity(Community.A, "invsimpson")
diversity(Community.B, "invsimpson")
```

]

.pull-right[



![](13_slides_files/figure-html/unnamed-chunk-3-1.png)&lt;!-- --&gt;
]

---
### Rényi diversity profiles intersect: cannot compare diversities

&lt;img src="13_slides_files/figure-html/unnamed-chunk-4-1.png" width="500" style="display: block; margin: auto;" /&gt;

---
## From taxonomic to functional diversity: the briefest introduction to trait-based approaches in ecology

+ __"Functional trait"__: a morphological/physiological characteristic that interacts directly with the biotic and abiotic environment, determining the species' ability to establish and survive along environmental gradients and in the presence of other species 

+ Restricted by the existence of fundamental trade-offs, creating integrated responses to the same factors (__ecological strategies__)

+ __Functional diversity:__ number, type, and distribution of functions performed by organisms within an ecosystem (Díaz &amp; Cabido )

---
## Trait spaces and distances

+ Measured at the __individual__ scale

--

+ Functional traits can be continuous or categorical

--

+ Adequate distance/dissimilarity measures: __Gower's__ distance

--

+ Inter-specific and intra-specific differences between individuals

--

+ Some examples: 
    + Whole-plant trait: plant height, growth form

    + Leaf traits: Specific Leaf Area and Leaf Dry matter content

---
class: middle

&lt;img src="figs/villeger.png" width="700" style="display: block; margin: auto;" /&gt;
Functional richness, functional evenness, divergence __(Villéger et al 2008)__


---
## Phylogenetic diversity

+ Distance between species is __phylogenetic__. The most simplistic: numbers of nodes in a phylogenetic classification. The ideal: millions of years of the last (most recent) known split between taxa.

+ Phylogenetic autocorrelation: closer taxa share common ancestry, more phylogenetic diversity == more distinct taxa in the communities

+ A classic first measure was a dendrogram-based approach (Faith 1992)
Sum of the length of the branches in a dendrogram

---
class: middle, center

&lt;img src="figs/PD.png" width="801" style="display: block; margin: auto;" /&gt;

__(Faith 1992)__
 &lt;!-- SOURCE: https://image1.slideserve.com/1873912/phylogenetic-diversity-pd-l.jpg --&gt; 

---
# Phylogenies in community ecology

+ We use topologies obtained from other authors (ex. APG III Magallón), __dated nodes__, and adjusting algorithms (Webb, Ackerly &amp; Kembel 2008) 

+ __Phylomatic__  (Webb, Ackerly &amp; Kembel 2008)


---
# A unifying paradigm: Rao's quadratic entropy

 &lt;!-- $$ H_D(p) = \sum_{i=1}^{n} \sum_{j=1}^{n} p_i p_j d_{ij}$$ --&gt; 
&lt;img src="figs/rao.png" width="283" style="display: block; margin: auto;" /&gt;

+ Where `\(d_{ij}\)` is a measure of the dissimilarity between species `\(i\)` and `\(j\)`

+ Quadratic entropy measures __the expected difference__ between two entities randomly drawn from the set

---

### How to conduct diversity analyses in R?

+ [Environmetrics CRAN Task View](https://cran.r-project.org/web/views/Environmetrics.html) (Gavin Simpson)

+ 📦 `vegan`, `ade4`, `FD`, `ape`

+ Publications!

---
    </textarea>
<style data-target="print-only">@media screen {.remark-slide-container{display:block;}.remark-slide-scaler{box-shadow:none;}}</style>
<script src="https://remarkjs.com/downloads/remark-latest.min.js"></script>
<script>var slideshow = remark.create({
"highlightStyle": "github",
"highlightLines": true,
"countIncrementalSlides": false,
"ratio": "16:9"
});
if (window.HTMLWidgets) slideshow.on('afterShowSlide', function (slide) {
  window.dispatchEvent(new Event('resize'));
});
(function(d) {
  var s = d.createElement("style"), r = d.querySelector(".remark-slide-scaler");
  if (!r) return;
  s.type = "text/css"; s.innerHTML = "@page {size: " + r.style.width + " " + r.style.height +"; }";
  d.head.appendChild(s);
})(document);

(function(d) {
  var el = d.getElementsByClassName("remark-slides-area");
  if (!el) return;
  var slide, slides = slideshow.getSlides(), els = el[0].children;
  for (var i = 1; i < slides.length; i++) {
    slide = slides[i];
    if (slide.properties.continued === "true" || slide.properties.count === "false") {
      els[i - 1].className += ' has-continuation';
    }
  }
  var s = d.createElement("style");
  s.type = "text/css"; s.innerHTML = "@media print { .has-continuation { display: none; } }";
  d.head.appendChild(s);
})(document);
// delete the temporary CSS (for displaying all slides initially) when the user
// starts to view slides
(function() {
  var deleted = false;
  slideshow.on('beforeShowSlide', function(slide) {
    if (deleted) return;
    var sheets = document.styleSheets, node;
    for (var i = 0; i < sheets.length; i++) {
      node = sheets[i].ownerNode;
      if (node.dataset["target"] !== "print-only") continue;
      node.parentNode.removeChild(node);
    }
    deleted = true;
  });
})();
// add `data-at-shortcutkeys` attribute to <body> to resolve conflicts with JAWS
// screen reader (see PR #262)
(function(d) {
  let res = {};
  d.querySelectorAll('.remark-help-content table tr').forEach(tr => {
    const t = tr.querySelector('td:nth-child(2)').innerText;
    tr.querySelectorAll('td:first-child .key').forEach(key => {
      const k = key.innerText;
      if (/^[a-z]$/.test(k)) res[k] = t;  // must be a single letter (key)
    });
  });
  d.body.setAttribute('data-at-shortcutkeys', JSON.stringify(res));
})(document);
(function() {
  "use strict"
  // Replace <script> tags in slides area to make them executable
  var scripts = document.querySelectorAll(
    '.remark-slides-area .remark-slide-container script'
  );
  if (!scripts.length) return;
  for (var i = 0; i < scripts.length; i++) {
    var s = document.createElement('script');
    var code = document.createTextNode(scripts[i].textContent);
    s.appendChild(code);
    var scriptAttrs = scripts[i].attributes;
    for (var j = 0; j < scriptAttrs.length; j++) {
      s.setAttribute(scriptAttrs[j].name, scriptAttrs[j].value);
    }
    scripts[i].parentElement.replaceChild(s, scripts[i]);
  }
})();
(function() {
  var links = document.getElementsByTagName('a');
  for (var i = 0; i < links.length; i++) {
    if (/^(https?:)?\/\//.test(links[i].getAttribute('href'))) {
      links[i].target = '_blank';
    }
  }
})();
// adds .remark-code-has-line-highlighted class to <pre> parent elements
// of code chunks containing highlighted lines with class .remark-code-line-highlighted
(function(d) {
  const hlines = d.querySelectorAll('.remark-code-line-highlighted');
  const preParents = [];
  const findPreParent = function(line, p = 0) {
    if (p > 1) return null; // traverse up no further than grandparent
    const el = line.parentElement;
    return el.tagName === "PRE" ? el : findPreParent(el, ++p);
  };

  for (let line of hlines) {
    let pre = findPreParent(line);
    if (pre && !preParents.includes(pre)) preParents.push(pre);
  }
  preParents.forEach(p => p.classList.add("remark-code-has-line-highlighted"));
})(document);</script>

<script>
slideshow._releaseMath = function(el) {
  var i, text, code, codes = el.getElementsByTagName('code');
  for (i = 0; i < codes.length;) {
    code = codes[i];
    if (code.parentNode.tagName !== 'PRE' && code.childElementCount === 0) {
      text = code.textContent;
      if (/^\\\((.|\s)+\\\)$/.test(text) || /^\\\[(.|\s)+\\\]$/.test(text) ||
          /^\$\$(.|\s)+\$\$$/.test(text) ||
          /^\\begin\{([^}]+)\}(.|\s)+\\end\{[^}]+\}$/.test(text)) {
        code.outerHTML = code.innerHTML;  // remove <code></code>
        continue;
      }
    }
    i++;
  }
};
slideshow._releaseMath(document);
</script>
<!-- dynamically load mathjax for compatibility with self-contained -->
<script>
(function () {
  var script = document.createElement('script');
  script.type = 'text/javascript';
  script.src  = 'https://mathjax.rstudio.com/latest/MathJax.js?config=TeX-MML-AM_CHTML';
  if (location.protocol !== 'file:' && /^https?:/.test(script.src))
    script.src  = script.src.replace(/^https?:/, '');
  document.getElementsByTagName('head')[0].appendChild(script);
})();
</script>
  </body>
</html>
