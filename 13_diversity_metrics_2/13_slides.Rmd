---
title: "From taxonomic to functional and phylogenetic diversity"
subtitle: "Serrapilheira/ICTP-SAIFR Training Program in Quantitative Biology and Ecology"
author: 
  - "Andrea Sánchez-Tapia"
date: '3 August 2022'
output: 
  xaringan::moon_reader:
    lib_dir: libs
    css: xaringan-themer.css
    nature:
      highlightStyle: github
      highlightLines: true
      countIncrementalSlides: false
      ratio: '16:9'
---

```{r setup, include=FALSE}
options(htmltools.dir.version = FALSE)
```

```{r xaringanExtra-clipboard, echo=FALSE}
htmltools::tagList(
  xaringanExtra::use_clipboard(
    button_text = "Copy code <i class=\"fa fa-clipboard\"></i>",
    success_text = "Copied! <i class=\"fa fa-check\" style=\"color: #90BE6D\"></i>",
    error_text = "Not copied :/ <i class=\"fa fa-times-circle\" style=\"color: #F94144\"></i>"
  ),
  rmarkdown::html_dependency_font_awesome()
)
xaringanExtra::use_share_again()
```



```{r xaringan-themer, include = FALSE}
library(xaringanthemer)
library(xaringanthemer)
co <- "#A70000"
xaringanthemer::style_mono_accent(
  base_color = co,
  #  primary_color = "#A70000",
  #secondary_color = "#ff0000",
  white_color = "white",
  colors = c(
    red = "#A70000",
    purple = "#88398a",
    orange = "#ff8811",
    green = "#136f63",
    blue = "#4B4FFF",
    white = "#FFFFFF",
    black = "#181818"
  ),
  header_font_google = google_font("Roboto Condensed"),
  text_font_google   = google_font("Roboto Condensed", "300", "300i"),
  code_font_google   = google_font("Ubuntu Mono"),
  text_font_size = "30px",
  code_font_size = "30px",
)
```

### So far: 

+ Organized repository

+ Map made in `tmap` for Spatial Analysis class

+ Code w/calculations from last class



---
## Creating functions

+ Think of the _application in real life_ of the function. Ex. If the function gets a single vector, or a data frame, it should work.

+ R functions tend to work in vectors, test at the vector or the matrix/dataframe scale first. 


---

```{r}
my_shannon <- function(x) {
  pi <- x/sum(x)
  H <- -sum(pi * log(pi))
}

my_simpson <- function(x) {
  pi <- x/sum(x)
  Simp <- 1 - sum(pi^2)
}

my_diversity <- function(x, index = "shannon") {
  pi <- x/sum(x)
  if (index == "shannon") d <- -sum(pi * log(pi))
  if (index == "simpson") d <- 1 -   sum(pi^2)
  return(d)
}
```

---

## Biological diversity measures

+ __Biodiversity__ is a complex multifaceted concept that includes scales in space and time, and entities such as __species__, __traits__ and __evolutionary units__ (Pavoine and Bonsall 2011)

--

+ Diversity indexes have a __richness__ and __evenness__ components 

--

+ Mathematical properties: 

    + Must increase when more _new_ elements are added to the sample

    + Must be maximal when all the elements have the same frequency (__evenness__)

+ Some indices are proportional to sample size: a mathematical artifact


---
## Taxonomic diversity indices


+ Shannon diversity index


$$
H = - \sum_{n=1}^{S}p_i \ln p_i
$$

+ Simpson's diversity index would be
$$
1 - \sum p_i^2
$$
+ Inverse Simpson 
$$
1/\sum p_i^2 
$$

---
# Hill numbers, Rényi diversity

+ Common diversity indices are special cases of _Rényi diversity_

$$
H_a = 1/(1-a) * log \sum(p^a)
$$

+ Hill numbers (Hill 1973)

$$
N_a = \exp(H_a)
$$

Some different Hill numbers:
Richness: $N_0 = S$
Shannon diversity: $N_1 = e(H')$
Inverse Simpson $N_2 = 1/D$


---
## Understanding which community is more diverse is not always straightforward

.pull-left[
```{r, eval = F}
Community.A <- c(10, 6, 4, 1)
Community.B <- c(17, rep(1, 7))

library(vegan)

diversity(Community.A, "shannon")
diversity(Community.B, "shannon")
diversity(Community.A, "invsimpson")
diversity(Community.B, "invsimpson")
```

]

.pull-right[



```{r, echo = FALSE}
library(ggplot2)
Community.A <- data.frame(t(c(10, 6, 4, 1)))
Community.B <- data.frame(t(c(17, rep(1, 7))))
Communities <-
  data.frame(Community = c(rep("Community A", 4), rep("Community B", 8)),
             Abundance = c(10, 6, 4, 1, 17, rep(1, 7)))

Communities$Species <- factor(c(c(1:4), c(1:8)))

ggplot(data = Communities,
       aes(x = Species, y = Abundance,  colour = Species, label = Abundance)) +
  geom_col(aes(fill = Community),
           position = position_dodge2(width = 1.0,  preserve = "single"), colour="black") +
  geom_text(aes(y = Abundance + 1),
            position = position_dodge2(width = 0.5, preserve = "single"), colour = "black", size = 5) +
  facet_grid(~ Community, scales = "free_x", space = "free_x") +
  theme_minimal() +
  theme(legend.position = "bottom",
        text = element_text(size = 2)
        )
```
]

---
### Rényi diversity profiles intersect: cannot compare diversities

```{r, echo = F, message=FALSE, warning=F, fig.align='center', out.width=500}
library(vegan, quietly = T)
Community.AB <- data.frame(array(0, dim = c(2, 8)))
rownames(Community.AB) <- c("Community A", "Community B")
Community.AB[1, 1:4] <- Community.A
Community.AB[2, ] <- Community.B


renyi.example <- renyi(Community.AB,
                       scales = c(0, 0.25, 0.5, 1, 2, 4, 8, Inf))

#renyi.example
matplot(t(renyi.example), type = "l", 
        ylab = "Rényi diversity profile", 
        axes = F)
box()
axis(2)
axis(1, at = 1:8, labels = c(0, 0.25, 0.5, 1, 2, 4, 8, "Inf"))
legend("topright",
       legend = c("Community A", "Community B"), lty = c(1,2))

```

---
## From taxonomic to functional diversity: the briefest introduction to trait-based approaches in ecology

+ __"Functional trait"__: a morphological/physiological characteristic that interacts directly with the biotic and abiotic environment, determining the species' ability to establish and survive along environmental gradients and in the presence of other species 

+ Restricted by the existence of fundamental trade-offs, creating integrated responses to the same factors (__ecological strategies__)

+ __Functional diversity:__ number, type, and distribution of functions performed by organisms within an ecosystem (Díaz & Cabido )

---
## Trait spaces and distances

+ Measured at the __individual__ scale

--

+ Functional traits can be continuous or categorical

--

+ Adequate distance/dissimilarity measures: __Gower's__ distance

--

+ Inter-specific and intra-specific differences between individuals

--

+ Some examples: 
    + Whole-plant trait: plant height, growth form

    + Leaf traits: Specific Leaf Area and Leaf Dry matter content

---
class: middle

```{r, echo = F, fig.align='center', out.width=700}
knitr::include_graphics("figs/villeger.png")
```
Functional richness, functional evenness, divergence __(Villéger et al 2008)__


---
## Phylogenetic diversity

+ Distance between species is __phylogenetic__. The most simplistic: numbers of nodes in a phylogenetic classification. The ideal: millions of years of the last (most recent) known split between taxa.

+ Phylogenetic autocorrelation: closer taxa share common ancestry. More phylogenetic diversity == more distinct taxa in the communities

+ A classic first measure was a dendrogram-based approach (Faith 1992)
Sum of the length of the branches in a dendrogram

---
class: middle, center

```{r, echo = F, fig.align='center'}
knitr::include_graphics("figs/PD.png")
```

__(Faith 1992)__
 <!-- SOURCE: https://image1.slideserve.com/1873912/phylogenetic-diversity-pd-l.jpg --> 

---
# Phylogenies in community ecology

+ We use topologies obtained from other authors (ex. APG III Magallón), __dated nodes__, and adjusting algorithms (Webb, Ackerly & Kembel 2008) 

+ __Phylomatic__  (Webb, Ackerly & Kembel 2008)


---
# A unifying paradigm: Rao's quadratic entropy

 <!-- $$ H_D(p) = \sum_{i=1}^{n} \sum_{j=1}^{n} p_i p_j d_{ij}$$ --> 
```{r, echo = FALSE, fig.align='center'}
knitr::include_graphics("figs/rao.png")
```

+ Where $d_{ij}$ is a measure of the dissimilarity between species $i$ and $j$

+ Quadratic entropy measures __the expected difference__ between two entities randomly drawn from the set

---

### How to conduct diversity analyses in R?

+ [Environmetrics CRAN Task View](https://cran.r-project.org/web/views/Environmetrics.html) (Gavin Simpson)

+ `r emo::ji("package")` `vegan`, `ade4`, `FD`, `ape`

+ Publications!

---
